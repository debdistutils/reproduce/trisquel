+ date
Mon Apr  3 15:17:05 UTC 2023
+ apt-get source --only-source wireless-tools
Reading package lists...
Need to get 195 kB of source archives.
Get:1 http://archive.trisquel.info/trisquel aramo/main wireless-tools 30~pre9-13.1ubuntu4+11.0trisquel1 (dsc) [2010 B]
Get:2 http://archive.trisquel.info/trisquel aramo/main wireless-tools 30~pre9-13.1ubuntu4+11.0trisquel1 (tar) [193 kB]
dpkg-source: info: extracting wireless-tools in wireless-tools-30~pre9
dpkg-source: info: unpacking wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1.tar.xz
Fetched 195 kB in 0s (1113 kB/s)
W: Download is performed unsandboxed as root as file 'wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source wireless-tools
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-strip-nondeterminism dwz libdebhelper-perl
  libfile-stripnondeterminism-perl libsub-override-perl libtool m4 po-debconf
0 upgraded, 15 newly installed, 0 to remove and 1 not upgraded.
Need to get 3149 kB of archives.
After this operation, 8859 kB of additional disk space will be used.
Get:1 http://archive.trisquel.info/trisquel aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://archive.trisquel.info/trisquel aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:3 http://archive.trisquel.info/trisquel aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:4 http://archive.trisquel.info/trisquel aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:5 http://archive.trisquel.info/trisquel aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:6 http://archive.trisquel.info/trisquel aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:7 http://archive.trisquel.info/trisquel aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:8 http://archive.trisquel.info/trisquel aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:9 http://archive.trisquel.info/trisquel aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:10 http://archive.trisquel.info/trisquel aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:11 http://archive.trisquel.info/trisquel aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:12 http://archive.trisquel.info/trisquel aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:13 http://archive.trisquel.info/trisquel aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:14 http://archive.trisquel.info/trisquel aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:15 http://archive.trisquel.info/trisquel aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3149 kB in 0s (10.3 MB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123238 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../01-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../02-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../03-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../04-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../05-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../06-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../07-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../08-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../09-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../10-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../11-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../12-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../13-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../14-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name wireless-tools* -type d
+ cd ./wireless-tools-30~pre9
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package wireless-tools
dpkg-buildpackage: info: source version 30~pre9-13.1ubuntu4+11.0trisquel1
dpkg-buildpackage: info: source distribution aramo
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   dh_auto_clean
	make -j1 realclean
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9'
rm -f *.BAK *.bak *.d *.o *.so ,* *~ *.a *.orig *.rej *.out 
rm -f libiw.a libiw.so.30 iwconfig iwlist iwpriv iwspy iwgetid iwevent ifrename macaddr iwmulticall libiw* wireless.h
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9'
   dh_clean
 dpkg-source -b .
dpkg-source: warning: native package version may not have a revision
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building wireless-tools in wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1.tar.xz
dpkg-source: info: building wireless-tools in wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1.dsc
 debian/rules build
dh build
   dh_update_autotools_config
   dh_autoreconf
   dh_auto_configure
   debian/rules override_dh_auto_build
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9'
dh_auto_build -- all libiw.a
	make -j1 "INSTALL=install --strip-program=true" all libiw.a
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9'
cp wireless.22.h wireless.h
gcc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -I. -MMD     -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro -fPIC -c -o iwlib.so iwlib.c
iwlib.c: In function 'iw_enum_devices':
iwlib.c:263:7: warning: ignoring return value of 'fgets' declared with attribute 'warn_unused_result' [-Wunused-result]
  263 |       fgets(buff, sizeof(buff), fh);
      |       ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~
iwlib.c:264:7: warning: ignoring return value of 'fgets' declared with attribute 'warn_unused_result' [-Wunused-result]
  264 |       fgets(buff, sizeof(buff), fh);
      |       ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~
iwlib.c: In function 'iw_get_kernel_we_version':
iwlib.c:346:3: warning: ignoring return value of 'fgets' declared with attribute 'warn_unused_result' [-Wunused-result]
  346 |   fgets(buff, sizeof(buff), fh);
      |   ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~
iwlib.c:362:3: warning: ignoring return value of 'fgets' declared with attribute 'warn_unused_result' [-Wunused-result]
  362 |   fgets(buff, sizeof(buff), fh);
      |   ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~
gcc -shared -o libiw.so.30 -Wl,-soname,libiw.so.30  -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro iwlib.so -lc -lm
gcc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -I. -MMD     -c iwconfig.c
gcc -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro  -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -I. -MMD     -o iwconfig iwconfig.o libiw.so.30
gcc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -I. -MMD     -c iwlist.c
gcc -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro  -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -I. -MMD     -o iwlist iwlist.o libiw.so.30
gcc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -I. -MMD     -c iwpriv.c
gcc -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro  -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -I. -MMD     -o iwpriv iwpriv.o libiw.so.30
gcc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -I. -MMD     -c iwspy.c
gcc -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro  -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -I. -MMD     -o iwspy iwspy.o libiw.so.30
gcc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -I. -MMD     -c iwgetid.c
gcc -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro  -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -I. -MMD     -o iwgetid iwgetid.o libiw.so.30
gcc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -I. -MMD     -c iwevent.c
gcc -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro  -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -I. -MMD     -o iwevent iwevent.o libiw.so.30
gcc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -I. -MMD     -c ifrename.c
ifrename.c: In function 'mapping_getsysfs':
ifrename.c:1789:23: warning: ignoring return value of 'fchdir' declared with attribute 'warn_unused_result' [-Wunused-result]
 1789 |                       fchdir(cwd_fd);
      |                       ^~~~~~~~~~~~~~
gcc -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro  -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -I. -MMD     -o ifrename ifrename.o libiw.so.30
rm -f libiw.a
ar cru libiw.a iwlib.so
ar: `u' modifier ignored since `D' is the default (see `U')
ranlib libiw.a
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9'
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9'
   dh_auto_test
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary
   dh_testroot
   dh_prep
   debian/rules override_dh_auto_install
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9'
dh_auto_install -- install install-static PREFIX=debian/tmp
	make -j1 install DESTDIR=/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30\~pre9/debian/tmp AM_UPDATE_INFO_DIR=no "INSTALL=install --strip-program=true" install install-static PREFIX=debian/tmp
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9'
install -m 755 -d debian/tmp/lib/x86_64-linux-gnu
install -m 755 libiw.so.30 debian/tmp/lib/x86_64-linux-gnu
install -m 755 -d debian/tmp/usr/lib/x86_64-linux-gnu
ln -sfn /lib/x86_64-linux-gnu/libiw.so.30 debian/tmp/usr/lib/x86_64-linux-gnu/libiw.so
*** Don't forget to add debian/tmp/lib/x86_64-linux-gnu to /etc/ld.so.conf, and run ldconfig as root. ***
install -m 755 -d debian/tmp/sbin
install -m 755 iwconfig iwlist iwpriv iwspy iwgetid iwevent ifrename debian/tmp/sbin
install -m 755 -d debian/tmp/usr/include
install -m 644 iwlib.h debian/tmp/usr/include
install -m 644 wireless.h debian/tmp/usr/include
for lang in . cs fr.*; do \
    install -m 755 -d debian/tmp/usr/share/man/$lang/man8/; \
    install -m 644 $lang/iwconfig.8 iwlist.8 iwpriv.8 iwspy.8 iwgetid.8 iwevent.8 ifrename.8 debian/tmp/usr/share/man/$lang/man8/; \
    install -m 755 -d debian/tmp/usr/share/man/$lang/man7/; \
    install -m 644 $lang/wireless.7 debian/tmp/usr/share/man/$lang/man7/; \
    install -m 755 -d debian/tmp/usr/share/man/$lang/man5/; \
    install -m 644 $lang/iftab.5 debian/tmp/usr/share/man/$lang/man5/; \
done
make[2]: Nothing to be done for 'install'.
install -m 755 -d debian/tmp/usr/lib/x86_64-linux-gnu
install -m 644 libiw.a debian/tmp/usr/lib/x86_64-linux-gnu
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9'
# Hack to make udeb friendly binaries
/usr/bin/make clean
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9'
rm -f *.BAK *.bak *.d *.o *.so ,* *~ *.a *.orig *.rej *.out 
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9'
dh_auto_build -- BUILD_NOLIBM=y CFLAGS="-Os -I."
	make -j1 "INSTALL=install --strip-program=true" BUILD_NOLIBM=y "CFLAGS=-Os -I."
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9'
gcc -Os -I. -MMD   -DWE_NOLIBM=y  -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro -fPIC -c -o iwlib.so iwlib.c
iwlib.c: In function 'iw_enum_devices':
iwlib.c:263:7: warning: ignoring return value of 'fgets' declared with attribute 'warn_unused_result' [-Wunused-result]
  263 |       fgets(buff, sizeof(buff), fh);
      |       ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~
iwlib.c:264:7: warning: ignoring return value of 'fgets' declared with attribute 'warn_unused_result' [-Wunused-result]
  264 |       fgets(buff, sizeof(buff), fh);
      |       ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~
iwlib.c: In function 'iw_get_kernel_we_version':
iwlib.c:346:3: warning: ignoring return value of 'fgets' declared with attribute 'warn_unused_result' [-Wunused-result]
  346 |   fgets(buff, sizeof(buff), fh);
      |   ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~
iwlib.c:362:3: warning: ignoring return value of 'fgets' declared with attribute 'warn_unused_result' [-Wunused-result]
  362 |   fgets(buff, sizeof(buff), fh);
      |   ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~
gcc -shared -o libiw.so.30 -Wl,-soname,libiw.so.30  -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro iwlib.so -lc 
gcc -Os -I. -MMD   -DWE_NOLIBM=y  -c iwconfig.c
gcc -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro  -Os -I. -MMD   -DWE_NOLIBM=y  -o iwconfig iwconfig.o libiw.so.30
gcc -Os -I. -MMD   -DWE_NOLIBM=y  -c iwlist.c
gcc -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro  -Os -I. -MMD   -DWE_NOLIBM=y  -o iwlist iwlist.o libiw.so.30
gcc -Os -I. -MMD   -DWE_NOLIBM=y  -c iwpriv.c
gcc -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro  -Os -I. -MMD   -DWE_NOLIBM=y  -o iwpriv iwpriv.o libiw.so.30
gcc -Os -I. -MMD   -DWE_NOLIBM=y  -c iwspy.c
gcc -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro  -Os -I. -MMD   -DWE_NOLIBM=y  -o iwspy iwspy.o libiw.so.30
gcc -Os -I. -MMD   -DWE_NOLIBM=y  -c iwgetid.c
gcc -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro  -Os -I. -MMD   -DWE_NOLIBM=y  -o iwgetid iwgetid.o libiw.so.30
gcc -Os -I. -MMD   -DWE_NOLIBM=y  -c iwevent.c
gcc -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro  -Os -I. -MMD   -DWE_NOLIBM=y  -o iwevent iwevent.o libiw.so.30
gcc -Os -I. -MMD   -DWE_NOLIBM=y  -c ifrename.c
ifrename.c: In function 'mapping_getsysfs':
ifrename.c:1789:23: warning: ignoring return value of 'fchdir' declared with attribute 'warn_unused_result' [-Wunused-result]
 1789 |                       fchdir(cwd_fd);
      |                       ^~~~~~~~~~~~~~
gcc -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro  -Os -I. -MMD   -DWE_NOLIBM=y  -o ifrename ifrename.o libiw.so.30
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9'
dh_install -pwireless-tools-udeb iwconfig iwgetid iwlist iwevent sbin/
dh_install -plibiw30-udeb libiw.so.30 lib/
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9'
   debian/rules override_dh_install
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9'
dh_install --list-missing
dh_install: warning: Please use dh_missing --list-missing/--fail-missing instead
dh_install: warning: This feature will be removed in compat 12.
dh_missing: warning: sbin/ifrename exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: usr/share/man/cs/man5/iftab.5 exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: usr/share/man/cs/man8/ifrename.8 exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: usr/share/man/fr.ISO8859-1/man5/iftab.5 exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: usr/share/man/fr.ISO8859-1/man8/ifrename.8 exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: usr/share/man/fr.UTF-8/man5/iftab.5 exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: usr/share/man/fr.UTF-8/man8/ifrename.8 exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: usr/share/man/man5/iftab.5 exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: usr/share/man/man8/ifrename.8 exists in debian/tmp but is not installed to anywhere 
	The following debhelper tools have reported what they installed (with files per package)
	 * dh_install: libiw-dev (2), libiw30 (1), libiw30-udeb (1), wireless-tools (34), wireless-tools-udeb (4)
	If the missing files are installed by another tool, please file a bug against it.
	When filing the report, if the tool is not part of debhelper itself, please reference the
	"Logging helpers and dh_missing" section from the "PROGRAMMING" guide for debhelper (10.6.3+).
	  (in the debhelper package: /usr/share/doc/debhelper/PROGRAMMING.gz)
	Be sure to test with dpkg-buildpackage -A/-B as the results may vary when only a subset is built
	If the omission is intentional or no other helper can take care of this consider adding the
	paths to debian/not-installed.
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9'
   dh_installdocs
   dh_installchangelogs
   dh_installman
   dh_installifupdown
   debian/rules override_dh_installudev
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9'
dh_installudev --priority=19
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-tools/wireless-tools-30~pre9'
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_strip
801f19f58c8707f45af9699d52015a755f8aae37
c767184d162412b4529536e9700025ff07f57ebc
19c9fb335e4907a95837da966fc93840f74d2307
5a4761cc000cb9e469a4948f3a5af73d703feb23
256a2495649bb999367f9b9dd32397f1faded8c2
82a97040d6422801cf025426d5543b205fe0a1f3
2fbcc73a0586b1c2e18d1cbe980a1414ace133b9
   dh_makeshlibs
   dh_shlibdeps
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
pkgstripfiles: processing control file: debian/wireless-tools/DEBIAN/control, package wireless-tools, directory debian/wireless-tools
Searching for duplicated docs in dependency libiw30...
  symlinking changelog.Debian.gz in wireless-tools to file in libiw30
pkgstripfiles: Running PNG optimization (using 1 cpus) for package wireless-tools ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'wireless-tools' in '../wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb'.
pkgstripfiles: processing control file: debian/libiw30/DEBIAN/control, package libiw30, directory debian/libiw30
pkgstripfiles: Truncating usr/share/doc/libiw30/changelog.Debian.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 1 cpus) for package libiw30 ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'libiw30' in '../libiw30_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb'.
pkgstripfiles: processing control file: debian/libiw-dev/DEBIAN/control, package libiw-dev, directory debian/libiw-dev
Searching for duplicated docs in dependency libiw30...
  symlinking changelog.Debian.gz in libiw-dev to file in libiw30
pkgstripfiles: Running PNG optimization (using 1 cpus) for package libiw-dev ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'libiw-dev' in '../libiw-dev_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb'.
INFO: Disabling pkgsanitychecks for udeb
pkgstripfiles: processing control file: debian/wireless-tools-udeb/DEBIAN/control, package wireless-tools-udeb, directory debian/wireless-tools-udeb
pkgstripfiles: Running PNG optimization (using 1 cpus) for package wireless-tools-udeb ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'wireless-tools-udeb' in 'debian/.debhelper/scratch-space/build-wireless-tools-udeb/wireless-tools-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb'.
	Renaming wireless-tools-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb to wireless-tools-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.udeb
INFO: Disabling pkgsanitychecks for udeb
pkgstripfiles: processing control file: debian/libiw30-udeb/DEBIAN/control, package libiw30-udeb, directory debian/libiw30-udeb
pkgstripfiles: Running PNG optimization (using 1 cpus) for package libiw30-udeb ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'libiw30-udeb' in 'debian/.debhelper/scratch-space/build-libiw30-udeb/libiw30-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb'.
	Renaming libiw30-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb to libiw30-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.udeb
 dpkg-genbuildinfo -O../wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.buildinfo
 dpkg-genchanges -O../wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ cd ..
+ ls -la
total 456
drwxr-xr-x 3 root root   4096 Apr  3 15:17 .
drwxr-xr-x 3 root root   4096 Apr  3 15:17 ..
-rw-r--r-- 1 root root  26759 Apr  3 15:17 buildlog.txt
-rw-r--r-- 1 root root  36700 Apr  3 15:17 libiw-dev_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb
-rw-r--r-- 1 root root  13164 Apr  3 15:17 libiw30-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.udeb
-rw-r--r-- 1 root root  18530 Apr  3 15:17 libiw30_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb
drwxr-xr-x 7 root root   4096 Apr  3 15:17 wireless-tools-30~pre9
-rw-r--r-- 1 root root  24880 Apr  3 15:17 wireless-tools-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.udeb
-rw-r--r-- 1 root root   1127 Apr  3 15:17 wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1.dsc
-rw-r--r-- 1 root root 192988 Apr  3 15:17 wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1.tar.xz
-rw-r--r-- 1 root root   8125 Apr  3 15:17 wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.buildinfo
-rw-r--r-- 1 root root   3794 Apr  3 15:17 wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.changes
-rw-r--r-- 1 root root 109770 Apr  3 15:17 wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb
+ find . -maxdepth 1 -type f
+ sha256sum ./libiw30-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.udeb ./wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb ./wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1.tar.xz ./buildlog.txt ./wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1.dsc ./libiw-dev_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb ./wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.changes ./wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.buildinfo ./wireless-tools-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.udeb ./libiw30_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb
5b0a3a699b40d2960e044be42a7a3fcbd87ba887c175aae2871df5ef4e8150ab  ./libiw30-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.udeb
0fc765f1bbd0fb86cfd0c50ea5a643ed54c6192da4c76414794858b5813c69be  ./wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb
7c3f6d0fef166e3bea2a87e2376f16ce5201e69fe5654740850c73b6230ad38c  ./wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1.tar.xz
cabd4c09d681c8c777813bd500ec1c83ed660432fd7f8a6de9f2adcf46b6cfa0  ./buildlog.txt
70bb36d2d7b999a4f93c9bcab10dd7a371edc6df16c6173c90aab4783e75888d  ./wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1.dsc
d0a2c1628102eff736f4e6edf1cbf299ddf737b88cb679052e59efe3b49fbdf6  ./libiw-dev_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb
db8fb61a3f5acb9a3aeb8086033578b44470473e473fbfba2158dcaf976122c2  ./wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.changes
73d2991644b7ce4c67ef21e0ff3c02cfc1fc5e047caf81f3fcb0fb938db1b877  ./wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.buildinfo
539b393f086ececa83add7cd3bd57670a6c502993fea8f5a3827ec7ee98ad977  ./wireless-tools-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.udeb
9c5402f4d47f7e2f2b0e930bc83589fa4298f61e0d160444e2be012599c10922  ./libiw30_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb
+ mkdir published
+ cd published
+ cd ../
+ ls libiw-dev_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb libiw30_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb libiw30-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.udeb wireless-tools-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.udeb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/w/wireless-tools/libiw-dev_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/w/wireless-tools/libiw30-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.udeb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/w/wireless-tools/libiw30_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/w/wireless-tools/wireless-tools-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.udeb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/w/wireless-tools/wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./libiw30-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.udeb ./wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb ./libiw-dev_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb ./wireless-tools-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.udeb ./libiw30_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb
5b0a3a699b40d2960e044be42a7a3fcbd87ba887c175aae2871df5ef4e8150ab  ./libiw30-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.udeb
0fc765f1bbd0fb86cfd0c50ea5a643ed54c6192da4c76414794858b5813c69be  ./wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb
d0a2c1628102eff736f4e6edf1cbf299ddf737b88cb679052e59efe3b49fbdf6  ./libiw-dev_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb
539b393f086ececa83add7cd3bd57670a6c502993fea8f5a3827ec7ee98ad977  ./wireless-tools-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.udeb
50782017ab4d47d71db464b411e486a378f3e412af82390796ac972ebbc5823b  ./libiw30_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./libiw30-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.udeb: OK
./wireless-tools_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb: OK
./libiw-dev_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb: OK
./wireless-tools-udeb_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.udeb: OK
./libiw30_30~pre9-13.1ubuntu4+11.0trisquel1_amd64.deb: FAILED
sha256sum: WARNING: 1 computed checksum did NOT match
