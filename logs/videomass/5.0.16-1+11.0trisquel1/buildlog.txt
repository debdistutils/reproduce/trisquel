+ date
Sat Jul  6 17:50:04 UTC 2024
+ id
uid=0(root) gid=0(root) groups=0(root)
+ apt-get source --only-source videomass=5.0.16-1+11.0trisquel1
Reading package lists...
Need to get 674 kB of source archives.
Get:1 http://ftp.acc.umu.se/mirror/trisquel/packages aramo-backports/main videomass 5.0.16-1+11.0trisquel1 (dsc) [1639 B]
Get:2 http://ftp.acc.umu.se/mirror/trisquel/packages aramo-backports/main videomass 5.0.16-1+11.0trisquel1 (tar) [672 kB]
dpkg-source: info: extracting videomass in videomass-5.0.16
dpkg-source: info: unpacking videomass_5.0.16-1+11.0trisquel1.tar.xz
Fetched 674 kB in 0s (3784 kB/s)
W: Download is performed unsandboxed as root as file 'videomass_5.0.16-1+11.0trisquel1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source videomass=5.0.16-1+11.0trisquel1
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-python dh-strip-nondeterminism dwz libdebhelper-perl
  libfile-stripnondeterminism-perl libsub-override-perl libtool m4 po-debconf
  pybuild-plugin-pyproject python3-all python3-build python3-hatchling
  python3-importlib-metadata python3-installer python3-more-itertools
  python3-pathspec python3-pep517 python3-pluggy python3-toml python3-tomli
  python3-wheel python3-zipp
0 upgraded, 30 newly installed, 0 to remove and 287 not upgraded.
Need to get 3523 kB of archives.
After this operation, 10.7 MB of additional disk space will be used.
Get:1 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:3 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:4 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:5 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:6 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:7 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:8 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:9 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:10 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:11 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:12 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:13 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:14 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:15 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
Get:16 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 dh-python all 5.20220403 [106 kB]
Get:17 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 python3-more-itertools all 8.10.0-2 [47.9 kB]
Get:18 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 python3-zipp all 1.0.0-3 [5440 B]
Get:19 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 python3-importlib-metadata all 4.6.4-1 [16.2 kB]
Get:20 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 python3-tomli all 1.2.2-2 [12.9 kB]
Get:21 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 python3-pep517 all 0.12.0-1 [17.5 kB]
Get:22 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 python3-toml all 0.10.2-1 [16.5 kB]
Get:23 http://ftp.acc.umu.se/mirror/trisquel/packages aramo-updates/main amd64 python3-wheel all 0.37.1-2ubuntu0.22.04.1 [32.0 kB]
Get:24 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 python3-build all 0.7.0-2 [15.7 kB]
Get:25 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 python3-installer all 0.5.0+dfsg1-2 [16.5 kB]
Get:26 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 pybuild-plugin-pyproject all 5.20220403 [1714 B]
Get:27 http://ftp.acc.umu.se/mirror/trisquel/packages aramo-updates/main amd64 python3-all amd64 3.10.6-1~22.04 [908 B]
Get:28 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 python3-pathspec all 0.9.0-1 [28.8 kB]
Get:29 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 python3-pluggy all 0.13.0-7.1 [19.0 kB]
Get:30 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 python3-hatchling all 0.15.0-1 [37.0 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3523 kB in 0s (7735 kB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123255 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../01-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../02-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../03-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../04-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../05-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../06-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../07-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../08-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../09-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../10-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../11-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../12-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../13-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../14-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Selecting previously unselected package dh-python.
Preparing to unpack .../15-dh-python_5.20220403_all.deb ...
Unpacking dh-python (5.20220403) ...
Selecting previously unselected package python3-more-itertools.
Preparing to unpack .../16-python3-more-itertools_8.10.0-2_all.deb ...
Unpacking python3-more-itertools (8.10.0-2) ...
Selecting previously unselected package python3-zipp.
Preparing to unpack .../17-python3-zipp_1.0.0-3_all.deb ...
Unpacking python3-zipp (1.0.0-3) ...
Selecting previously unselected package python3-importlib-metadata.
Preparing to unpack .../18-python3-importlib-metadata_4.6.4-1_all.deb ...
Unpacking python3-importlib-metadata (4.6.4-1) ...
Selecting previously unselected package python3-tomli.
Preparing to unpack .../19-python3-tomli_1.2.2-2_all.deb ...
Unpacking python3-tomli (1.2.2-2) ...
Selecting previously unselected package python3-pep517.
Preparing to unpack .../20-python3-pep517_0.12.0-1_all.deb ...
Unpacking python3-pep517 (0.12.0-1) ...
Selecting previously unselected package python3-toml.
Preparing to unpack .../21-python3-toml_0.10.2-1_all.deb ...
Unpacking python3-toml (0.10.2-1) ...
Selecting previously unselected package python3-wheel.
Preparing to unpack .../22-python3-wheel_0.37.1-2ubuntu0.22.04.1_all.deb ...
Unpacking python3-wheel (0.37.1-2ubuntu0.22.04.1) ...
Selecting previously unselected package python3-build.
Preparing to unpack .../23-python3-build_0.7.0-2_all.deb ...
Unpacking python3-build (0.7.0-2) ...
Selecting previously unselected package python3-installer.
Preparing to unpack .../24-python3-installer_0.5.0+dfsg1-2_all.deb ...
Unpacking python3-installer (0.5.0+dfsg1-2) ...
Selecting previously unselected package pybuild-plugin-pyproject.
Preparing to unpack .../25-pybuild-plugin-pyproject_5.20220403_all.deb ...
Unpacking pybuild-plugin-pyproject (5.20220403) ...
Selecting previously unselected package python3-all.
Preparing to unpack .../26-python3-all_3.10.6-1~22.04_amd64.deb ...
Unpacking python3-all (3.10.6-1~22.04) ...
Selecting previously unselected package python3-pathspec.
Preparing to unpack .../27-python3-pathspec_0.9.0-1_all.deb ...
Unpacking python3-pathspec (0.9.0-1) ...
Selecting previously unselected package python3-pluggy.
Preparing to unpack .../28-python3-pluggy_0.13.0-7.1_all.deb ...
Unpacking python3-pluggy (0.13.0-7.1) ...
Selecting previously unselected package python3-hatchling.
Preparing to unpack .../29-python3-hatchling_0.15.0-1_all.deb ...
Unpacking python3-hatchling (0.15.0-1) ...
Setting up dh-python (5.20220403) ...
Setting up python3-more-itertools (8.10.0-2) ...
Setting up python3-tomli (1.2.2-2) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up python3-all (3.10.6-1~22.04) ...
Setting up python3-zipp (1.0.0-3) ...
Setting up python3-wheel (0.37.1-2ubuntu0.22.04.1) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up python3-toml (0.10.2-1) ...
Setting up python3-installer (0.5.0+dfsg1-2) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up python3-pathspec (0.9.0-1) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up python3-importlib-metadata (4.6.4-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up dh-autoreconf (20) ...
Setting up python3-pep517 (0.12.0-1) ...
Setting up python3-pluggy (0.13.0-7.1) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up python3-hatchling (0.15.0-1) ...
Setting up python3-build (0.7.0-2) ...
Setting up pybuild-plugin-pyproject (5.20220403) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name videomass* -type d
+ cd ./videomass-5.0.16
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package videomass
dpkg-buildpackage: info: source version 5.0.16-1+11.0trisquel1
dpkg-buildpackage: info: source distribution aramo-backports
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --with python3 --buildsystem=pybuild
   dh_auto_clean -O--buildsystem=pybuild
   dh_autoreconf_clean -O--buildsystem=pybuild
   dh_clean -O--buildsystem=pybuild
 dpkg-source -b .
dpkg-source: warning: native package version may not have a revision
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building videomass in videomass_5.0.16-1+11.0trisquel1.tar.xz
dpkg-source: info: building videomass in videomass_5.0.16-1+11.0trisquel1.dsc
 debian/rules build
dh build --with python3 --buildsystem=pybuild
   dh_update_autotools_config -O--buildsystem=pybuild
   dh_autoreconf -O--buildsystem=pybuild
   dh_auto_configure -O--buildsystem=pybuild
   dh_auto_build -O--buildsystem=pybuild
I: pybuild plugin_pyproject:107: Building wheel for python3.10 with "build" module
I: pybuild base:239: python3.10 -m build --skip-dependency-check --no-isolation --wheel --outdir /build/videomass/videomass-5.0.16/.pybuild/cpython3_3.10_videomass 
* Building wheel...
Successfully built videomass-5.0.16-py3-none-any.whl
I: pybuild plugin_pyproject:118: Unpacking wheel built for python3.10 with "installer" module
   dh_auto_test -O--buildsystem=pybuild
I: pybuild base:239: cd /build/videomass/videomass-5.0.16/.pybuild/cpython3_3.10_videomass/build; python3.10 -m unittest discover -v 

----------------------------------------------------------------------
Ran 0 tests in 0.000s

OK
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary --with python3 --buildsystem=pybuild
   dh_testroot -O--buildsystem=pybuild
   dh_prep -O--buildsystem=pybuild
   dh_auto_install --destdir=debian/videomass/ -O--buildsystem=pybuild
I: pybuild plugin_pyproject:158: Copying package built for python3.10 to destdir
   dh_installdocs -O--buildsystem=pybuild
   dh_installchangelogs -O--buildsystem=pybuild
   dh_python3 -O--buildsystem=pybuild
   dh_installsystemduser -O--buildsystem=pybuild
   dh_perl -O--buildsystem=pybuild
   dh_link -O--buildsystem=pybuild
   dh_strip_nondeterminism -O--buildsystem=pybuild
	Normalized debian/videomass/usr/lib/python3/dist-packages/videomass/data/locale/nl_NL/LC_MESSAGES/videomass.mo
	Normalized debian/videomass/usr/lib/python3/dist-packages/videomass/data/locale/zh_CN/LC_MESSAGES/videomass.mo
	Normalized debian/videomass/usr/lib/python3/dist-packages/videomass/data/locale/en_US/LC_MESSAGES/videomass.mo
	Normalized debian/videomass/usr/lib/python3/dist-packages/videomass/data/locale/fr_FR/LC_MESSAGES/videomass.mo
	Normalized debian/videomass/usr/lib/python3/dist-packages/videomass/data/locale/es_MX/LC_MESSAGES/videomass.mo
	Normalized debian/videomass/usr/lib/python3/dist-packages/videomass/data/locale/it_IT/LC_MESSAGES/videomass.mo
	Normalized debian/videomass/usr/lib/python3/dist-packages/videomass/data/locale/es_CU/LC_MESSAGES/videomass.mo
	Normalized debian/videomass/usr/lib/python3/dist-packages/videomass/data/locale/es_ES/LC_MESSAGES/videomass.mo
	Normalized debian/videomass/usr/lib/python3/dist-packages/videomass/data/locale/pt_BR/LC_MESSAGES/videomass.mo
	Normalized debian/videomass/usr/lib/python3/dist-packages/videomass/data/locale/ru_RU/LC_MESSAGES/videomass.mo
   dh_compress -O--buildsystem=pybuild
   dh_fixperms -O--buildsystem=pybuild
   dh_missing -O--buildsystem=pybuild
   dh_installdeb -O--buildsystem=pybuild
   dh_gencontrol -O--buildsystem=pybuild
   dh_md5sums -O--buildsystem=pybuild
   dh_builddeb -O--buildsystem=pybuild
pkgstripfiles: processing control file: debian/videomass/DEBIAN/control, package videomass, directory debian/videomass
pkgstripfiles: Truncating usr/share/doc/videomass/changelog.Debian.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 40 cpus) for package videomass ...
xargs: warning: options --max-lines and --replace/-I/-i are mutually exclusive, ignoring previous --max-lines value
ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
pkgstripfiles: PNG optimization (153/0) for package videomass took 3 s
dpkg-deb: building package 'videomass' in '../videomass_5.0.16-1+11.0trisquel1_all.deb'.
 dpkg-genbuildinfo -O../videomass_5.0.16-1+11.0trisquel1_amd64.buildinfo
 dpkg-genchanges -O../videomass_5.0.16-1+11.0trisquel1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Sat Jul  6 17:50:39 UTC 2024
+ cd ..
+ ls -la
total 1352
drwxr-xr-x  3 root root   4096 Jul  6 17:50 .
drwxr-xr-x  3 root root   4096 Jul  6 17:50 ..
-rw-r--r--  1 root root  17199 Jul  6 17:50 buildlog.txt
drwxr-xr-x 10 root root   4096 Jul  6 17:50 videomass-5.0.16
-rw-r--r--  1 root root    756 Jul  6 17:50 videomass_5.0.16-1+11.0trisquel1.dsc
-rw-r--r--  1 root root 672084 Jul  6 17:50 videomass_5.0.16-1+11.0trisquel1.tar.xz
-rw-r--r--  1 root root 658484 Jul  6 17:50 videomass_5.0.16-1+11.0trisquel1_all.deb
-rw-r--r--  1 root root   7793 Jul  6 17:50 videomass_5.0.16-1+11.0trisquel1_amd64.buildinfo
-rw-r--r--  1 root root   1792 Jul  6 17:50 videomass_5.0.16-1+11.0trisquel1_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./buildlog.txt ./videomass_5.0.16-1+11.0trisquel1.dsc ./videomass_5.0.16-1+11.0trisquel1.tar.xz ./videomass_5.0.16-1+11.0trisquel1_amd64.changes ./videomass_5.0.16-1+11.0trisquel1_all.deb ./videomass_5.0.16-1+11.0trisquel1_amd64.buildinfo
72fa74f5ddf26ee3ee4171730520b1d611b0275ac2278496cc71a8b7e35631d3  ./buildlog.txt
515d43ee85b9b618bfeb6e5ecb376c23fde6e4293f7777a32fe02b5fbfea2ceb  ./videomass_5.0.16-1+11.0trisquel1.dsc
cb4cd9581818ce3b0cdd01a421a33a18ceff5bcc1f6b81795cdedc6e13cdf11f  ./videomass_5.0.16-1+11.0trisquel1.tar.xz
8f452c0c1406efbd60a303e32552fc43acb0724b577df486cc7318fecd34803c  ./videomass_5.0.16-1+11.0trisquel1_amd64.changes
97a77c3b9f7e9ef4e89eae903263266f4e8595775206b9c09b72502ac2050758  ./videomass_5.0.16-1+11.0trisquel1_all.deb
517173fce96ae9970a152d14e5dfd2747074e4e55cba127b9c28c3c23d523fe4  ./videomass_5.0.16-1+11.0trisquel1_amd64.buildinfo
+ mkdir published
+ cd published
+ cd ../
+ ls videomass_5.0.16-1+11.0trisquel1_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://ftp.acc.umu.se/mirror/trisquel/packages/pool/main/v/videomass/videomass_5.0.16-1+11.0trisquel1_all.deb
--2024-07-06 17:50:39--  http://ftp.acc.umu.se/mirror/trisquel/packages/pool/main/v/videomass/videomass_5.0.16-1+11.0trisquel1_all.deb
Resolving ftp.acc.umu.se (ftp.acc.umu.se)... 194.71.11.173, 194.71.11.163, 194.71.11.165, ...
Connecting to ftp.acc.umu.se (ftp.acc.umu.se)|194.71.11.173|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 658484 (643K) [application/x-debian-package]
Saving to: 'videomass_5.0.16-1+11.0trisquel1_all.deb'

     0K .......... .......... .......... .......... ..........  7% 2.37M 0s
    50K .......... .......... .......... .......... .......... 15% 4.64M 0s
   100K .......... .......... .......... .......... .......... 23% 9.00M 0s
   150K .......... .......... .......... .......... .......... 31% 8.65M 0s
   200K .......... .......... .......... .......... .......... 38% 18.8M 0s
   250K .......... .......... .......... .......... .......... 46% 16.4M 0s
   300K .......... .......... .......... .......... .......... 54% 18.7M 0s
   350K .......... .......... .......... .......... .......... 62% 25.3M 0s
   400K .......... .......... .......... .......... .......... 69% 18.7M 0s
   450K .......... .......... .......... .......... .......... 77% 24.2M 0s
   500K .......... .......... .......... .......... .......... 85% 29.3M 0s
   550K .......... .......... .......... .......... .......... 93% 21.1M 0s
   600K .......... .......... .......... .......... ...       100% 45.4M=0.06s

2024-07-06 17:50:40 (10.2 MB/s) - 'videomass_5.0.16-1+11.0trisquel1_all.deb' saved [658484/658484]

+ + tee ../SHA256SUMS
find . -maxdepth 1 -type f
+ sha256sum ./videomass_5.0.16-1+11.0trisquel1_all.deb
97a77c3b9f7e9ef4e89eae903263266f4e8595775206b9c09b72502ac2050758  ./videomass_5.0.16-1+11.0trisquel1_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./videomass_5.0.16-1+11.0trisquel1_all.deb: OK
+ echo Package videomass version 5.0.16-1+11.0trisquel1 is reproducible!
Package videomass version 5.0.16-1+11.0trisquel1 is reproducible!
