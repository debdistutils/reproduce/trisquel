+ date
Wed Feb 14 06:40:41 UTC 2024
+ id
uid=0(root) gid=0(root) groups=0(root)
+ apt-get source --only-source distro-info-data=0.52ubuntu0.6+11.0trisquel7
Reading package lists...
NOTICE: 'distro-info-data' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/debian/distro-info-data.git
Please use:
git clone https://salsa.debian.org/debian/distro-info-data.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 12.2 kB of source archives.
Get:1 http://ftp.acc.umu.se/mirror/trisquel/packages aramo-updates/main distro-info-data 0.52ubuntu0.6+11.0trisquel7 (dsc) [1862 B]
Get:2 http://ftp.acc.umu.se/mirror/trisquel/packages aramo-updates/main distro-info-data 0.52ubuntu0.6+11.0trisquel7 (tar) [10.3 kB]
dpkg-source: info: extracting distro-info-data in distro-info-data-0.52ubuntu0.6+11.0trisquel7
dpkg-source: info: unpacking distro-info-data_0.52ubuntu0.6+11.0trisquel7.tar.xz
Fetched 12.2 kB in 0s (126 kB/s)
W: Download is performed unsandboxed as root as file 'distro-info-data_0.52ubuntu0.6+11.0trisquel7.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source distro-info-data=0.52ubuntu0.6+11.0trisquel7
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-strip-nondeterminism dwz libdebhelper-perl
  libfile-stripnondeterminism-perl libsub-override-perl libtool m4 po-debconf
0 upgraded, 15 newly installed, 0 to remove and 241 not upgraded.
Need to get 3149 kB of archives.
After this operation, 8859 kB of additional disk space will be used.
Get:1 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:3 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:4 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:5 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:6 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:7 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:8 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:9 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:10 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:11 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:12 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:13 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:14 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:15 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3149 kB in 0s (15.6 MB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123255 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../01-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../02-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../03-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../04-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../05-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../06-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../07-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../08-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../09-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../10-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../11-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../12-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../13-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../14-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name distro-info-data* -type d
+ cd ./distro-info-data-0.52ubuntu0.6+11.0trisquel7
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package distro-info-data
dpkg-buildpackage: info: source version 0.52ubuntu0.6+11.0trisquel7
dpkg-buildpackage: info: source distribution aramo
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   dh_auto_clean
   dh_clean
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building distro-info-data in distro-info-data_0.52ubuntu0.6+11.0trisquel7.tar.xz
dpkg-source: info: building distro-info-data in distro-info-data_0.52ubuntu0.6+11.0trisquel7.dsc
 debian/rules binary
dh binary
   dh_update_autotools_config
   dh_autoreconf
   dh_auto_configure
   dh_auto_build
	make -j40 "INSTALL=install --strip-program=true"
make[1]: Entering directory '/build/distro-info-data/distro-info-data-0.52ubuntu0.6+11.0trisquel7'
make[1]: Nothing to be done for 'build'.
make[1]: Leaving directory '/build/distro-info-data/distro-info-data-0.52ubuntu0.6+11.0trisquel7'
   dh_auto_test
	make -j40 test
make[1]: Entering directory '/build/distro-info-data/distro-info-data-0.52ubuntu0.6+11.0trisquel7'
./validate-csv-data -d debian.csv
./validate-csv-data -u ubuntu.csv
make[1]: Leaving directory '/build/distro-info-data/distro-info-data-0.52ubuntu0.6+11.0trisquel7'
   create-stamp debian/debhelper-build-stamp
   dh_prep
   dh_auto_install --destdir=debian/distro-info-data/
	make -j40 install DESTDIR=/build/distro-info-data/distro-info-data-0.52ubuntu0.6\+11.0trisquel7/debian/distro-info-data AM_UPDATE_INFO_DIR=no "INSTALL=install --strip-program=true"
make[1]: Entering directory '/build/distro-info-data/distro-info-data-0.52ubuntu0.6+11.0trisquel7'
install -d /build/distro-info-data/distro-info-data-0.52ubuntu0.6+11.0trisquel7/debian/distro-info-data/usr/share/distro-info
install -m 644 debian.csv trisquel.csv ubuntu.csv /build/distro-info-data/distro-info-data-0.52ubuntu0.6+11.0trisquel7/debian/distro-info-data/usr/share/distro-info
make[1]: Leaving directory '/build/distro-info-data/distro-info-data-0.52ubuntu0.6+11.0trisquel7'
   dh_installdocs
   dh_installchangelogs
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
pkgstripfiles: processing control file: debian/distro-info-data/DEBIAN/control, package distro-info-data, directory debian/distro-info-data
pkgstripfiles: Truncating usr/share/doc/distro-info-data/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 40 cpus) for package distro-info-data ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'distro-info-data' in '../distro-info-data_0.52ubuntu0.6+11.0trisquel7_all.deb'.
 dpkg-genbuildinfo -O../distro-info-data_0.52ubuntu0.6+11.0trisquel7_amd64.buildinfo
 dpkg-genchanges -O../distro-info-data_0.52ubuntu0.6+11.0trisquel7_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Wed Feb 14 06:40:53 UTC 2024
+ cd ..
+ ls -la
total 60
drwxr-xr-x 3 root root  4096 Feb 14 06:40 .
drwxr-xr-x 3 root root  4096 Feb 14 06:40 ..
-rw-r--r-- 1 root root 10749 Feb 14 06:40 buildlog.txt
drwxr-xr-x 4 root root  4096 Jan  3 00:38 distro-info-data-0.52ubuntu0.6+11.0trisquel7
-rw-r--r-- 1 root root   979 Feb 14 06:40 distro-info-data_0.52ubuntu0.6+11.0trisquel7.dsc
-rw-r--r-- 1 root root 10336 Feb 14 06:40 distro-info-data_0.52ubuntu0.6+11.0trisquel7.tar.xz
-rw-r--r-- 1 root root  5584 Feb 14 06:40 distro-info-data_0.52ubuntu0.6+11.0trisquel7_all.deb
-rw-r--r-- 1 root root  7274 Feb 14 06:40 distro-info-data_0.52ubuntu0.6+11.0trisquel7_amd64.buildinfo
-rw-r--r-- 1 root root  2020 Feb 14 06:40 distro-info-data_0.52ubuntu0.6+11.0trisquel7_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./distro-info-data_0.52ubuntu0.6+11.0trisquel7_amd64.buildinfo ./buildlog.txt ./distro-info-data_0.52ubuntu0.6+11.0trisquel7_amd64.changes ./distro-info-data_0.52ubuntu0.6+11.0trisquel7_all.deb ./distro-info-data_0.52ubuntu0.6+11.0trisquel7.dsc ./distro-info-data_0.52ubuntu0.6+11.0trisquel7.tar.xz
47d23782a97feb4800c7fcfc8f0bca2915e4e2367dc498337d09f31f270227c5  ./distro-info-data_0.52ubuntu0.6+11.0trisquel7_amd64.buildinfo
e963800e64632b9f23658988de6df41909fac39c316c10279157980e00b0117a  ./buildlog.txt
6ce4a28ec47d2fb70df9c5253daea001a8714cff6db32c2787393817b73828db  ./distro-info-data_0.52ubuntu0.6+11.0trisquel7_amd64.changes
5ef05119648259221fcce38e06547819b5ae2567ecd5ec96a9ea870cdcac3b48  ./distro-info-data_0.52ubuntu0.6+11.0trisquel7_all.deb
acd1364acaa8b715741e98eaf49b9ea7b2edc13e582f0916ff5c98a574d28915  ./distro-info-data_0.52ubuntu0.6+11.0trisquel7.dsc
49228e1877899983a440b55d89fa71c99d31dac5091284c689838a530a6d3f32  ./distro-info-data_0.52ubuntu0.6+11.0trisquel7.tar.xz
+ mkdir published
+ cd published
+ cd ../
+ ls distro-info-data_0.52ubuntu0.6+11.0trisquel7_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://ftp.acc.umu.se/mirror/trisquel/packages/pool/main/d/distro-info-data/distro-info-data_0.52ubuntu0.6+11.0trisquel7_all.deb
--2024-02-14 06:40:53--  http://ftp.acc.umu.se/mirror/trisquel/packages/pool/main/d/distro-info-data/distro-info-data_0.52ubuntu0.6+11.0trisquel7_all.deb
Resolving ftp.acc.umu.se (ftp.acc.umu.se)... 194.71.11.163, 194.71.11.165, 194.71.11.173, ...
Connecting to ftp.acc.umu.se (ftp.acc.umu.se)|194.71.11.163|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 5584 (5.5K) [application/x-debian-package]
Saving to: 'distro-info-data_0.52ubuntu0.6+11.0trisquel7_all.deb'

     0K .....                                                 100%  164M=0s

2024-02-14 06:40:53 (164 MB/s) - 'distro-info-data_0.52ubuntu0.6+11.0trisquel7_all.deb' saved [5584/5584]

+ + tee ../SHA256SUMS
find . -maxdepth 1 -type f
+ sha256sum ./distro-info-data_0.52ubuntu0.6+11.0trisquel7_all.deb
5ef05119648259221fcce38e06547819b5ae2567ecd5ec96a9ea870cdcac3b48  ./distro-info-data_0.52ubuntu0.6+11.0trisquel7_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./distro-info-data_0.52ubuntu0.6+11.0trisquel7_all.deb: OK
+ echo Package distro-info-data version 0.52ubuntu0.6+11.0trisquel7 is reproducible!
Package distro-info-data version 0.52ubuntu0.6+11.0trisquel7 is reproducible!
