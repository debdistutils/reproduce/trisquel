+ date
Mon Apr  3 13:24:32 UTC 2023
+ apt-get source --only-source localechooser
Reading package lists...
NOTICE: 'localechooser' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/installer-team/localechooser.git
Please use:
git clone https://salsa.debian.org/installer-team/localechooser.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 184 kB of source archives.
Get:1 http://archive.trisquel.info/trisquel aramo/main localechooser 2.93+11.0trisquel2 (dsc) [1730 B]
Get:2 http://archive.trisquel.info/trisquel aramo/main localechooser 2.93+11.0trisquel2 (tar) [182 kB]
dpkg-source: info: extracting localechooser in localechooser-2.93+11.0trisquel2
dpkg-source: info: unpacking localechooser_2.93+11.0trisquel2.tar.xz
Fetched 184 kB in 0s (1167 kB/s)
W: Download is performed unsandboxed as root as file 'localechooser_2.93+11.0trisquel2.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source localechooser
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-strip-nondeterminism dwz isoquery libdebhelper-perl
  libfile-stripnondeterminism-perl libsub-override-perl libtool m4 po-debconf
0 upgraded, 16 newly installed, 0 to remove and 1 not upgraded.
Need to get 3184 kB of archives.
After this operation, 9006 kB of additional disk space will be used.
Get:1 http://archive.trisquel.info/trisquel aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://archive.trisquel.info/trisquel aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:3 http://archive.trisquel.info/trisquel aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:4 http://archive.trisquel.info/trisquel aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:5 http://archive.trisquel.info/trisquel aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:6 http://archive.trisquel.info/trisquel aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:7 http://archive.trisquel.info/trisquel aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:8 http://archive.trisquel.info/trisquel aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:9 http://archive.trisquel.info/trisquel aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:10 http://archive.trisquel.info/trisquel aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:11 http://archive.trisquel.info/trisquel aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:12 http://archive.trisquel.info/trisquel aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:13 http://archive.trisquel.info/trisquel aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:14 http://archive.trisquel.info/trisquel aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:15 http://archive.trisquel.info/trisquel aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
Get:16 http://archive.trisquel.info/trisquel aramo/main amd64 isoquery amd64 3.2.6-1 [34.3 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3184 kB in 0s (9965 kB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123238 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../01-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../02-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../03-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../04-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../05-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../06-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../07-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../08-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../09-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../10-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../11-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../12-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../13-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../14-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Selecting previously unselected package isoquery.
Preparing to unpack .../15-isoquery_3.2.6-1_amd64.deb ...
Unpacking isoquery (3.2.6-1) ...
Setting up isoquery (3.2.6-1) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name localechooser* -type d
+ cd ./localechooser-2.93+11.0trisquel2
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package localechooser
dpkg-buildpackage: info: source version 2.93+11.0trisquel2
dpkg-buildpackage: info: source distribution aramo
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_auto_clean
dh_auto_clean: warning: Compatibility levels before 10 are deprecated (level 9 in use)
	make -j1 clean
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/localechooser/localechooser-2.93+11.0trisquel2'
rm -f localechooser.templates debian/templates debian/templates.continents debian/templates.base-in debian/templates.base debian/templates.tmp debian/languagelist.data SUPPORTED-short debian/iso_3166.tab
rm -f -rf debian/short-tmp debian/iso-codes
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/localechooser/localechooser-2.93+11.0trisquel2'
   debian/rules override_dh_clean
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/localechooser/localechooser-2.93+11.0trisquel2'
rm -rf debian/pobuild debian/iso-codes debian/short-tmp
rm -rf debian/locales debian/sort-tmp
dh_clean
dh_clean: warning: Compatibility levels before 10 are deprecated (level 9 in use)
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/localechooser/localechooser-2.93+11.0trisquel2'
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building localechooser in localechooser_2.93+11.0trisquel2.tar.xz
dpkg-source: warning: missing information for output field Standards-Version
dpkg-source: info: building localechooser in localechooser_2.93+11.0trisquel2.dsc
 debian/rules build
dh build
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_update_autotools_config
   dh_auto_configure
dh_auto_configure: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_auto_build
dh_auto_build: warning: Compatibility levels before 10 are deprecated (level 9 in use)
	make -j1
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/localechooser/localechooser-2.93+11.0trisquel2'
Checking syntax of finish-install.d/05localechooser
Checking syntax of post-base-installer.d/05localechooser
Checking syntax of debian/postinst
Checking syntax of languagemap
Checking syntax of localechooser
LC_COLLATE=C ./mklanguagelist.data languagelist debian/languagelist.data
./get-SUPPORTED
isoquery -c | cut -f 1,4 | sort >debian/iso_3166.tab
[ -s debian/iso_3166.tab ]
./get-iso-codes
./mkshort
Building short lists of countries...
ar
bn
bo
ca
de
el
es
eu
fr
it
nl
pa
pt
pt_BR
ru
sq
sr
sv
ta
tr
zh_CN
zh_TW
en
./mktemplates.continents debian/iso_3166.tab regionmap debian/templates.continents
I: skipping country code 'AN': in regionmap but not in debian/iso_3166.tab
W: unknown region for country CW: not listed in regionmap
cat debian/localechooser.templates-in debian/templates.continents >debian/templates.base-in
./mktemplates.base debian/templates.base-in debian/templates.base
am.po ............................................... done.
ar.po ................................ done.
ast.po .................................. done.
be.po ................................ done.
bg.po .................................. done.
bn.po ................................ done.
bo.po bs.po .................................. done.
ca.po ................................. done.
cs.po ................................ done.
cy.po ................................ done.
da.po ................................ done.
de.po .............................. done.
dz.po .................................... done.
el.po ................................ done.
eo.po .............................. done.
es.po ................................ done.
et.po .............................. done.
eu.po ................................ done.
fa.po ................................ done.
fi.po .................................. done.
fr.po .............................. done.
ga.po .................................. done.
gl.po .................................. done.
gu.po .............................. done.
he.po .............................. done.
hi.po .............................. done.
hr.po .............................. done.
hu.po ................................ done.
id.po .............................. done.
is.po .............................. done.
it.po ................................ done.
ja.po ................................. done.
ka.po .................................... done.
kab.po ......................................................... done.
kk.po .................................. done.
km.po ................................. done.
kn.po ................................... done.
ko.po ................................ done.
ku.po lo.po .............................................. done.
lt.po .................................. done.
lv.po .................................. done.
mk.po ................................... done.
ml.po .................................... done.
mr.po ................................ done.
nb.po .............................. done.
ne.po ................................... done.
nl.po .............................. done.
nn.po ................................... done.
oc.po ............................... done.
pa.po ................................ done.
pl.po ................................ done.
pt.po ................................ done.
pt_BR.po .............................. done.
ro.po ................................. done.
ru.po .............................. done.
se.po .............................. done.
si.po .................................. done.
sk.po ................................. done.
sl.po .................................. done.
sq.po ................................ done.
sr.po ................................ done.
sv.po .............................. done.
ta.po .................................... done.
te.po ................................... done.
tg.po ................................ done.
th.po .................................. done.
tl.po .................................. done.
tr.po .............................. done.
ug.po ................................ done.
uk.po .............................. done.
vi.po .................................. done.
zh_CN.po .............................. done.
zh_TW.po ................................ done.

WARNING: se: spurious newline removed
WARNING: se: spurious newline removed
WARNING: se: spurious newline removed
WARNING: se: spurious newline removed
WARNING: se: spurious newline removed
WARNING: se: spurious newline removed
WARNING: se: spurious newline removed
WARNING: se: spurious newline removed
WARNING: se: spurious newline removed
./mktemplates.shortlist
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
ar
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
bn
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
bo
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
ca
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
de
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
el
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
en
es
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
eu
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
fr
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
it
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
nl
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
pa
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
pt
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
pt_BR
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
ru
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
sq
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
sr
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
sv
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
ta
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
tr
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
zh_CN
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
zh_TW
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
awk: template_get_item.awk:18: warning: regexp escape sequence `\ ' is not a known regexp operator
./mktemplates.warnings languagelist debian/templates
Appending language-specific templates about incomplete translations...
Warning: no localized warnings for zh_TW in file!
# Check that the listed locale is supported, to make sure it will work.
iconv -f utf-8 -t unicode languagelist > /dev/null
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/localechooser/localechooser-2.93+11.0trisquel2'
   dh_auto_test
dh_auto_test: warning: Compatibility levels before 10 are deprecated (level 9 in use)
 debian/rules binary
dh binary
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_testroot
   dh_prep
   dh_installdirs
dh_installdirs: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_auto_install --destdir=debian/localechooser/
dh_auto_install: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   debian/rules override_dh_install
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/localechooser/localechooser-2.93+11.0trisquel2'
dh_install
dh_install: warning: Compatibility levels before 10 are deprecated (level 9 in use)
cat languagelist | grep -v "^#.*" | cut -d";" -f1,4- \
	>debian/localechooser/usr/share/localechooser/languagelist
chmod 0644 debian/localechooser/usr/share/localechooser/languagelist
gzip -c debian/languagelist.data \
	>debian/localechooser/usr/share/localechooser/languagelist.data.gz
chmod 0644 debian/localechooser/usr/share/localechooser/languagelist.data.gz
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/localechooser/localechooser-2.93+11.0trisquel2'
   dh_installdocs
dh_installdocs: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installchangelogs
   debian/rules override_dh_installdebconf
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/localechooser/localechooser-2.93+11.0trisquel2'
dh_installdebconf
# Sort countries and continents (including translations)
# MUST be after dh_installdebconf
./sort-templates
Sorting template 'localechooser/countrylist/Africa'...
W: language 'no' skipped: no UTF-8 variant found
Sorting template 'localechooser/countrylist/Antarctica'...
I: nothing to be sorted
Sorting template 'localechooser/countrylist/Asia'...
W: language 'no' skipped: no UTF-8 variant found
Sorting template 'localechooser/countrylist/Atlantic_Ocean'...
W: language 'no' skipped: no UTF-8 variant found
Sorting template 'localechooser/countrylist/Caribbean'...
W: language 'no' skipped: no UTF-8 variant found
Sorting template 'localechooser/countrylist/Central_America'...
W: language 'no' skipped: no UTF-8 variant found
Sorting template 'localechooser/countrylist/Europe'...
W: language 'no' skipped: no UTF-8 variant found
Sorting template 'localechooser/countrylist/Indian_Ocean'...
W: language 'no' skipped: no UTF-8 variant found
Sorting template 'localechooser/countrylist/North_America'...
W: language 'no' skipped: no UTF-8 variant found
Sorting template 'localechooser/countrylist/Oceania'...
W: language 'no' skipped: no UTF-8 variant found
Sorting template 'localechooser/countrylist/South_America'...
W: language 'no' skipped: no UTF-8 variant found
Sorting template 'localechooser/countrylist/other'...
I: nothing to be sorted
Sorting template 'localechooser/continentlist'...
W: language 'no' skipped: no UTF-8 variant found
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/localechooser/localechooser-2.93+11.0trisquel2'
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
dh_compress: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_fixperms
   dh_missing
dh_missing: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_strip
dh_strip: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_makeshlibs
   dh_shlibdeps
dh_shlibdeps: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installdeb
   debian/rules override_dh_gencontrol
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/localechooser/localechooser-2.93+11.0trisquel2'
dh_gencontrol -- -Vmenuitemnum=1000
dpkg-gencontrol: warning: package localechooser: substitution variable ${misc:Depends} unused, but is defined
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/localechooser/localechooser-2.93+11.0trisquel2'
   dh_md5sums
   dh_builddeb
INFO: Disabling pkgsanitychecks for udeb
pkgstripfiles: processing control file: debian/localechooser/DEBIAN/control, package localechooser, directory debian/localechooser
pkgstripfiles: Running PNG optimization (using 1 cpus) for package localechooser ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'localechooser' in 'debian/.debhelper/scratch-space/build-localechooser/localechooser_2.93+11.0trisquel2_amd64.deb'.
	Renaming localechooser_2.93+11.0trisquel2_amd64.deb to localechooser_2.93+11.0trisquel2_amd64.udeb
 dpkg-genbuildinfo -O../localechooser_2.93+11.0trisquel2_amd64.buildinfo
 dpkg-genchanges -O../localechooser_2.93+11.0trisquel2_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: source-only upload: Debian-native package
+ cd ..
+ ls -la
total 448
drwxr-xr-x 3 root root   4096 Apr  3 13:27 .
drwxr-xr-x 3 root root   4096 Apr  3 13:24 ..
-rw-r--r-- 1 root root  24438 Apr  3 13:27 buildlog.txt
drwxr-xr-x 5 root root   4096 Dec 20  2020 localechooser-2.93+11.0trisquel2
-rw-r--r-- 1 root root    847 Apr  3 13:24 localechooser_2.93+11.0trisquel2.dsc
-rw-r--r-- 1 root root 182276 Apr  3 13:24 localechooser_2.93+11.0trisquel2.tar.xz
-rw-r--r-- 1 root root   6865 Apr  3 13:27 localechooser_2.93+11.0trisquel2_amd64.buildinfo
-rw-r--r-- 1 root root   1812 Apr  3 13:27 localechooser_2.93+11.0trisquel2_amd64.changes
-rw-r--r-- 1 root root 218508 Apr  3 13:27 localechooser_2.93+11.0trisquel2_amd64.udeb
+ find . -maxdepth 1 -type f
+ sha256sum ./localechooser_2.93+11.0trisquel2.dsc ./localechooser_2.93+11.0trisquel2_amd64.buildinfo ./buildlog.txt ./localechooser_2.93+11.0trisquel2_amd64.changes ./localechooser_2.93+11.0trisquel2_amd64.udeb ./localechooser_2.93+11.0trisquel2.tar.xz
8ad64b7f571bfb9cd6fb47fa613b7d17ae5472bc87d6c54158af44c4fa5e91d5  ./localechooser_2.93+11.0trisquel2.dsc
140e939390a2182bf8b19acfd84297f89438d726584f06fa8c3854b570ee3d72  ./localechooser_2.93+11.0trisquel2_amd64.buildinfo
55f154d0fd7531b85cd0387b7b40b3184d67d93a617b4eb0effd41762e6950de  ./buildlog.txt
30091361537aa73c646a14a58c5f77253bbbc70be18aab516a9b40b6b2b54e69  ./localechooser_2.93+11.0trisquel2_amd64.changes
47258a01799a64a4ad2137cb70a50da80c07285d982877eee7f6ab3162dcb04d  ./localechooser_2.93+11.0trisquel2_amd64.udeb
af4e2dc75e58f020dbbcdee3979a977feeaee5e9627d764306c790e4219e6f48  ./localechooser_2.93+11.0trisquel2.tar.xz
+ mkdir published
+ cd published
+ cd ../
+ ls *.deb localechooser_2.93+11.0trisquel2_amd64.udeb
ls: cannot access '*.deb': No such file or directory
+ wget -q http://archive.trisquel.info/trisquel/pool/main/l/localechooser/localechooser_2.93+11.0trisquel2_amd64.udeb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./localechooser_2.93+11.0trisquel2_amd64.udeb
47258a01799a64a4ad2137cb70a50da80c07285d982877eee7f6ab3162dcb04d  ./localechooser_2.93+11.0trisquel2_amd64.udeb
+ cd ..
+ sha256sum -c SHA256SUMS
./localechooser_2.93+11.0trisquel2_amd64.udeb: OK
+ echo Package localechooser is reproducible!
Package localechooser is reproducible!
