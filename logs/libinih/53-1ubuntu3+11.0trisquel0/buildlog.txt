+ date
Mon Apr  3 12:33:30 UTC 2023
+ apt-get source --only-source libinih
Reading package lists...
NOTICE: 'libinih' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/yangfl-guest/inih.git
Please use:
git clone https://salsa.debian.org/yangfl-guest/inih.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 25.4 kB of source archives.
Get:1 http://archive.trisquel.info/trisquel aramo/main libinih 53-1ubuntu3+11.0trisquel0 (dsc) [1975 B]
Get:2 http://archive.trisquel.info/trisquel aramo/main libinih 53-1ubuntu3+11.0trisquel0 (tar) [23.5 kB]
dpkg-source: info: extracting libinih in libinih-53
dpkg-source: info: unpacking libinih_53-1ubuntu3+11.0trisquel0.tar.xz
Fetched 25.4 kB in 0s (176 kB/s)
W: Download is performed unsandboxed as root as file 'libinih_53-1ubuntu3+11.0trisquel0.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source libinih
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-strip-nondeterminism dwz libdebhelper-perl
  libfile-stripnondeterminism-perl libsub-override-perl libtool m4 meson
  ninja-build po-debconf
0 upgraded, 17 newly installed, 0 to remove and 1 not upgraded.
Need to get 3779 kB of archives.
After this operation, 12.6 MB of additional disk space will be used.
Get:1 http://archive.trisquel.info/trisquel aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://archive.trisquel.info/trisquel aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:3 http://archive.trisquel.info/trisquel aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:4 http://archive.trisquel.info/trisquel aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:5 http://archive.trisquel.info/trisquel aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:6 http://archive.trisquel.info/trisquel aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:7 http://archive.trisquel.info/trisquel aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:8 http://archive.trisquel.info/trisquel aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:9 http://archive.trisquel.info/trisquel aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:10 http://archive.trisquel.info/trisquel aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:11 http://archive.trisquel.info/trisquel aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:12 http://archive.trisquel.info/trisquel aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:13 http://archive.trisquel.info/trisquel aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:14 http://archive.trisquel.info/trisquel aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:15 http://archive.trisquel.info/trisquel aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
Get:16 http://archive.trisquel.info/trisquel aramo/main amd64 ninja-build amd64 1.10.1-1 [111 kB]
Get:17 http://archive.trisquel.info/trisquel aramo/main amd64 meson all 0.61.2-1 [519 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3779 kB in 0s (12.3 MB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123238 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../01-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../02-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../03-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../04-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../05-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../06-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../07-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../08-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../09-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../10-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../11-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../12-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../13-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../14-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Selecting previously unselected package ninja-build.
Preparing to unpack .../15-ninja-build_1.10.1-1_amd64.deb ...
Unpacking ninja-build (1.10.1-1) ...
Selecting previously unselected package meson.
Preparing to unpack .../16-meson_0.61.2-1_all.deb ...
Unpacking meson (0.61.2-1) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up ninja-build (1.10.1-1) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up meson (0.61.2-1) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name libinih* -type d
+ cd ./libinih-53
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package libinih
dpkg-buildpackage: info: source version 53-1ubuntu3+11.0trisquel0
dpkg-buildpackage: info: source distribution aramo
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   debian/rules execute_after_dh_auto_clean
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53'
/usr/bin/make -C tests clean
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/tests'
rm -rf *.o unittest unittest_string test_*.txt
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/tests'
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53'
   dh_clean
 dpkg-source -b .
dpkg-source: warning: native package version may not have a revision
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building libinih in libinih_53-1ubuntu3+11.0trisquel0.tar.xz
dpkg-source: info: building libinih in libinih_53-1ubuntu3+11.0trisquel0.dsc
 debian/rules binary
dh binary
   dh_update_autotools_config
   dh_autoreconf
   debian/rules override_dh_auto_configure
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53'
dh_auto_configure -- \
	-Ddefault_library=both \
	-Dwith_INIReader=true \
	-Ddistro_install=true
	cd obj-x86_64-linux-gnu && LC_ALL=C.UTF-8 meson .. --wrap-mode=nodownload --buildtype=plain --prefix=/usr --sysconfdir=/etc --localstatedir=/var --libdir=lib/x86_64-linux-gnu -Ddefault_library=both -Dwith_INIReader=true -Ddistro_install=true
The Meson build system
Version: 0.61.2
Source dir: /builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53
Build dir: /builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/obj-x86_64-linux-gnu
Build type: native build
Project name: inih
Project version: undefined
C compiler for the host machine: cc (gcc 11.3.0 "cc (Ubuntu 11.3.0-1ubuntu1~22.04) 11.3.0")
C linker for the host machine: cc ld.bfd 2.38
Host machine cpu family: x86_64
Host machine cpu: x86_64
C++ compiler for the host machine: c++ (gcc 11.3.0 "c++ (Ubuntu 11.3.0-1ubuntu1~22.04) 11.3.0")
C++ linker for the host machine: c++ ld.bfd 2.38
Build targets in project: 4

inih undefined

  User defined options
    buildtype      : plain
    default_library: both
    libdir         : lib/x86_64-linux-gnu
    localstatedir  : /var
    prefix         : /usr
    sysconfdir     : /etc
    wrap_mode      : nodownload
    distro_install : true
    with_INIReader : true

Found ninja-1.10.1 at /usr/bin/ninja
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53'
   dh_auto_build
	cd obj-x86_64-linux-gnu && LC_ALL=C.UTF-8 ninja -j1 -v
[1/7] cc -Ilibinih.so.1.p -I. -I.. -fdiagnostics-color=always -D_FILE_OFFSET_BITS=64 -Wall -Winvalid-pch -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wall -pedantic -Wdate-time -D_FORTIFY_SOURCE=2 -fPIC -DINI_HANDLER_LINENO=1 -MD -MQ libinih.so.1.p/ini.c.o -MF libinih.so.1.p/ini.c.o.d -o libinih.so.1.p/ini.c.o -c ../ini.c
[2/7] cc  -o libinih.so.1 libinih.so.1.p/ini.c.o -Wl,--as-needed -Wl,--no-undefined -shared -fPIC -Wl,--start-group -Wl,-soname,libinih.so.1 -Wl,--end-group -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro -Wl,-z,now -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wall -pedantic -Wdate-time -D_FORTIFY_SOURCE=2
[3/7] /usr/bin/meson --internal symbolextractor /builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/obj-x86_64-linux-gnu libinih.so.1 libinih.so.1 libinih.so.1.p/libinih.so.1.symbols 
[4/7] rm -f libinih.a && gcc-ar csrD libinih.a libinih.so.1.p/ini.c.o
[5/7] c++ -IlibINIReader.so.0.p -I. -I.. -I../cpp -fdiagnostics-color=always -D_FILE_OFFSET_BITS=64 -Wall -Winvalid-pch -Wnon-virtual-dtor -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -fPIC -DINI_HANDLER_LINENO=1 -MD -MQ libINIReader.so.0.p/cpp_INIReader.cpp.o -MF libINIReader.so.0.p/cpp_INIReader.cpp.o.d -o libINIReader.so.0.p/cpp_INIReader.cpp.o -c ../cpp/INIReader.cpp
[6/7] c++  -o libINIReader.so.0 libINIReader.so.0.p/cpp_INIReader.cpp.o -Wl,--as-needed -Wl,--no-undefined -shared -fPIC -Wl,--start-group -Wl,-soname,libINIReader.so.0 -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro -Wl,-z,now -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 '-Wl,-rpath,$ORIGIN/' -Wl,-rpath-link,/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/obj-x86_64-linux-gnu/ libinih.so.1 -Wl,--end-group
[7/7] rm -f libINIReader.a && gcc-ar csrD libINIReader.a libINIReader.so.0.p/cpp_INIReader.cpp.o
   debian/rules override_dh_auto_test
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53'
/usr/bin/make -C tests test CFLAGS+=-I.. LDFLAGS+="-Wl,-rpath,../obj-x86_64-linux-gnu -L../obj-x86_64-linux-gnu"
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/tests'
cc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wall -pedantic -I.. -Wdate-time -D_FORTIFY_SOURCE=2  -c -o parseargs.o parseargs.c
cc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wall -pedantic -I.. -Wdate-time -D_FORTIFY_SOURCE=2  -c -o unittest.o unittest.c
cc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wall -pedantic -I.. -o unittest parseargs.o unittest.o -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro -Wl,-z,now  -Wl,-rpath,../obj-x86_64-linux-gnu -L../obj-x86_64-linux-gnu -linih
./unittest > test_multi.txt
./unittest --ini_max_line 20 > test_multi_max_line.txt
./unittest --ini_allow_multiline 0 > test_single.txt
./unittest --ini_allow_inline_comments 0 > test_disallow_inline_comments.txt
./unittest --ini_stop_on_first_error 1 > test_stop_on_first_error.txt
./unittest --ini_handler_lineno 1 > test_handler_lineno.txt
cc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wall -pedantic -I.. -Wdate-time -D_FORTIFY_SOURCE=2  -c -o unittest_string.o unittest_string.c
cc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wall -pedantic -I.. -o unittest_string parseargs.o unittest_string.o -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro -Wl,-z,now  -Wl,-rpath,../obj-x86_64-linux-gnu -L../obj-x86_64-linux-gnu -linih
./unittest_string --ini_max_line 20 > test_string.txt
./unittest --ini_use_stack 0 > test_heap.txt
./unittest --ini_use_stack 0 --ini_max_line 20 --ini_initial_alloc 20 > test_heap_max_line.txt
./unittest --ini_use_stack 0 --ini_allow_realloc 1 --ini_initial_alloc 5 > test_heap_realloc.txt
./unittest --ini_use_stack 0 --ini_max_line 20 --ini_allow_realloc 1 --ini_initial_alloc 5 > test_heap_realloc_max_line.txt
./unittest_string --ini_use_stack 0 --ini_max_line 20 --ini_initial_alloc 20 > test_heap_string.txt
./unittest --ini_allow_no_value 1 > test_allow_no_value.txt
diff -u baseline_multi.txt test_multi.txt
diff -u baseline_multi_max_line.txt test_multi_max_line.txt
diff -u baseline_single.txt test_single.txt
diff -u baseline_disallow_inline_comments.txt test_disallow_inline_comments.txt
diff -u baseline_stop_on_first_error.txt test_stop_on_first_error.txt
diff -u baseline_handler_lineno.txt test_handler_lineno.txt
diff -u baseline_string.txt test_string.txt
diff -u baseline_heap.txt test_heap.txt
diff -u baseline_heap_max_line.txt test_heap_max_line.txt
diff -u baseline_heap_realloc.txt test_heap_realloc.txt
diff -u baseline_heap_realloc_max_line.txt test_heap_realloc_max_line.txt
diff -u baseline_heap_string.txt test_heap_string.txt
diff -u baseline_allow_no_value.txt test_allow_no_value.txt
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/tests'
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53'
   create-stamp debian/debhelper-build-stamp
   dh_prep
   dh_auto_install
	cd obj-x86_64-linux-gnu && DESTDIR=/builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/debian/tmp LC_ALL=C.UTF-8 ninja install
[0/1] Installing files.
Installing libinih.so.1 to /builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/debian/tmp/usr/lib/x86_64-linux-gnu
Installing symlink pointing to libinih.so.1 to /builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/debian/tmp/usr/lib/x86_64-linux-gnu/libinih.so
Installing libinih.a to /builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/debian/tmp/usr/lib/x86_64-linux-gnu
Installing libINIReader.so.0 to /builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/debian/tmp/usr/lib/x86_64-linux-gnu
Installing symlink pointing to libINIReader.so.0 to /builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/debian/tmp/usr/lib/x86_64-linux-gnu/libINIReader.so
Installing libINIReader.a to /builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/debian/tmp/usr/lib/x86_64-linux-gnu
Installing /builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/ini.h to /builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/debian/tmp/usr/include
Installing /builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/cpp/INIReader.h to /builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/debian/tmp/usr/include
Installing /builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/obj-x86_64-linux-gnu/meson-private/inih.pc to /builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/debian/tmp/usr/lib/x86_64-linux-gnu/pkgconfig
Installing /builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/obj-x86_64-linux-gnu/meson-private/INIReader.pc to /builds/debdistutils/reproduce-trisquel/buildlogs/libinih/libinih-53/debian/tmp/usr/lib/x86_64-linux-gnu/pkgconfig
   dh_install
   dh_installdocs
   dh_installchangelogs
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_dwz -a
   dh_strip -a
8bbd810ec289f709d91b70b250ba2bf351eaf297
508cadfeb9f256c760633e32f17650de49a75451
   dh_makeshlibs -a
dpkg-gensymbols: warning: debian/libinireader0/DEBIAN/symbols doesn't match completely debian/libinireader0.symbols
--- debian/libinireader0.symbols (libinireader0_53-1ubuntu3+11.0trisquel0_amd64)
+++ dpkg-gensymbolscMMBjL	2023-04-03 12:33:55.672198780 +0000
@@ -13,6 +13,6 @@
  (c++)"INIReader::MakeKey(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&)@Base" 48
  (c++)"INIReader::ParseError() const@Base" 48
  (c++)"INIReader::ValueHandler(void*, char const*, char const*, char const*)@Base" 48
- (c++|optional=templinst|arch=!i386)"std::_Rb_tree<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::_Select1st<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > >, std::less<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::allocator<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > > >::_M_get_insert_hint_unique_pos(std::_Rb_tree_const_iterator<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > >, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&)@Base" 48
- (c++|optional=templinst)"std::_Rb_tree<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::_Select1st<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > >, std::less<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::allocator<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > > >::_M_get_insert_unique_pos(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&)@Base" 48
- (c++|optional=templinst)"std::_Rb_tree<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::_Select1st<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > >, std::less<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::allocator<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > > >::find(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&) const@Base" 48
+#MISSING: 53-1ubuntu3+11.0trisquel0# (c++|optional=templinst|arch=!i386)"std::_Rb_tree<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::_Select1st<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > >, std::less<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::allocator<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > > >::_M_get_insert_hint_unique_pos(std::_Rb_tree_const_iterator<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > >, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&)@Base" 48
+#MISSING: 53-1ubuntu3+11.0trisquel0# (c++|optional=templinst)"std::_Rb_tree<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::_Select1st<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > >, std::less<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::allocator<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > > >::_M_get_insert_unique_pos(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&)@Base" 48
+#MISSING: 53-1ubuntu3+11.0trisquel0# (c++|optional=templinst)"std::_Rb_tree<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::_Select1st<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > >, std::less<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::allocator<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > > >::find(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&) const@Base" 48
   dh_shlibdeps -a
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
pkgstripfiles: processing control file: debian/libinih-dev/DEBIAN/control, package libinih-dev, directory debian/libinih-dev
Searching for duplicated docs in dependency libinih1...
  symlinking changelog.Debian.gz in libinih-dev to file in libinih1
Searching for duplicated docs in dependency libinireader0...
pkgstripfiles: Running PNG optimization (using 1 cpus) for package libinih-dev ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'libinih-dev' in '../libinih-dev_53-1ubuntu3+11.0trisquel0_amd64.deb'.
pkgstripfiles: processing control file: debian/libinih1/DEBIAN/control, package libinih1, directory debian/libinih1
pkgstripfiles: Truncating usr/share/doc/libinih1/changelog.Debian.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 1 cpus) for package libinih1 ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'libinih1' in '../libinih1_53-1ubuntu3+11.0trisquel0_amd64.deb'.
INFO: Disabling pkgsanitychecks for udeb
pkgstripfiles: processing control file: debian/libinih1-udeb/DEBIAN/control, package libinih1-udeb, directory debian/libinih1-udeb
pkgstripfiles: Running PNG optimization (using 1 cpus) for package libinih1-udeb ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'libinih1-udeb' in 'debian/.debhelper/scratch-space/build-libinih1-udeb/libinih1-udeb_53-1ubuntu3+11.0trisquel0_amd64.deb'.
	Renaming libinih1-udeb_53-1ubuntu3+11.0trisquel0_amd64.deb to libinih1-udeb_53-1ubuntu3+11.0trisquel0_amd64.udeb
pkgstripfiles: processing control file: debian/libinireader0/DEBIAN/control, package libinireader0, directory debian/libinireader0
Searching for duplicated docs in dependency libinih1...
  symlinking changelog.Debian.gz in libinireader0 to file in libinih1
pkgstripfiles: Running PNG optimization (using 1 cpus) for package libinireader0 ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'libinireader0' in '../libinireader0_53-1ubuntu3+11.0trisquel0_amd64.deb'.
 dpkg-genbuildinfo -O../libinih_53-1ubuntu3+11.0trisquel0_amd64.buildinfo
 dpkg-genchanges -O../libinih_53-1ubuntu3+11.0trisquel0_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ cd ..
+ ls -la
total 136
drwxr-xr-x 3 root root  4096 Apr  3 12:34 .
drwxr-xr-x 3 root root  4096 Apr  3 12:33 ..
-rw-r--r-- 1 root root 28457 Apr  3 12:34 buildlog.txt
drwxr-xr-x 8 root root  4096 Apr  3 12:33 libinih-53
-rw-r--r-- 1 root root 20628 Apr  3 12:33 libinih-dev_53-1ubuntu3+11.0trisquel0_amd64.deb
-rw-r--r-- 1 root root  4324 Apr  3 12:33 libinih1-udeb_53-1ubuntu3+11.0trisquel0_amd64.udeb
-rw-r--r-- 1 root root  7294 Apr  3 12:33 libinih1_53-1ubuntu3+11.0trisquel0_amd64.deb
-rw-r--r-- 1 root root  1092 Apr  3 12:33 libinih_53-1ubuntu3+11.0trisquel0.dsc
-rw-r--r-- 1 root root 23460 Apr  3 12:33 libinih_53-1ubuntu3+11.0trisquel0.tar.xz
-rw-r--r-- 1 root root  8260 Apr  3 12:34 libinih_53-1ubuntu3+11.0trisquel0_amd64.buildinfo
-rw-r--r-- 1 root root  3006 Apr  3 12:34 libinih_53-1ubuntu3+11.0trisquel0_amd64.changes
-rw-r--r-- 1 root root  9660 Apr  3 12:34 libinireader0_53-1ubuntu3+11.0trisquel0_amd64.deb
+ find . -maxdepth 1 -type f
+ sha256sum ./libinih1_53-1ubuntu3+11.0trisquel0_amd64.deb ./buildlog.txt ./libinih_53-1ubuntu3+11.0trisquel0.dsc ./libinih-dev_53-1ubuntu3+11.0trisquel0_amd64.deb ./libinireader0_53-1ubuntu3+11.0trisquel0_amd64.deb ./libinih1-udeb_53-1ubuntu3+11.0trisquel0_amd64.udeb ./libinih_53-1ubuntu3+11.0trisquel0_amd64.changes ./libinih_53-1ubuntu3+11.0trisquel0_amd64.buildinfo ./libinih_53-1ubuntu3+11.0trisquel0.tar.xz
97838155f9e4bfe9294aa6ab3212e69d4c64aa7bf846d662ed073bc537bcb5f9  ./libinih1_53-1ubuntu3+11.0trisquel0_amd64.deb
84c108005fbe163267ada8503a5ca87ef398f852c6e7e63d956ff5c3be7dbe6d  ./buildlog.txt
d5dae0d6a97eb2cba9b7bfffb03449acc6ce1781e606ff4c951a1ebf51e371ea  ./libinih_53-1ubuntu3+11.0trisquel0.dsc
ff21e13d82874f1f69ddbc836b23a6663b9f5709b7f194004fca24dcab8cfb35  ./libinih-dev_53-1ubuntu3+11.0trisquel0_amd64.deb
64c7109b6935c1f21df985e34b07e4736972cefb612920f1a99060aaf2121ee5  ./libinireader0_53-1ubuntu3+11.0trisquel0_amd64.deb
cb8710d951fffe875db01da2d24bd8b427a460449890082429723c0f9ea8b733  ./libinih1-udeb_53-1ubuntu3+11.0trisquel0_amd64.udeb
8cdeec8ae18d836b7909de9303860ce7a2fb02d0428e253aa50b1838f8d11e10  ./libinih_53-1ubuntu3+11.0trisquel0_amd64.changes
2be8d2e4083ed1f14e38f6ac79f909434be18327553b7ced179ced135b848921  ./libinih_53-1ubuntu3+11.0trisquel0_amd64.buildinfo
7e495449b0846367824e8ad08492c4fd847e80330c4a65c700372f36935649f7  ./libinih_53-1ubuntu3+11.0trisquel0.tar.xz
+ mkdir published
+ cd published
+ cd ../
+ ls libinih-dev_53-1ubuntu3+11.0trisquel0_amd64.deb libinih1_53-1ubuntu3+11.0trisquel0_amd64.deb libinireader0_53-1ubuntu3+11.0trisquel0_amd64.deb libinih1-udeb_53-1ubuntu3+11.0trisquel0_amd64.udeb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/libi/libinih/libinih-dev_53-1ubuntu3+11.0trisquel0_amd64.deb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/libi/libinih/libinih1-udeb_53-1ubuntu3+11.0trisquel0_amd64.udeb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/libi/libinih/libinih1_53-1ubuntu3+11.0trisquel0_amd64.deb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/libi/libinih/libinireader0_53-1ubuntu3+11.0trisquel0_amd64.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./libinih1_53-1ubuntu3+11.0trisquel0_amd64.deb ./libinih-dev_53-1ubuntu3+11.0trisquel0_amd64.deb ./libinireader0_53-1ubuntu3+11.0trisquel0_amd64.deb ./libinih1-udeb_53-1ubuntu3+11.0trisquel0_amd64.udeb
97838155f9e4bfe9294aa6ab3212e69d4c64aa7bf846d662ed073bc537bcb5f9  ./libinih1_53-1ubuntu3+11.0trisquel0_amd64.deb
ff21e13d82874f1f69ddbc836b23a6663b9f5709b7f194004fca24dcab8cfb35  ./libinih-dev_53-1ubuntu3+11.0trisquel0_amd64.deb
64c7109b6935c1f21df985e34b07e4736972cefb612920f1a99060aaf2121ee5  ./libinireader0_53-1ubuntu3+11.0trisquel0_amd64.deb
cb8710d951fffe875db01da2d24bd8b427a460449890082429723c0f9ea8b733  ./libinih1-udeb_53-1ubuntu3+11.0trisquel0_amd64.udeb
+ cd ..
+ sha256sum -c SHA256SUMS
./libinih1_53-1ubuntu3+11.0trisquel0_amd64.deb: OK
./libinih-dev_53-1ubuntu3+11.0trisquel0_amd64.deb: OK
./libinireader0_53-1ubuntu3+11.0trisquel0_amd64.deb: OK
./libinih1-udeb_53-1ubuntu3+11.0trisquel0_amd64.udeb: OK
+ echo Package libinih is reproducible!
Package libinih is reproducible!
