+ date
Mon Apr  3 12:33:01 UTC 2023
+ apt-get source --only-source debian-ports-archive-keyring
Reading package lists...
Need to get 33.4 kB of source archives.
Get:1 http://archive.trisquel.info/trisquel aramo/main debian-ports-archive-keyring 2022.02.15+11.0trisquel0 (dsc) [1737 B]
Get:2 http://archive.trisquel.info/trisquel aramo/main debian-ports-archive-keyring 2022.02.15+11.0trisquel0 (tar) [31.7 kB]
dpkg-source: info: extracting debian-ports-archive-keyring in debian-ports-archive-keyring-2022.02.15+11.0trisquel0
dpkg-source: info: unpacking debian-ports-archive-keyring_2022.02.15+11.0trisquel0.tar.gz
Fetched 33.4 kB in 0s (253 kB/s)
W: Download is performed unsandboxed as root as file 'debian-ports-archive-keyring_2022.02.15+11.0trisquel0.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source debian-ports-archive-keyring
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-strip-nondeterminism dwz libdebhelper-perl
  libfile-stripnondeterminism-perl libsub-override-perl libtool m4 po-debconf
0 upgraded, 15 newly installed, 0 to remove and 1 not upgraded.
Need to get 3149 kB of archives.
After this operation, 8859 kB of additional disk space will be used.
Get:1 http://archive.trisquel.info/trisquel aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://archive.trisquel.info/trisquel aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:3 http://archive.trisquel.info/trisquel aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:4 http://archive.trisquel.info/trisquel aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:5 http://archive.trisquel.info/trisquel aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:6 http://archive.trisquel.info/trisquel aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:7 http://archive.trisquel.info/trisquel aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:8 http://archive.trisquel.info/trisquel aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:9 http://archive.trisquel.info/trisquel aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:10 http://archive.trisquel.info/trisquel aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:11 http://archive.trisquel.info/trisquel aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:12 http://archive.trisquel.info/trisquel aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:13 http://archive.trisquel.info/trisquel aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:14 http://archive.trisquel.info/trisquel aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:15 http://archive.trisquel.info/trisquel aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3149 kB in 0s (9447 kB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123238 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../01-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../02-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../03-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../04-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../05-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../06-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../07-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../08-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../09-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../10-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../11-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../12-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../13-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../14-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name debian-ports-archive-keyring* -type d
+ cd ./debian-ports-archive-keyring-2022.02.15+11.0trisquel0
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package debian-ports-archive-keyring
dpkg-buildpackage: info: source version 2022.02.15+11.0trisquel0
dpkg-buildpackage: info: source distribution aramo
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   debian/rules override_dh_auto_clean
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/debian-ports-archive-keyring/debian-ports-archive-keyring-2022.02.15+11.0trisquel0'
rm -fr /builds/debdistutils/reproduce-trisquel/buildlogs/debian-ports-archive-keyring/debian-ports-archive-keyring-2022.02.15+11.0trisquel0/build
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/debian-ports-archive-keyring/debian-ports-archive-keyring-2022.02.15+11.0trisquel0'
   dh_clean
 dpkg-source -b .
dpkg-source: warning: no source format specified in debian/source/format, see dpkg-source(1)
dpkg-source: info: using source format '1.0'
dpkg-source: info: building debian-ports-archive-keyring in debian-ports-archive-keyring_2022.02.15+11.0trisquel0.tar.gz
dpkg-source: info: building debian-ports-archive-keyring in debian-ports-archive-keyring_2022.02.15+11.0trisquel0.dsc
 debian/rules binary
dh binary
   dh_update_autotools_config
   dh_autoreconf
   debian/rules override_dh_auto_build
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/debian-ports-archive-keyring/debian-ports-archive-keyring-2022.02.15+11.0trisquel0'
mkdir -p /builds/debdistutils/reproduce-trisquel/buildlogs/debian-ports-archive-keyring/debian-ports-archive-keyring-2022.02.15+11.0trisquel0/build
# Build keyrings
mkdir -p /builds/debdistutils/reproduce-trisquel/buildlogs/debian-ports-archive-keyring/debian-ports-archive-keyring-2022.02.15+11.0trisquel0/build/keyrings
gpg --no-options --no-default-keyring --no-auto-check-trustdb --no-keyring --import-options import-export --import /builds/debdistutils/reproduce-trisquel/buildlogs/debian-ports-archive-keyring/debian-ports-archive-keyring-2022.02.15+11.0trisquel0/active-keys/* > /builds/debdistutils/reproduce-trisquel/buildlogs/debian-ports-archive-keyring/debian-ports-archive-keyring-2022.02.15+11.0trisquel0/build/keyrings/debian-ports-archive-keyring.gpg
gpg: Total number processed: 2
gpg --no-options --no-default-keyring --no-auto-check-trustdb --no-keyring --import-options import-export --import /builds/debdistutils/reproduce-trisquel/buildlogs/debian-ports-archive-keyring/debian-ports-archive-keyring-2022.02.15+11.0trisquel0/removed-keys/* > /builds/debdistutils/reproduce-trisquel/buildlogs/debian-ports-archive-keyring/debian-ports-archive-keyring-2022.02.15+11.0trisquel0/build/keyrings/debian-ports-archive-keyring-removed.gpg
gpg: key 3A3B88433E4C6047: 1 signature not checked due to a missing key
gpg: key 0792182443229C06: 1 signature not checked due to a missing key
gpg: key AE4A31290917E535: 1 signature not checked due to a missing key
gpg: key F460A91E4D922A56: 2 signatures not checked due to missing keys
gpg: key D9B937C622261D71: 2 signatures not checked due to missing keys
gpg: key A2A560BCA92F9FF4: 2 signatures not checked due to missing keys
gpg: key 1C466F272FF7A9F4: 2 signatures not checked due to missing keys
gpg: key AA651E74623DB0B8: 2 signatures not checked due to missing keys
gpg: key A53AB45AC448326E: 1 signature not checked due to a missing key
gpg: key B4C86482705A2CE1: 1 signature not checked due to a missing key
gpg: key 8BC3A7D46F930576: 1 signature not checked due to a missing key
gpg: Total number processed: 15
# Build fragment files
mkdir -p /builds/debdistutils/reproduce-trisquel/buildlogs/debian-ports-archive-keyring/debian-ports-archive-keyring-2022.02.15+11.0trisquel0/build/trusted.gpg.d
set -e ; \
for k in `ls /builds/debdistutils/reproduce-trisquel/buildlogs/debian-ports-archive-keyring/debian-ports-archive-keyring-2022.02.15+11.0trisquel0/active-keys` ; do \
    gpg --no-options --no-default-keyring --no-auto-check-trustdb --no-keyring --import-options import-export --import /builds/debdistutils/reproduce-trisquel/buildlogs/debian-ports-archive-keyring/debian-ports-archive-keyring-2022.02.15+11.0trisquel0/active-keys/$k > /builds/debdistutils/reproduce-trisquel/buildlogs/debian-ports-archive-keyring/debian-ports-archive-keyring-2022.02.15+11.0trisquel0/build/trusted.gpg.d/${k%.key}.gpg ; \
done
gpg: Total number processed: 1
gpg: Total number processed: 1
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/debian-ports-archive-keyring/debian-ports-archive-keyring-2022.02.15+11.0trisquel0'
   create-stamp debian/debhelper-build-stamp
   dh_prep
   dh_install
   dh_installdocs
   dh_installchangelogs
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
pkgstripfiles: processing control file: debian/debian-ports-archive-keyring/DEBIAN/control, package debian-ports-archive-keyring, directory debian/debian-ports-archive-keyring
pkgstripfiles: Truncating usr/share/doc/debian-ports-archive-keyring/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 1 cpus) for package debian-ports-archive-keyring ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'debian-ports-archive-keyring' in '../debian-ports-archive-keyring_2022.02.15+11.0trisquel0_all.deb'.
INFO: Disabling pkgsanitychecks for udeb
pkgstripfiles: processing control file: debian/debian-ports-archive-keyring-udeb/DEBIAN/control, package debian-ports-archive-keyring-udeb, directory debian/debian-ports-archive-keyring-udeb
pkgstripfiles: Running PNG optimization (using 1 cpus) for package debian-ports-archive-keyring-udeb ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'debian-ports-archive-keyring-udeb' in 'debian/.debhelper/scratch-space/build-debian-ports-archive-keyring-udeb/debian-ports-archive-keyring-udeb_2022.02.15+11.0trisquel0_all.deb'.
	Renaming debian-ports-archive-keyring-udeb_2022.02.15+11.0trisquel0_all.deb to debian-ports-archive-keyring-udeb_2022.02.15+11.0trisquel0_all.udeb
 dpkg-genbuildinfo -O../debian-ports-archive-keyring_2022.02.15+11.0trisquel0_amd64.buildinfo
 dpkg-genchanges -O../debian-ports-archive-keyring_2022.02.15+11.0trisquel0_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ cd ..
+ ls -la
total 116
drwxr-xr-x 3 root root  4096 Apr  3 12:33 .
drwxr-xr-x 3 root root  4096 Apr  3 12:33 ..
-rw-r--r-- 1 root root 13680 Apr  3 12:33 buildlog.txt
drwxr-xr-x 7 root root  4096 Apr  3 12:33 debian-ports-archive-keyring-2022.02.15+11.0trisquel0
-rw-r--r-- 1 root root  3520 Apr  3 12:33 debian-ports-archive-keyring-udeb_2022.02.15+11.0trisquel0_all.udeb
-rw-r--r-- 1 root root   854 Apr  3 12:33 debian-ports-archive-keyring_2022.02.15+11.0trisquel0.dsc
-rw-r--r-- 1 root root 31712 Apr  3 12:33 debian-ports-archive-keyring_2022.02.15+11.0trisquel0.tar.gz
-rw-r--r-- 1 root root 28886 Apr  3 12:33 debian-ports-archive-keyring_2022.02.15+11.0trisquel0_all.deb
-rw-r--r-- 1 root root  8407 Apr  3 12:33 debian-ports-archive-keyring_2022.02.15+11.0trisquel0_amd64.buildinfo
-rw-r--r-- 1 root root  2614 Apr  3 12:33 debian-ports-archive-keyring_2022.02.15+11.0trisquel0_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./debian-ports-archive-keyring_2022.02.15+11.0trisquel0_amd64.changes ./debian-ports-archive-keyring_2022.02.15+11.0trisquel0_amd64.buildinfo ./debian-ports-archive-keyring-udeb_2022.02.15+11.0trisquel0_all.udeb ./buildlog.txt ./debian-ports-archive-keyring_2022.02.15+11.0trisquel0.tar.gz ./debian-ports-archive-keyring_2022.02.15+11.0trisquel0.dsc ./debian-ports-archive-keyring_2022.02.15+11.0trisquel0_all.deb
04c26d5bd7e968e043852839902d3d0e31e813b715f34046226fcc38e9ac788c  ./debian-ports-archive-keyring_2022.02.15+11.0trisquel0_amd64.changes
0e5e2285fb43b6186cc700166e31dd8181ec9c79be8a8d0c929649a28cad34dd  ./debian-ports-archive-keyring_2022.02.15+11.0trisquel0_amd64.buildinfo
28597367aac884f0a672d708e691448515d9578da64aa110b9ce7933851e7c8e  ./debian-ports-archive-keyring-udeb_2022.02.15+11.0trisquel0_all.udeb
adb64d386488f11dec1666ee4a41a5ab779a070f1cb580a535be7d905acb4dc3  ./buildlog.txt
4275c038d9efc717ffbf1b127565b81ca3de49540ceac7b62ab0b6d39018569e  ./debian-ports-archive-keyring_2022.02.15+11.0trisquel0.tar.gz
2559cc5fa88443a558a91d801bf086987f5839ed8114095910505bcf2128bea2  ./debian-ports-archive-keyring_2022.02.15+11.0trisquel0.dsc
52ac8b8a0e703deee24d5b75138771809b9e2a236d2351eaa58a799ce9ef8213  ./debian-ports-archive-keyring_2022.02.15+11.0trisquel0_all.deb
+ mkdir published
+ cd published
+ cd ../
+ ls debian-ports-archive-keyring_2022.02.15+11.0trisquel0_all.deb debian-ports-archive-keyring-udeb_2022.02.15+11.0trisquel0_all.udeb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/d/debian-ports-archive-keyring/debian-ports-archive-keyring-udeb_2022.02.15+11.0trisquel0_all.udeb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/d/debian-ports-archive-keyring/debian-ports-archive-keyring_2022.02.15+11.0trisquel0_all.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./debian-ports-archive-keyring-udeb_2022.02.15+11.0trisquel0_all.udeb ./debian-ports-archive-keyring_2022.02.15+11.0trisquel0_all.deb
28597367aac884f0a672d708e691448515d9578da64aa110b9ce7933851e7c8e  ./debian-ports-archive-keyring-udeb_2022.02.15+11.0trisquel0_all.udeb
52ac8b8a0e703deee24d5b75138771809b9e2a236d2351eaa58a799ce9ef8213  ./debian-ports-archive-keyring_2022.02.15+11.0trisquel0_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./debian-ports-archive-keyring-udeb_2022.02.15+11.0trisquel0_all.udeb: OK
./debian-ports-archive-keyring_2022.02.15+11.0trisquel0_all.deb: OK
+ echo Package debian-ports-archive-keyring is reproducible!
Package debian-ports-archive-keyring is reproducible!
