+ date
Sat Nov 25 18:09:18 UTC 2023
+ id
uid=0(root) gid=0(root) groups=0(root)
+ apt-get source --only-source gajim-triggers=1.4.6-1~bpo11+1+11.0trisquel0
Reading package lists...
NOTICE: 'gajim-triggers' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/xmpp-team/gajim-triggers.git
Please use:
git clone https://salsa.debian.org/xmpp-team/gajim-triggers.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 14.2 kB of source archives.
Get:1 http://ftp.acc.umu.se/mirror/trisquel/packages aramo-backports/main gajim-triggers 1.4.6-1~bpo11+1+11.0trisquel0 (dsc) [1822 B]
Get:2 http://ftp.acc.umu.se/mirror/trisquel/packages aramo-backports/main gajim-triggers 1.4.6-1~bpo11+1+11.0trisquel0 (tar) [12.4 kB]
dpkg-source: info: extracting gajim-triggers in gajim-triggers-1.4.6
dpkg-source: info: unpacking gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0.tar.xz
Fetched 14.2 kB in 0s (158 kB/s)
W: Download is performed unsandboxed as root as file 'gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source gajim-triggers=1.4.6-1~bpo11+1+11.0trisquel0
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-python dh-strip-nondeterminism dwz libdebhelper-perl
  libfile-stripnondeterminism-perl libsub-override-perl libtool m4 po-debconf
0 upgraded, 16 newly installed, 0 to remove and 221 not upgraded.
Need to get 3255 kB of archives.
After this operation, 9349 kB of additional disk space will be used.
Get:1 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:3 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:4 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:5 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:6 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:7 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:8 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:9 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:10 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:11 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:12 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:13 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:14 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:15 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
Get:16 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 dh-python all 5.20220403 [106 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3255 kB in 0s (18.9 MB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123255 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../01-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../02-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../03-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../04-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../05-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../06-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../07-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../08-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../09-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../10-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../11-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../12-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../13-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../14-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Selecting previously unselected package dh-python.
Preparing to unpack .../15-dh-python_5.20220403_all.deb ...
Unpacking dh-python (5.20220403) ...
Setting up dh-python (5.20220403) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name gajim-triggers* -type d
+ cd ./gajim-triggers-1.4.6
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package gajim-triggers
dpkg-buildpackage: info: source version 1.4.6-1~bpo11+1+11.0trisquel0
dpkg-buildpackage: info: source distribution aramo-backports
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --with python3
   dh_clean
 dpkg-source -b .
dpkg-source: warning: native package version may not have a revision
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building gajim-triggers in gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0.tar.xz
dpkg-source: info: building gajim-triggers in gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0.dsc
 debian/rules binary
dh binary --with python3
   dh_update_autotools_config
   dh_autoreconf
   create-stamp debian/debhelper-build-stamp
   dh_prep
   dh_auto_install --destdir=debian/gajim-triggers/
   dh_install
   dh_installdocs
   dh_installchangelogs
   dh_python3
E: dh_python3 dh_python3:176: no package to act on (python3-foo or one with ${python3:Depends} in Depends)
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
pkgstripfiles: processing control file: debian/gajim-triggers/DEBIAN/control, package gajim-triggers, directory debian/gajim-triggers
pkgstripfiles: Truncating usr/share/doc/gajim-triggers/changelog.Debian.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 40 cpus) for package gajim-triggers ...
xargs: warning: options --max-lines and --replace/-I/-i are mutually exclusive, ignoring previous --max-lines value
o
pkgstripfiles: PNG optimization (1/0) for package gajim-triggers took 0 s
dpkg-deb: building package 'gajim-triggers' in '../gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_all.deb'.
 dpkg-genbuildinfo -O../gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_amd64.buildinfo
 dpkg-genchanges -O../gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Sat Nov 25 18:09:30 UTC 2023
+ cd ..
+ ls -la
total 72
drwxr-xr-x 3 root root  4096 Nov 25 18:09 .
drwxr-xr-x 3 root root  4096 Nov 25 18:09 ..
-rw-r--r-- 1 root root 10092 Nov 25 18:09 buildlog.txt
drwxr-xr-x 4 root root  4096 Oct 31 21:36 gajim-triggers-1.4.6
-rw-r--r-- 1 root root   939 Nov 25 18:09 gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0.dsc
-rw-r--r-- 1 root root 12408 Nov 25 18:09 gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0.tar.xz
-rw-r--r-- 1 root root 12916 Nov 25 18:09 gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_all.deb
-rw-r--r-- 1 root root  7352 Nov 25 18:09 gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_amd64.buildinfo
-rw-r--r-- 1 root root  1967 Nov 25 18:09 gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0.dsc ./buildlog.txt ./gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0.tar.xz ./gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_all.deb ./gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_amd64.changes ./gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_amd64.buildinfo
c875fd75c397ab8741046073f455549d398dc868365b4bc1fa7414b5115eb2e9  ./gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0.dsc
dc9220e1f24a615b27dc04dae8fc98a7b39248a756f967a5c08dc770b8d6f5f2  ./buildlog.txt
8ca9c168c763424bc71f518c5c6a6a47e4ace7d5c00fcd6c3c41565a6d312208  ./gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0.tar.xz
ef0991c27bd92f1d7de3243792cef91591f48784af6afb2e953b797ecf1217a3  ./gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_all.deb
f8e82553f42be12ae830a3e2f5ca458efc8547bb0d8c4b6d7f9d1b6fad1e23d0  ./gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_amd64.changes
e4357215675ca96344b08f20e8d6ec64c9b136833957675511f83bdde6efc7b2  ./gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_amd64.buildinfo
+ mkdir published
+ cd published
+ cd ../
+ ls gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://ftp.acc.umu.se/mirror/trisquel/packages/pool/main/g/gajim-triggers/gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_all.deb
--2023-11-25 18:09:30--  http://ftp.acc.umu.se/mirror/trisquel/packages/pool/main/g/gajim-triggers/gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_all.deb
Resolving ftp.acc.umu.se (ftp.acc.umu.se)... 194.71.11.163, 194.71.11.173, 194.71.11.165, ...
Connecting to ftp.acc.umu.se (ftp.acc.umu.se)|194.71.11.163|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 12916 (13K) [application/x-debian-package]
Saving to: 'gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_all.deb'

     0K .......... ..                                         100% 26.3M=0s

2023-11-25 18:09:30 (26.3 MB/s) - 'gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_all.deb' saved [12916/12916]

+ tee+ find ../SHA256SUMS
 . -maxdepth 1 -type f
+ sha256sum ./gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_all.deb
ef0991c27bd92f1d7de3243792cef91591f48784af6afb2e953b797ecf1217a3  ./gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./gajim-triggers_1.4.6-1~bpo11+1+11.0trisquel0_all.deb: OK
+ echo Package gajim-triggers version 1.4.6-1~bpo11+1+11.0trisquel0 is reproducible!
Package gajim-triggers version 1.4.6-1~bpo11+1+11.0trisquel0 is reproducible!
