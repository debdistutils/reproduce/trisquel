+ date
Mon Apr  3 12:33:18 UTC 2023
+ apt-get source --only-source installation-report
Reading package lists...
NOTICE: 'installation-report' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/installer-team/installation-report.git
Please use:
git clone https://salsa.debian.org/installer-team/installation-report.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 105 kB of source archives.
Get:1 http://archive.trisquel.info/trisquel aramo/main installation-report 2.78+11.0trisquel0 (dsc) [1808 B]
Get:2 http://archive.trisquel.info/trisquel aramo/main installation-report 2.78+11.0trisquel0 (tar) [103 kB]
dpkg-source: info: extracting installation-report in installation-report-2.78+11.0trisquel0
dpkg-source: info: unpacking installation-report_2.78+11.0trisquel0.tar.gz
Fetched 105 kB in 0s (404 kB/s)
W: Download is performed unsandboxed as root as file 'installation-report_2.78+11.0trisquel0.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source installation-report
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-strip-nondeterminism dwz libdebhelper-perl
  libfile-stripnondeterminism-perl libsub-override-perl libtool m4 po-debconf
0 upgraded, 15 newly installed, 0 to remove and 1 not upgraded.
Need to get 3149 kB of archives.
After this operation, 8859 kB of additional disk space will be used.
Get:1 http://archive.trisquel.info/trisquel aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://archive.trisquel.info/trisquel aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:3 http://archive.trisquel.info/trisquel aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:4 http://archive.trisquel.info/trisquel aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:5 http://archive.trisquel.info/trisquel aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:6 http://archive.trisquel.info/trisquel aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:7 http://archive.trisquel.info/trisquel aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:8 http://archive.trisquel.info/trisquel aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:9 http://archive.trisquel.info/trisquel aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:10 http://archive.trisquel.info/trisquel aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:11 http://archive.trisquel.info/trisquel aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:12 http://archive.trisquel.info/trisquel aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:13 http://archive.trisquel.info/trisquel aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:14 http://archive.trisquel.info/trisquel aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:15 http://archive.trisquel.info/trisquel aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3149 kB in 0s (10.3 MB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123238 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../01-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../02-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../03-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../04-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../05-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../06-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../07-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../08-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../09-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../10-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../11-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../12-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../13-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../14-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name installation-report* -type d
+ cd ./installation-report-2.78+11.0trisquel0
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package installation-report
dpkg-buildpackage: info: source version 2.78+11.0trisquel0
dpkg-buildpackage: info: source distribution aramo
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_clean
dh_clean: warning: Compatibility levels before 10 are deprecated (level 9 in use)
 dpkg-source -b .
dpkg-source: warning: no source format specified in debian/source/format, see dpkg-source(1)
dpkg-source: info: using source format '1.0'
dpkg-source: info: building installation-report in installation-report_2.78+11.0trisquel0.tar.gz
dpkg-source: info: building installation-report in installation-report_2.78+11.0trisquel0.dsc
 debian/rules build
dh build
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_update_autotools_config
 debian/rules binary
dh binary
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_testroot
   dh_prep
   dh_install
dh_install: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installdocs
dh_installdocs: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installchangelogs
   dh_installman
dh_installman: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installdebconf
   dh_perl
   dh_link
dh_link: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_strip_nondeterminism
   dh_compress
dh_compress: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_fixperms
   dh_missing
dh_missing: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installdeb
dh_installdeb: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_gencontrol
   dh_md5sums
   dh_builddeb
INFO: Disabling pkgsanitychecks for udeb
pkgstripfiles: processing control file: debian/save-logs/DEBIAN/control, package save-logs, directory debian/save-logs
pkgstripfiles: Running PNG optimization (using 1 cpus) for package save-logs ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'save-logs' in 'debian/.debhelper/scratch-space/build-save-logs/save-logs_2.78+11.0trisquel0_all.deb'.
	Renaming save-logs_2.78+11.0trisquel0_all.deb to save-logs_2.78+11.0trisquel0_all.udeb
pkgstripfiles: processing control file: debian/installation-report/DEBIAN/control, package installation-report, directory debian/installation-report
pkgstripfiles: Truncating usr/share/doc/installation-report/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 1 cpus) for package installation-report ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'installation-report' in '../installation-report_2.78+11.0trisquel0_all.deb'.
 dpkg-genbuildinfo -O../installation-report_2.78+11.0trisquel0_amd64.buildinfo
 dpkg-genchanges -O../installation-report_2.78+11.0trisquel0_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ cd ..
+ ls -la
total 204
drwxr-xr-x 3 root root   4096 Apr  3 12:33 .
drwxr-xr-x 3 root root   4096 Apr  3 12:33 ..
-rw-r--r-- 1 root root  10575 Apr  3 12:33 buildlog.txt
drwxr-xr-x 6 root root   4096 Oct  5  2019 installation-report-2.78+11.0trisquel0
-rw-r--r-- 1 root root    925 Apr  3 12:33 installation-report_2.78+11.0trisquel0.dsc
-rw-r--r-- 1 root root 103170 Apr  3 12:33 installation-report_2.78+11.0trisquel0.tar.gz
-rw-r--r-- 1 root root   6600 Apr  3 12:33 installation-report_2.78+11.0trisquel0_all.deb
-rw-r--r-- 1 root root   6949 Apr  3 12:33 installation-report_2.78+11.0trisquel0_amd64.buildinfo
-rw-r--r-- 1 root root   2271 Apr  3 12:33 installation-report_2.78+11.0trisquel0_amd64.changes
-rw-r--r-- 1 root root  50452 Apr  3 12:33 save-logs_2.78+11.0trisquel0_all.udeb
+ find . -maxdepth 1 -type f
+ sha256sum ./save-logs_2.78+11.0trisquel0_all.udeb ./installation-report_2.78+11.0trisquel0_all.deb ./installation-report_2.78+11.0trisquel0_amd64.changes ./buildlog.txt ./installation-report_2.78+11.0trisquel0_amd64.buildinfo ./installation-report_2.78+11.0trisquel0.dsc ./installation-report_2.78+11.0trisquel0.tar.gz
083df836e1605b8c73fe798547e16e5fa16bed00e949f331e22d02ff97653c08  ./save-logs_2.78+11.0trisquel0_all.udeb
95ceb604620e1e513ac93e2102111a5e13fe9bcd155d92ac403973ec4ff4b244  ./installation-report_2.78+11.0trisquel0_all.deb
1adaddc09fa83e974853afe3a6013714b1cc0b27c9a16fd536b04f7f49609df6  ./installation-report_2.78+11.0trisquel0_amd64.changes
5ce45f2819b29019f936d682173af6326f4b367618c76de976ff95cad8460acb  ./buildlog.txt
545a46fb99be76304f5f3fbbace976207caa4d9ea65004142c420634ea5cd303  ./installation-report_2.78+11.0trisquel0_amd64.buildinfo
8f759b3cb4b987453c8352cc1435314d48806812def8de64300acf0ae9aa85e2  ./installation-report_2.78+11.0trisquel0.dsc
d80f8a8a8ad11b551370b47cf6e7d130d7de68dd2a73cc4d30c9af207990020b  ./installation-report_2.78+11.0trisquel0.tar.gz
+ mkdir published
+ cd published
+ cd ../
+ ls installation-report_2.78+11.0trisquel0_all.deb save-logs_2.78+11.0trisquel0_all.udeb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/i/installation-report/installation-report_2.78+11.0trisquel0_all.deb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/i/installation-report/save-logs_2.78+11.0trisquel0_all.udeb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./save-logs_2.78+11.0trisquel0_all.udeb ./installation-report_2.78+11.0trisquel0_all.deb
083df836e1605b8c73fe798547e16e5fa16bed00e949f331e22d02ff97653c08  ./save-logs_2.78+11.0trisquel0_all.udeb
95ceb604620e1e513ac93e2102111a5e13fe9bcd155d92ac403973ec4ff4b244  ./installation-report_2.78+11.0trisquel0_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./save-logs_2.78+11.0trisquel0_all.udeb: OK
./installation-report_2.78+11.0trisquel0_all.deb: OK
+ echo Package installation-report is reproducible!
Package installation-report is reproducible!
