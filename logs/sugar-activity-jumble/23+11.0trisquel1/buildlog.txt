+ date
Sat Apr  1 23:01:06 UTC 2023
+ apt-get source --only-source sugar-activity-jumble
Reading package lists...
Need to get 1994 kB of source archives.
Get:1 http://archive.trisquel.info/trisquel aramo/main sugar-activity-jumble 23+11.0trisquel1 (dsc) [1516 B]
Get:2 http://archive.trisquel.info/trisquel aramo/main sugar-activity-jumble 23+11.0trisquel1 (tar) [1993 kB]
dpkg-source: info: extracting sugar-activity-jumble in sugar-activity-jumble-23+11.0trisquel1
dpkg-source: info: unpacking sugar-activity-jumble_23+11.0trisquel1.tar.gz
Fetched 1994 kB in 1s (3742 kB/s)
W: Download is performed unsandboxed as root as file 'sugar-activity-jumble_23+11.0trisquel1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ apt-get build-dep -y --only-source sugar-activity-jumble
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-strip-nondeterminism dwz libdebhelper-perl
  libfile-stripnondeterminism-perl libsub-override-perl libtool m4 po-debconf
0 upgraded, 15 newly installed, 0 to remove and 1 not upgraded.
Need to get 3149 kB of archives.
After this operation, 8859 kB of additional disk space will be used.
Get:1 http://archive.trisquel.info/trisquel aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://archive.trisquel.info/trisquel aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:3 http://archive.trisquel.info/trisquel aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:4 http://archive.trisquel.info/trisquel aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:5 http://archive.trisquel.info/trisquel aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:6 http://archive.trisquel.info/trisquel aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:7 http://archive.trisquel.info/trisquel aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:8 http://archive.trisquel.info/trisquel aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:9 http://archive.trisquel.info/trisquel aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:10 http://archive.trisquel.info/trisquel aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:11 http://archive.trisquel.info/trisquel aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:12 http://archive.trisquel.info/trisquel aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:13 http://archive.trisquel.info/trisquel aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:14 http://archive.trisquel.info/trisquel aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:15 http://archive.trisquel.info/trisquel aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3149 kB in 0s (9983 kB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123231 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../01-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../02-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../03-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../04-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../05-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../06-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../07-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../08-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../09-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../10-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../11-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../12-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../13-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../14-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name sugar-activity-jumble* -type d
+ cd ./sugar-activity-jumble-23+11.0trisquel1
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package sugar-activity-jumble
dpkg-buildpackage: info: source version 23+11.0trisquel1
dpkg-buildpackage: info: source distribution aramo
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   dh_clean
 dpkg-source -b .
dpkg-source: warning: no source format specified in debian/source/format, see dpkg-source(1)
dpkg-source: info: using source format '1.0'
dpkg-source: info: building sugar-activity-jumble in sugar-activity-jumble_23+11.0trisquel1.tar.gz
dpkg-source: info: building sugar-activity-jumble in sugar-activity-jumble_23+11.0trisquel1.dsc
 debian/rules build
dh build
   dh_update_autotools_config
   dh_autoreconf
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary
   dh_testroot
   dh_prep
   dh_auto_install --destdir=debian/sugar-activity-jumble/
   dh_install
   dh_installdocs
   dh_installchangelogs
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
pkgstripfiles: processing control file: debian/sugar-activity-jumble/DEBIAN/control, package sugar-activity-jumble, directory debian/sugar-activity-jumble
pkgstripfiles: Skipping PNG optimization for package in games section.
dpkg-deb: building package 'sugar-activity-jumble' in '../sugar-activity-jumble_23+11.0trisquel1_all.deb'.
 dpkg-genbuildinfo -O../sugar-activity-jumble_23+11.0trisquel1_amd64.buildinfo
 dpkg-genchanges -O../sugar-activity-jumble_23+11.0trisquel1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ cd ..
+ ls -la
total 3924
drwxr-xr-x 3 root root    4096 Apr  1 23:01 .
drwxr-xr-x 3 root root    4096 Apr  1 23:01 ..
-rw-r--r-- 1 root root    8795 Apr  1 23:01 buildlog.txt
drwxr-xr-x 4 root root    4096 Jul  4  2022 sugar-activity-jumble-23+11.0trisquel1
-rw-r--r-- 1 root root     633 Apr  1 23:01 sugar-activity-jumble_23+11.0trisquel1.dsc
-rw-r--r-- 1 root root 1992831 Apr  1 23:01 sugar-activity-jumble_23+11.0trisquel1.tar.gz
-rw-r--r-- 1 root root 1980850 Apr  1 23:01 sugar-activity-jumble_23+11.0trisquel1_all.deb
-rw-r--r-- 1 root root    6666 Apr  1 23:01 sugar-activity-jumble_23+11.0trisquel1_amd64.buildinfo
-rw-r--r-- 1 root root    1804 Apr  1 23:01 sugar-activity-jumble_23+11.0trisquel1_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./sugar-activity-jumble_23+11.0trisquel1.dsc ./sugar-activity-jumble_23+11.0trisquel1_amd64.changes ./sugar-activity-jumble_23+11.0trisquel1_all.deb ./buildlog.txt ./sugar-activity-jumble_23+11.0trisquel1_amd64.buildinfo ./sugar-activity-jumble_23+11.0trisquel1.tar.gz
05798982fab94c1eaa7718ac077dece5f16bf7a1ce65d7c0fff4facf11720e51  ./sugar-activity-jumble_23+11.0trisquel1.dsc
c41782cc96d530857a7c324396b830faf7938542db7759e01a0fcc9de82c6206  ./sugar-activity-jumble_23+11.0trisquel1_amd64.changes
64944967e4c32023d380eaa1b96faef1d97e3444b8b3fdeb74fc6fac4394c5e6  ./sugar-activity-jumble_23+11.0trisquel1_all.deb
e78ee5e869c62eb1c939a4c4d62bcb2356463ab8452ad248294c57e6b3abe50b  ./buildlog.txt
a023d4524fa78fe36968bb5b1918e8c9f0bad5d75fa12d894be258175d292e50  ./sugar-activity-jumble_23+11.0trisquel1_amd64.buildinfo
677f7dd996bae0c3df1bf5b62be6fedb87806f39b2434dd8f52aa2d6a2143df0  ./sugar-activity-jumble_23+11.0trisquel1.tar.gz
+ mkdir published
+ cd published
+ sed -e s,.*/\([^_]*\)_.*,\1,
+ echo ../sugar-activity-jumble_23+11.0trisquel1_all.deb
+ deb=sugar-activity-jumble
+ cut -d' -f2
+ grep ^'
+ apt-get --print-uris --yes download sugar-activity-jumble
+ url=http://archive.trisquel.info/trisquel/pool/main/s/sugar-activity-jumble/sugar-activity-jumble_23%2b11.0trisquel1_all.deb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/s/sugar-activity-jumble/sugar-activity-jumble_23%2b11.0trisquel1_all.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./sugar-activity-jumble_23+11.0trisquel1_all.deb
64944967e4c32023d380eaa1b96faef1d97e3444b8b3fdeb74fc6fac4394c5e6  ./sugar-activity-jumble_23+11.0trisquel1_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./sugar-activity-jumble_23+11.0trisquel1_all.deb: OK
+ echo Package sugar-activity-jumble is reproducible!
Package sugar-activity-jumble is reproducible!
