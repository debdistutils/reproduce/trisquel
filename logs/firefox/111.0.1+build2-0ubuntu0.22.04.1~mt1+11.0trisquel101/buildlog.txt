+ date
Sun Apr  2 09:53:21 UTC 2023
+ apt-get source --only-source firefox
Reading package lists...
NOTICE: 'firefox' packaging is maintained in the 'Bzr' version control system at:
https://code.launchpad.net/~mozillateam/firefox/firefox.jammy
Please use:
bzr branch https://code.launchpad.net/~mozillateam/firefox/firefox.jammy
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 516 MB of source archives.
Get:1 http://archive.trisquel.info/trisquel aramo-updates/main firefox 111.0.1+build2-0ubuntu0.22.04.1~mt1+11.0trisquel101 (dsc) [9078 B]
Get:2 http://archive.trisquel.info/trisquel aramo-updates/main firefox 111.0.1+build2-0ubuntu0.22.04.1~mt1+11.0trisquel101 (tar) [516 MB]
dpkg-source: info: extracting firefox in firefox-111.0.1+build2
dpkg-source: info: unpacking firefox_111.0.1+build2-0ubuntu0.22.04.1~mt1+11.0trisquel101.tar.xz
Fetched 516 MB in 21s (24.9 MB/s)
W: Download is performed unsandboxed as root as file 'firefox_111.0.1+build2-0ubuntu0.22.04.1~mt1+11.0trisquel101.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source firefox
Reading package lists...
Building dependency tree...
Reading state information...
Some packages could not be installed. This may mean that you have
requested an impossible situation or if you are using the unstable
distribution that some required packages have not yet been created
or been moved out of Incoming.
The following information may help to resolve the situation:

The following packages have unmet dependencies:
 builddeps:firefox : Depends: nodejs (>= 12.22.12) but it is not going to be installed
E: Unable to correct problems, you have held broken packages.
