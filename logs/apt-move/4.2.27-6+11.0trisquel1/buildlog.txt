+ date
Sun Apr 16 15:50:49 UTC 2023
+ apt-get source --only-source apt-move=4.2.27-6+11.0trisquel1
Reading package lists...
Need to get 113 kB of source archives.
Get:1 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main apt-move 4.2.27-6+11.0trisquel1 (dsc) [1542 B]
Get:2 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main apt-move 4.2.27-6+11.0trisquel1 (tar) [111 kB]
dpkg-source: info: extracting apt-move in apt-move-4.2.27
dpkg-source: info: unpacking apt-move_4.2.27-6+11.0trisquel1.tar.gz
Fetched 113 kB in 0s (810 kB/s)
W: Download is performed unsandboxed as root as file 'apt-move_4.2.27-6+11.0trisquel1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source apt-move=4.2.27-6+11.0trisquel1
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-strip-nondeterminism dwz libapt-pkg-dev libdebhelper-perl
  libfile-stripnondeterminism-perl libsub-override-perl libtool m4 po-debconf
0 upgraded, 16 newly installed, 0 to remove and 0 not upgraded.
Need to get 3249 kB of archives.
After this operation, 9516 kB of additional disk space will be used.
Get:1 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:3 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:4 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:5 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:6 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:7 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:8 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:9 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:10 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:11 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:12 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:13 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:14 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:15 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
Get:16 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libapt-pkg-dev amd64 2.4.8+11.0trisquel3 [100 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3249 kB in 0s (15.8 MB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123255 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../01-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../02-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../03-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../04-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../05-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../06-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../07-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../08-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../09-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../10-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../11-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../12-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../13-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../14-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Selecting previously unselected package libapt-pkg-dev:amd64.
Preparing to unpack .../15-libapt-pkg-dev_2.4.8+11.0trisquel3_amd64.deb ...
Unpacking libapt-pkg-dev:amd64 (2.4.8+11.0trisquel3) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up libapt-pkg-dev:amd64 (2.4.8+11.0trisquel3) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name apt-move* -type d
+ cd ./apt-move-4.2.27
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package apt-move
dpkg-buildpackage: info: source version 4.2.27-6+11.0trisquel1
dpkg-buildpackage: info: source distribution aramo
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_auto_clean
dh_auto_clean: warning: Compatibility levels before 10 are deprecated (level 9 in use)
	make -j1 clean
make[1]: Entering directory '/build/apt-move/apt-move-4.2.27'
rm -f fetch *.o
make[1]: Leaving directory '/build/apt-move/apt-move-4.2.27'
   dh_clean
dh_clean: warning: Compatibility levels before 10 are deprecated (level 9 in use)
 dpkg-source -b .
dpkg-source: warning: no source format specified in debian/source/format, see dpkg-source(1)
dpkg-source: warning: native package version may not have a revision
dpkg-source: info: using source format '1.0'
dpkg-source: info: building apt-move in apt-move_4.2.27-6+11.0trisquel1.tar.gz
dpkg-source: info: building apt-move in apt-move_4.2.27-6+11.0trisquel1.dsc
 debian/rules build
dh build
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_update_autotools_config
   dh_auto_configure
dh_auto_configure: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_auto_build
dh_auto_build: warning: Compatibility levels before 10 are deprecated (level 9 in use)
	make -j1
make[1]: Entering directory '/build/apt-move/apt-move-4.2.27'
g++ -g -O2 -ffile-prefix-map=/build/apt-move/apt-move-4.2.27=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -g -O2 -Wall -Wdate-time -D_FORTIFY_SOURCE=2  -c -o fetch.o fetch.cc
g++ -o fetch -g -O2 -ffile-prefix-map=/build/apt-move/apt-move-4.2.27=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -g -O2 -Wall -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro -Wl,-z,now fetch.o  -lapt-pkg
make[1]: Leaving directory '/build/apt-move/apt-move-4.2.27'
   dh_auto_test
dh_auto_test: warning: Compatibility levels before 10 are deprecated (level 9 in use)
 debian/rules binary
dh binary
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_testroot
   dh_prep
   dh_installdirs
dh_installdirs: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_auto_install --destdir=debian/apt-move/
dh_auto_install: warning: Compatibility levels before 10 are deprecated (level 9 in use)
	make -j1 install DESTDIR=/build/apt-move/apt-move-4.2.27/debian/apt-move AM_UPDATE_INFO_DIR=no
make[1]: Entering directory '/build/apt-move/apt-move-4.2.27'
install -p apt-move /build/apt-move/apt-move-4.2.27/debian/apt-move/usr/bin
install fetch /build/apt-move/apt-move-4.2.27/debian/apt-move/usr/lib/apt-move
install del1 move3 pkg1 /build/apt-move/apt-move-4.2.27/debian/apt-move/usr/share/apt-move
install -m 644 get[23] move[4-7] *.awk /build/apt-move/apt-move-4.2.27/debian/apt-move/usr/share/apt-move
cp -p apt-move.conf /build/apt-move/apt-move-4.2.27/debian/apt-move/etc
cp -p apt-move.8 /build/apt-move/apt-move-4.2.27/debian/apt-move/usr/share/man/man8
make[1]: Leaving directory '/build/apt-move/apt-move-4.2.27'
   dh_installdocs
dh_installdocs: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installchangelogs
   dh_installexamples
dh_installexamples: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installman
dh_installman: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
dh_compress: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_fixperms
   dh_missing
dh_missing: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_strip
dh_strip: warning: Compatibility levels before 10 are deprecated (level 9 in use)
96db28c6985cd88a879f3a64610516ab6cf018a8
   dh_makeshlibs
dh_makeshlibs: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_shlibdeps
dh_shlibdeps: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installdeb
dh_installdeb: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_gencontrol
dpkg-gencontrol: warning: package apt-move: substitution variable ${perl:Depends} unused, but is defined
   dh_md5sums
   dh_builddeb
pkgstripfiles: processing control file: debian/apt-move/DEBIAN/control, package apt-move, directory debian/apt-move
.. removing usr/share/doc/apt-move/changelog.ancient.gz
pkgstripfiles: Truncating usr/share/doc/apt-move/changelog.Debian.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 50 cpus) for package apt-move ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'apt-move' in '../apt-move_4.2.27-6+11.0trisquel1_amd64.deb'.
 dpkg-genbuildinfo -O../apt-move_4.2.27-6+11.0trisquel1_amd64.buildinfo
 dpkg-genchanges -O../apt-move_4.2.27-6+11.0trisquel1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Sun Apr 16 15:51:05 UTC 2023
+ cd ..
+ ls -la
total 196
drwxr-xr-x 3 root root   4096 Apr 16 15:51 .
drwxr-xr-x 3 root root   4096 Apr 16 15:50 ..
drwxr-xr-x 4 root root   4096 Apr 16 15:50 apt-move-4.2.27
-rw-r--r-- 1 root root    659 Apr 16 15:50 apt-move_4.2.27-6+11.0trisquel1.dsc
-rw-r--r-- 1 root root 111353 Apr 16 15:50 apt-move_4.2.27-6+11.0trisquel1.tar.gz
-rw-r--r-- 1 root root   6823 Apr 16 15:51 apt-move_4.2.27-6+11.0trisquel1_amd64.buildinfo
-rw-r--r-- 1 root root   1709 Apr 16 15:51 apt-move_4.2.27-6+11.0trisquel1_amd64.changes
-rw-r--r-- 1 root root  37474 Apr 16 15:51 apt-move_4.2.27-6+11.0trisquel1_amd64.deb
-rw-r--r-- 1 root root  12884 Apr 16 15:51 buildlog.txt
+ find . -maxdepth 1 -type f
+ sha256sum ./buildlog.txt ./apt-move_4.2.27-6+11.0trisquel1.tar.gz ./apt-move_4.2.27-6+11.0trisquel1.dsc ./apt-move_4.2.27-6+11.0trisquel1_amd64.deb ./apt-move_4.2.27-6+11.0trisquel1_amd64.buildinfo ./apt-move_4.2.27-6+11.0trisquel1_amd64.changes
a6f057624ed6e8cb48e6e5f6d776c94a15c1740600dcb256316a0d0588970fe8  ./buildlog.txt
ce52b073c36e0b299542aaec8d816b447baa07661ad87987d76fa49da7083b49  ./apt-move_4.2.27-6+11.0trisquel1.tar.gz
4f84a1dd5973aa659a1577201eeb85a9224b2f848ffa293bfa29c8590471446f  ./apt-move_4.2.27-6+11.0trisquel1.dsc
620d02c3b0ca8d27d907451789d3b05edefe3e21873f01bb78ff70154453c39b  ./apt-move_4.2.27-6+11.0trisquel1_amd64.deb
04dd8a13b10351e3211217096983d440c21ec8d8b7bea22ff48daf2d97c7e4c4  ./apt-move_4.2.27-6+11.0trisquel1_amd64.buildinfo
10ef1642cab7b3cbb22eea459f25a3c159da87a134bd821adf45668b0d9d99d3  ./apt-move_4.2.27-6+11.0trisquel1_amd64.changes
+ mkdir published
+ cd published
+ cd ../
+ ls apt-move_4.2.27-6+11.0trisquel1_amd64.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://ftp.acc.umu.se/mirror/trisquel/packages/pool/main/a/apt-move/apt-move_4.2.27-6+11.0trisquel1_amd64.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./apt-move_4.2.27-6+11.0trisquel1_amd64.deb
e8a42b27c3ff7c88123318580c12a6b7ba81f46e6fa2f038afcc4397e90093e7  ./apt-move_4.2.27-6+11.0trisquel1_amd64.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./apt-move_4.2.27-6+11.0trisquel1_amd64.deb: FAILED
sha256sum: WARNING: 1 computed checksum did NOT match
