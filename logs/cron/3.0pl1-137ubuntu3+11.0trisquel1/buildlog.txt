+ date
Sat Apr  1 19:04:14 UTC 2023
+ apt-get source --only-source cron
Reading package lists...
NOTICE: 'cron' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/debian/cron.git
Please use:
git clone https://salsa.debian.org/debian/cron.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 709 kB of source archives.
Get:1 http://archive.trisquel.info/trisquel aramo/main cron 3.0pl1-137ubuntu3+11.0trisquel1 (dsc) [1779 B]
Get:2 http://archive.trisquel.info/trisquel aramo/main cron 3.0pl1-137ubuntu3+11.0trisquel1 (tar) [707 kB]
dpkg-source: info: extracting cron in cron-3.0pl1
dpkg-source: info: unpacking cron_3.0pl1-137ubuntu3+11.0trisquel1.tar.gz
Fetched 709 kB in 1s (998 kB/s)
W: Download is performed unsandboxed as root as file 'cron_3.0pl1-137ubuntu3+11.0trisquel1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ apt-get build-dep -y --only-source cron
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-strip-nondeterminism dwz libaudit-dev libcap-ng-dev libdebhelper-perl
  libfile-stripnondeterminism-perl libpam0g-dev libselinux1-dev libsepol-dev
  libsub-override-perl libtool m4 po-debconf
0 upgraded, 20 newly installed, 0 to remove and 1 not upgraded.
Need to get 3904 kB of archives.
After this operation, 12.1 MB of additional disk space will be used.
Get:1 http://archive.trisquel.info/trisquel aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://archive.trisquel.info/trisquel aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:3 http://archive.trisquel.info/trisquel aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:4 http://archive.trisquel.info/trisquel aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:5 http://archive.trisquel.info/trisquel aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:6 http://archive.trisquel.info/trisquel aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:7 http://archive.trisquel.info/trisquel aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:8 http://archive.trisquel.info/trisquel aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:9 http://archive.trisquel.info/trisquel aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:10 http://archive.trisquel.info/trisquel aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:11 http://archive.trisquel.info/trisquel aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:12 http://archive.trisquel.info/trisquel aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:13 http://archive.trisquel.info/trisquel aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:14 http://archive.trisquel.info/trisquel aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:15 http://archive.trisquel.info/trisquel aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
Get:16 http://archive.trisquel.info/trisquel aramo/main amd64 libcap-ng-dev amd64 0.7.9-2.2build3 [22.9 kB]
Get:17 http://archive.trisquel.info/trisquel aramo-updates/main amd64 libpam0g-dev amd64 1.4.0-11ubuntu2.3 [117 kB]
Get:18 http://archive.trisquel.info/trisquel aramo/main amd64 libsepol-dev amd64 3.3-1build1 [378 kB]
Get:19 http://archive.trisquel.info/trisquel aramo/main amd64 libselinux1-dev amd64 3.3-1build2+11.0trisquel1 [158 kB]
Get:20 http://archive.trisquel.info/trisquel aramo/main amd64 libaudit-dev amd64 1:3.0.7-1build1 [78.6 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3904 kB in 0s (10.7 MB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123231 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../01-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../02-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../03-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../04-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../05-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../06-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../07-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../08-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../09-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../10-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../11-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../12-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../13-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../14-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Selecting previously unselected package libcap-ng-dev.
Preparing to unpack .../15-libcap-ng-dev_0.7.9-2.2build3_amd64.deb ...
Unpacking libcap-ng-dev (0.7.9-2.2build3) ...
Selecting previously unselected package libpam0g-dev:amd64.
Preparing to unpack .../16-libpam0g-dev_1.4.0-11ubuntu2.3_amd64.deb ...
Unpacking libpam0g-dev:amd64 (1.4.0-11ubuntu2.3) ...
Selecting previously unselected package libsepol-dev:amd64.
Preparing to unpack .../17-libsepol-dev_3.3-1build1_amd64.deb ...
Unpacking libsepol-dev:amd64 (3.3-1build1) ...
Selecting previously unselected package libselinux1-dev:amd64.
Preparing to unpack .../18-libselinux1-dev_3.3-1build2+11.0trisquel1_amd64.deb ...
Unpacking libselinux1-dev:amd64 (3.3-1build2+11.0trisquel1) ...
Selecting previously unselected package libaudit-dev:amd64.
Preparing to unpack .../19-libaudit-dev_1%3a3.0.7-1build1_amd64.deb ...
Unpacking libaudit-dev:amd64 (1:3.0.7-1build1) ...
Setting up libpam0g-dev:amd64 (1.4.0-11ubuntu2.3) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up libsepol-dev:amd64 (3.3-1build1) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up libcap-ng-dev (0.7.9-2.2build3) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up libselinux1-dev:amd64 (3.3-1build2+11.0trisquel1) ...
Setting up libaudit-dev:amd64 (1:3.0.7-1build1) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name cron* -type d
+ cd ./cron-3.0pl1
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package cron
dpkg-buildpackage: info: source version 3.0pl1-137ubuntu3+11.0trisquel1
dpkg-buildpackage: info: source distribution aramo
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   dh_auto_clean
	make -j1 clean
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/cron/cron-3.0pl1'
rm -f *.o cron crontab a.out core tags *~ #*
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/cron/cron-3.0pl1'
   dh_clean
 dpkg-source -b .
dpkg-source: warning: no source format specified in debian/source/format, see dpkg-source(1)
dpkg-source: warning: native package version may not have a revision
dpkg-source: info: using source format '1.0'
dpkg-source: info: building cron in cron_3.0pl1-137ubuntu3+11.0trisquel1.tar.gz
dpkg-source: info: building cron in cron_3.0pl1-137ubuntu3+11.0trisquel1.dsc
 debian/rules binary
dh binary
   dh_update_autotools_config
   dh_autoreconf
   dh_auto_configure
   dh_auto_build
	make -j1 "INSTALL=install --strip-program=true"
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/cron/cron-3.0pl1'
cc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/cron/cron-3.0pl1=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wall -Wno-unused -Wno-comment -I. -DPOSIX -DDEBUGGING=0 -DUSE_PAM -DWITH_SELINUX  -Wdate-time -D_FORTIFY_SOURCE=2  -c -o cron.o cron.c
In file included from cron.c:26:
cron.h:208:9: warning: 'security_context_t' is deprecated [-Wdeprecated-declarations]
  208 |         security_context_t scontext;    /* SELinux security context */
      |         ^~~~~~~~~~~~~~~~~~
cron.c: In function 'main':
cron.c:134:25: warning: ignoring return value of 'freopen' declared with attribute 'warn_unused_result' [-Wunused-result]
  134 |                         freopen("/dev/null", "r", stdin);
      |                         ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cron.c:135:25: warning: ignoring return value of 'freopen' declared with attribute 'warn_unused_result' [-Wunused-result]
  135 |                         freopen("/dev/null", "w", stdout);
      |                         ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cron.c:136:25: warning: ignoring return value of 'freopen' declared with attribute 'warn_unused_result' [-Wunused-result]
  136 |                         freopen("/dev/null", "w", stderr);
      |                         ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
In file included from /usr/include/string.h:535,
                 from externs.h:21,
                 from cron.h:41,
                 from cron.c:26:
In function 'strncpy',
    inlined from 'main' at cron.c:112:3:
/usr/include/x86_64-linux-gnu/bits/string_fortified.h:95:10: warning: 'strncpy' specified bound 1000 equals destination size [-Wstringop-truncation]
   95 |   return __builtin___strncpy_chk (__dest, __src, __len,
      |          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   96 |                                   __glibc_objsize (__dest));
      |                                   ~~~~~~~~~~~~~~~~~~~~~~~~~
cc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/cron/cron-3.0pl1=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wall -Wno-unused -Wno-comment -I. -DPOSIX -DDEBUGGING=0 -DUSE_PAM -DWITH_SELINUX  -Wdate-time -D_FORTIFY_SOURCE=2  -c -o database.o database.c
In file included from database.c:26:
cron.h:208:9: warning: 'security_context_t' is deprecated [-Wdeprecated-declarations]
  208 |         security_context_t scontext;    /* SELinux security context */
      |         ^~~~~~~~~~~~~~~~~~
cc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/cron/cron-3.0pl1=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wall -Wno-unused -Wno-comment -I. -DPOSIX -DDEBUGGING=0 -DUSE_PAM -DWITH_SELINUX  -Wdate-time -D_FORTIFY_SOURCE=2  -c -o user.o user.c
In file included from user.c:28:
cron.h:208:9: warning: 'security_context_t' is deprecated [-Wdeprecated-declarations]
  208 |         security_context_t scontext;    /* SELinux security context */
      |         ^~~~~~~~~~~~~~~~~~
user.c:37:33: warning: 'security_context_t' is deprecated [-Wdeprecated-declarations]
   37 |                                 *rcontext, char *tabname) {
      |                                 ^
user.c: In function 'get_security_context':
user.c:38:9: warning: 'security_context_t' is deprecated [-Wdeprecated-declarations]
   38 |         security_context_t *context_list = NULL;
      |         ^~~~~~~~~~~~~~~~~~
user.c:39:9: warning: 'security_context_t' is deprecated [-Wdeprecated-declarations]
   39 |         security_context_t current_con;
      |         ^~~~~~~~~~~~~~~~~~
user.c:41:9: warning: 'security_context_t' is deprecated [-Wdeprecated-declarations]
   41 |         security_context_t  file_context=NULL;
      |         ^~~~~~~~~~~~~~~~~~
cc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/cron/cron-3.0pl1=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wall -Wno-unused -Wno-comment -I. -DPOSIX -DDEBUGGING=0 -DUSE_PAM -DWITH_SELINUX  -Wdate-time -D_FORTIFY_SOURCE=2  -c -o entry.o entry.c
In file included from entry.c:29:
cron.h:208:9: warning: 'security_context_t' is deprecated [-Wdeprecated-declarations]
  208 |         security_context_t scontext;    /* SELinux security context */
      |         ^~~~~~~~~~~~~~~~~~
cc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/cron/cron-3.0pl1=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wall -Wno-unused -Wno-comment -I. -DPOSIX -DDEBUGGING=0 -DUSE_PAM -DWITH_SELINUX  -Wdate-time -D_FORTIFY_SOURCE=2  -c -o job.o job.c
In file included from job.c:23:
cron.h:208:9: warning: 'security_context_t' is deprecated [-Wdeprecated-declarations]
  208 |         security_context_t scontext;    /* SELinux security context */
      |         ^~~~~~~~~~~~~~~~~~
cc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/cron/cron-3.0pl1=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wall -Wno-unused -Wno-comment -I. -DPOSIX -DDEBUGGING=0 -DUSE_PAM -DWITH_SELINUX  -Wdate-time -D_FORTIFY_SOURCE=2  -c -o do_command.o do_command.c
In file included from do_command.c:23:
cron.h:208:9: warning: 'security_context_t' is deprecated [-Wdeprecated-declarations]
  208 |         security_context_t scontext;    /* SELinux security context */
      |         ^~~~~~~~~~~~~~~~~~
do_command.c: In function 'safe_p':
do_command.c:699:24: warning: passing argument 1 of 'log_it' discards 'const' qualifier from pointer target type [-Wdiscarded-qualifiers]
  699 |                 log_it(usernm, getpid(), "UNSAFE MAIL", s);
      |                        ^~~~~~
In file included from /usr/include/features.h:486,
                 from /usr/include/x86_64-linux-gnu/sys/types.h:25,
                 from cron.h:29,
                 from do_command.c:23:
cron.h:241:29: note: expected 'char *' but argument is of type 'const char *'
  241 |                 log_it __P((char *, int, char *, char *)),
      |                             ^~~~~~
do_command.c:699:57: warning: passing argument 4 of 'log_it' discards 'const' qualifier from pointer target type [-Wdiscarded-qualifiers]
  699 |                 log_it(usernm, getpid(), "UNSAFE MAIL", s);
      |                                                         ^
In file included from /usr/include/features.h:486,
                 from /usr/include/x86_64-linux-gnu/sys/types.h:25,
                 from cron.h:29,
                 from do_command.c:23:
cron.h:241:50: note: expected 'char *' but argument is of type 'const char *'
  241 |                 log_it __P((char *, int, char *, char *)),
      |                                                  ^~~~~~
do_command.c: In function 'child_process':
do_command.c:175:9: warning: ignoring return value of 'pipe' declared with attribute 'warn_unused_result' [-Wunused-result]
  175 |         pipe(stdin_pipe);       /* child's stdin */
      |         ^~~~~~~~~~~~~~~~
do_command.c:314:17: warning: ignoring return value of 'chdir' declared with attribute 'warn_unused_result' [-Wunused-result]
  314 |                 chdir(env_get("HOME", e->envp));
      |                 ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/cron/cron-3.0pl1=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wall -Wno-unused -Wno-comment -I. -DPOSIX -DDEBUGGING=0 -DUSE_PAM -DWITH_SELINUX  -Wdate-time -D_FORTIFY_SOURCE=2  -c -o misc.o misc.c
In file included from misc.c:27:
cron.h:208:9: warning: 'security_context_t' is deprecated [-Wdeprecated-declarations]
  208 |         security_context_t scontext;    /* SELinux security context */
      |         ^~~~~~~~~~~~~~~~~~
misc.c: In function 'acquire_daemonlock':
misc.c:313:25: warning: ignoring return value of 'fscanf' declared with attribute 'warn_unused_result' [-Wunused-result]
  313 |                         fscanf(fp, "%d", &otherpid);
      |                         ^~~~~~~~~~~~~~~~~~~~~~~~~~~
misc.c:328:16: warning: ignoring return value of 'ftruncate' declared with attribute 'warn_unused_result' [-Wunused-result]
  328 |         (void) ftruncate(fileno(fp), ftell(fp));
      |                ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/cron/cron-3.0pl1=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wall -Wno-unused -Wno-comment -I. -DPOSIX -DDEBUGGING=0 -DUSE_PAM -DWITH_SELINUX  -Wdate-time -D_FORTIFY_SOURCE=2  -c -o env.o env.c
In file included from env.c:23:
cron.h:208:9: warning: 'security_context_t' is deprecated [-Wdeprecated-declarations]
  208 |         security_context_t scontext;    /* SELinux security context */
      |         ^~~~~~~~~~~~~~~~~~
cc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/cron/cron-3.0pl1=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wall -Wno-unused -Wno-comment -I. -DPOSIX -DDEBUGGING=0 -DUSE_PAM -DWITH_SELINUX  -Wdate-time -D_FORTIFY_SOURCE=2  -c -o popen.o popen.c
In file included from popen.c:31:
cron.h:208:9: warning: 'security_context_t' is deprecated [-Wdeprecated-declarations]
  208 |         security_context_t scontext;    /* SELinux security context */
      |         ^~~~~~~~~~~~~~~~~~
popen.c: In function 'cron_popen':
popen.c:150:17: warning: ignoring return value of 'chdir' declared with attribute 'warn_unused_result' [-Wunused-result]
  150 |                 chdir(env_get("HOME", e->envp));
      |                 ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/cron/cron-3.0pl1=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wall -Wno-unused -Wno-comment -I. -DPOSIX -DDEBUGGING=0 -DUSE_PAM -DWITH_SELINUX  -Wdate-time -D_FORTIFY_SOURCE=2  -c -o compat.o compat.c
In file included from compat.c:27:
cron.h:208:9: warning: 'security_context_t' is deprecated [-Wdeprecated-declarations]
  208 |         security_context_t scontext;    /* SELinux security context */
      |         ^~~~~~~~~~~~~~~~~~
cc -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro -Wl,-z,now -o cron cron.o database.o user.o entry.o job.o do_command.o misc.o env.o popen.o compat.o -lpam -lselinux 
cc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/cron/cron-3.0pl1=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -Wall -Wno-unused -Wno-comment -I. -DPOSIX -DDEBUGGING=0 -DUSE_PAM -DWITH_SELINUX  -Wdate-time -D_FORTIFY_SOURCE=2  -c -o crontab.o crontab.c
In file included from crontab.c:31:
cron.h:208:9: warning: 'security_context_t' is deprecated [-Wdeprecated-declarations]
  208 |         security_context_t scontext;    /* SELinux security context */
      |         ^~~~~~~~~~~~~~~~~~
crontab.c: In function 'delete_cmd':
crontab.c:371:32: warning: ignoring return value of 'fgets' declared with attribute 'warn_unused_result' [-Wunused-result]
  371 |                         (void) fgets(q, sizeof q, stdin);
      |                                ^~~~~~~~~~~~~~~~~~~~~~~~~
crontab.c: In function 'edit_cmd':
crontab.c:753:32: warning: ignoring return value of 'fgets' declared with attribute 'warn_unused_result' [-Wunused-result]
  753 |                         (void) fgets(q, sizeof q, stdin);
      |                                ^~~~~~~~~~~~~~~~~~~~~~~~~
cc -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro -Wl,-z,now -o crontab crontab.o misc.o entry.o env.o compat.o -lpam -lselinux 
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/cron/cron-3.0pl1'
   dh_auto_test
   create-stamp debian/debhelper-build-stamp
   dh_prep
   dh_installdirs
   debian/rules override_dh_auto_install
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/cron/cron-3.0pl1'
# Empty target to bypass the auto-detected install target in Makefile
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/cron/cron-3.0pl1'
   debian/rules override_dh_install
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/cron/cron-3.0pl1'
dh_install
install -m 644 debian/crontab.main debian/cron/etc/crontab
install -m 644 debian/placeholder debian/cron/etc/cron.d/.placeholder
install -m 644 debian/placeholder debian/cron/etc/cron.hourly/.placeholder
install -m 644 debian/placeholder debian/cron/etc/cron.daily/.placeholder
install -m 644 debian/placeholder debian/cron/etc/cron.weekly/.placeholder
install -m 644 debian/placeholder debian/cron/etc/cron.monthly/.placeholder
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/cron/cron-3.0pl1'
   dh_installdocs
   dh_installchangelogs
   dh_installexamples
   dh_installman
   dh_installinit
   dh_installsystemd
   dh_installpam
   dh_bugfiles
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_dwz
   dh_strip
debugedit: debian/cron/usr/bin/crontab: Unknown DWARF DW_FORM_0x1f20
6962edb23e487a78eb9d324988334c0b1f2e85f4
debugedit: debian/cron/usr/sbin/cron: Unknown DWARF DW_FORM_0x1f21
ee8acf776de998e03b51abd269efb0eb273fcd79
   dh_makeshlibs
   dh_shlibdeps
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
pkgstripfiles: processing control file: debian/cron/DEBIAN/control, package cron, directory debian/cron
pkgstripfiles: Truncating usr/share/doc/cron/changelog.Debian.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 1 cpus) for package cron ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'cron' in '../cron_3.0pl1-137ubuntu3+11.0trisquel1_amd64.deb'.
 dpkg-genbuildinfo -O../cron_3.0pl1-137ubuntu3+11.0trisquel1_amd64.buildinfo
 dpkg-genchanges -O../cron_3.0pl1-137ubuntu3+11.0trisquel1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ cd ..
+ ls -la
total 812
drwxr-xr-x 3 root root   4096 Apr  1 19:04 .
drwxr-xr-x 3 root root   4096 Apr  1 19:04 ..
-rw-r--r-- 1 root root  24834 Apr  1 19:04 buildlog.txt
drwxr-xr-x 4 root root   4096 Apr  1 19:04 cron-3.0pl1
-rw-r--r-- 1 root root    896 Apr  1 19:04 cron_3.0pl1-137ubuntu3+11.0trisquel1.dsc
-rw-r--r-- 1 root root 707177 Apr  1 19:04 cron_3.0pl1-137ubuntu3+11.0trisquel1.tar.gz
-rw-r--r-- 1 root root   7057 Apr  1 19:04 cron_3.0pl1-137ubuntu3+11.0trisquel1_amd64.buildinfo
-rw-r--r-- 1 root root   1859 Apr  1 19:04 cron_3.0pl1-137ubuntu3+11.0trisquel1_amd64.changes
-rw-r--r-- 1 root root  64966 Apr  1 19:04 cron_3.0pl1-137ubuntu3+11.0trisquel1_amd64.deb
+ find . -maxdepth 1 -type f
+ sha256sum ./cron_3.0pl1-137ubuntu3+11.0trisquel1.dsc ./buildlog.txt ./cron_3.0pl1-137ubuntu3+11.0trisquel1_amd64.deb ./cron_3.0pl1-137ubuntu3+11.0trisquel1.tar.gz ./cron_3.0pl1-137ubuntu3+11.0trisquel1_amd64.changes ./cron_3.0pl1-137ubuntu3+11.0trisquel1_amd64.buildinfo
f7c5751d93f4f8f2e01eb2e0fdd764d8aefae45ca6fc427e54d68ea7f1a1078e  ./cron_3.0pl1-137ubuntu3+11.0trisquel1.dsc
174df871468b4080a09883d7037cf7b0d3565f3446148b878e2887f09453e145  ./buildlog.txt
484b0c6cfbc7e61e6df4bd3bf8f0fdda5bb81985529df432d82198ec21d9ea0a  ./cron_3.0pl1-137ubuntu3+11.0trisquel1_amd64.deb
edc07b70010397ffc304e01686bb363e724036bccbe9b4c5989f085db0a552be  ./cron_3.0pl1-137ubuntu3+11.0trisquel1.tar.gz
ac3696583ff5a02e5df1a509d8a02853d668c5abe4d5950134491c791778e875  ./cron_3.0pl1-137ubuntu3+11.0trisquel1_amd64.changes
0b40fe71cfc2e3f1065b50fac10223877f818f0db9d5d8f3011ed049c1908688  ./cron_3.0pl1-137ubuntu3+11.0trisquel1_amd64.buildinfo
+ mkdir published
+ cd published
+ sed -e s,.*/\([^_]*\)_.*,\1,
+ echo ../cron_3.0pl1-137ubuntu3+11.0trisquel1_amd64.deb
+ deb=cron
+ cut -d' -f2
+ grep ^'
+ apt-get --print-uris --yes download cron
+ url=http://archive.trisquel.info/trisquel/pool/main/c/cron/cron_3.0pl1-137ubuntu3%2b11.0trisquel1_amd64.deb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/c/cron/cron_3.0pl1-137ubuntu3%2b11.0trisquel1_amd64.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./cron_3.0pl1-137ubuntu3+11.0trisquel1_amd64.deb
d72a61a9f2e0f41ac2be2955a12799ac7974b48e902cf0b4d98640f17a98f8b1  ./cron_3.0pl1-137ubuntu3+11.0trisquel1_amd64.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./cron_3.0pl1-137ubuntu3+11.0trisquel1_amd64.deb: FAILED
sha256sum: WARNING: 1 computed checksum did NOT match
