+ date
Sun Apr  2 08:12:35 UTC 2023
+ apt-get source --only-source unp
Reading package lists...
Need to get 19.9 kB of source archives.
Get:1 http://archive.trisquel.info/trisquel aramo/main unp 2.0~pre9+11.0trisquel1 (dsc) [1458 B]
Get:2 http://archive.trisquel.info/trisquel aramo/main unp 2.0~pre9+11.0trisquel1 (tar) [18.4 kB]
dpkg-source: info: extracting unp in unp-2.0~pre9+11.0trisquel1
dpkg-source: info: unpacking unp_2.0~pre9+11.0trisquel1.tar.gz
Fetched 19.9 kB in 0s (42.5 kB/s)
W: Download is performed unsandboxed as root as file 'unp_2.0~pre9+11.0trisquel1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ apt-get build-dep -y --only-source unp
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev bash-completion debhelper
  debugedit dh-autoreconf dh-strip-nondeterminism dwz libdebhelper-perl
  libfile-stripnondeterminism-perl libsub-override-perl libtool m4 po-debconf
0 upgraded, 16 newly installed, 0 to remove and 1 not upgraded.
Need to get 3329 kB of archives.
After this operation, 10.4 MB of additional disk space will be used.
Get:1 http://archive.trisquel.info/trisquel aramo/main amd64 bash-completion all 1:2.11-5ubuntu1 [180 kB]
Get:2 http://archive.trisquel.info/trisquel aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:3 http://archive.trisquel.info/trisquel aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:4 http://archive.trisquel.info/trisquel aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:5 http://archive.trisquel.info/trisquel aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:6 http://archive.trisquel.info/trisquel aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:7 http://archive.trisquel.info/trisquel aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:8 http://archive.trisquel.info/trisquel aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:9 http://archive.trisquel.info/trisquel aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:10 http://archive.trisquel.info/trisquel aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:11 http://archive.trisquel.info/trisquel aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:12 http://archive.trisquel.info/trisquel aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:13 http://archive.trisquel.info/trisquel aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:14 http://archive.trisquel.info/trisquel aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:15 http://archive.trisquel.info/trisquel aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:16 http://archive.trisquel.info/trisquel aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3329 kB in 1s (4563 kB/s)
Selecting previously unselected package bash-completion.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123231 files and directories currently installed.)
Preparing to unpack .../00-bash-completion_1%3a2.11-5ubuntu1_all.deb ...
Unpacking bash-completion (1:2.11-5ubuntu1) ...
Selecting previously unselected package m4.
Preparing to unpack .../01-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../02-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../03-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../04-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../05-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../06-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../07-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../08-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../09-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../10-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../11-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../12-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../13-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../14-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../15-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up autotools-dev (20220109.1) ...
Setting up bash-completion (1:2.11-5ubuntu1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name unp* -type d
+ cd ./unp-2.0~pre9+11.0trisquel1
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package unp
dpkg-buildpackage: info: source version 2.0~pre9+11.0trisquel1
dpkg-buildpackage: info: source distribution aramo
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --with bash-completion
   debian/rules override_dh_clean
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/unp/unp-2.0~pre9+11.0trisquel1'
dh_clean
rm -f po/*.mo
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/unp/unp-2.0~pre9+11.0trisquel1'
 dpkg-source -b .
dpkg-source: warning: no source format specified in debian/source/format, see dpkg-source(1)
dpkg-source: info: using source format '1.0'
dpkg-source: info: building unp in unp_2.0~pre9+11.0trisquel1.tar.gz
dpkg-source: info: building unp in unp_2.0~pre9+11.0trisquel1.dsc
 debian/rules build
dh build --with bash-completion
   dh_update_autotools_config
   dh_autoreconf
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary --with bash-completion
   dh_testroot
   dh_prep
   dh_installdirs
   dh_auto_install --destdir=debian/unp/
   debian/rules override_dh_install
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/unp/unp-2.0~pre9+11.0trisquel1'
dh_installdirs
mkdir -p debian/unp/usr/bin debian/unp/etc
install -m755 unp debian/unp/usr/bin/
ln -sf unp debian/unp/usr/bin/ucat
/usr/bin/make -C po install "DESTDIR=/builds/debdistutils/reproduce-trisquel/buildlogs/unp/unp-2.0~pre9+11.0trisquel1/debian/unp"
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/unp/unp-2.0~pre9+11.0trisquel1/po'
msgmerge -U cs.po messages.pot
... done.
msgfmt cs.po -o cs.mo
msgfmt --statistics cs.po
21 translated messages.
msgmerge -U de.po messages.pot
... done.
msgfmt de.po -o de.mo
msgfmt --statistics de.po
21 translated messages.
msgmerge -U fr.po messages.pot
... done.
msgfmt fr.po -o fr.mo
msgfmt --statistics fr.po
21 translated messages.
msgmerge -U it.po messages.pot
... done.
msgfmt it.po -o it.mo
msgfmt --statistics it.po
21 translated messages.
msgmerge -U pt.po messages.pot
... done.
msgfmt pt.po -o pt.mo
msgfmt --statistics pt.po
21 translated messages.
for mo in cs.mo de.mo fr.mo it.mo pt.mo; do \
	lang=$(basename $mo .mo); \
  dir=/usr/share/locale/$lang/LC_MESSAGES; \
  install -d /builds/debdistutils/reproduce-trisquel/buildlogs/unp/unp-2.0~pre9+11.0trisquel1/debian/unp$dir; \
  install -m 644 $lang.mo /builds/debdistutils/reproduce-trisquel/buildlogs/unp/unp-2.0~pre9+11.0trisquel1/debian/unp$dir/unp.mo; \
done
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/unp/unp-2.0~pre9+11.0trisquel1/po'
dh_installman debian/unp.1
dh_compress
ln -sf unp.1.gz debian/unp/usr/share/man/man1/ucat.1.gz
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/unp/unp-2.0~pre9+11.0trisquel1'
   dh_installdocs
   dh_installchangelogs
   dh_installman
   dh_bash-completion
   dh_perl
   dh_link
   dh_strip_nondeterminism
	Normalized debian/unp/usr/share/locale/fr/LC_MESSAGES/unp.mo
	Normalized debian/unp/usr/share/locale/cs/LC_MESSAGES/unp.mo
	Normalized debian/unp/usr/share/locale/de/LC_MESSAGES/unp.mo
	Normalized debian/unp/usr/share/locale/pt/LC_MESSAGES/unp.mo
	Normalized debian/unp/usr/share/locale/it/LC_MESSAGES/unp.mo
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
pkgstripfiles: processing control file: debian/unp/DEBIAN/control, package unp, directory debian/unp
pkgstripfiles: Truncating usr/share/doc/unp/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 1 cpus) for package unp ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'unp' in '../unp_2.0~pre9+11.0trisquel1_all.deb'.
 dpkg-genbuildinfo -O../unp_2.0~pre9+11.0trisquel1_amd64.buildinfo
 dpkg-genchanges -O../unp_2.0~pre9+11.0trisquel1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ cd ..
+ ls -la
total 76
drwxr-xr-x 3 root root  4096 Apr  2 08:12 .
drwxr-xr-x 3 root root  4096 Apr  2 08:12 ..
-rw-r--r-- 1 root root 11382 Apr  2 08:12 buildlog.txt
drwxr-xr-x 4 root root  4096 Aug 11  2020 unp-2.0~pre9+11.0trisquel1
-rw-r--r-- 1 root root   575 Apr  2 08:12 unp_2.0~pre9+11.0trisquel1.dsc
-rw-r--r-- 1 root root 18447 Apr  2 08:12 unp_2.0~pre9+11.0trisquel1.tar.gz
-rw-r--r-- 1 root root 15668 Apr  2 08:12 unp_2.0~pre9+11.0trisquel1_all.deb
-rw-r--r-- 1 root root  6596 Apr  2 08:12 unp_2.0~pre9+11.0trisquel1_amd64.buildinfo
-rw-r--r-- 1 root root  1629 Apr  2 08:12 unp_2.0~pre9+11.0trisquel1_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./unp_2.0~pre9+11.0trisquel1_amd64.buildinfo ./buildlog.txt ./unp_2.0~pre9+11.0trisquel1.tar.gz ./unp_2.0~pre9+11.0trisquel1_amd64.changes ./unp_2.0~pre9+11.0trisquel1_all.deb ./unp_2.0~pre9+11.0trisquel1.dsc
8cbb630c936fc17c78551e2bb57b66e1e1dc1d6b46f1aee800fb6e46353267e7  ./unp_2.0~pre9+11.0trisquel1_amd64.buildinfo
af22811a5791a23981df6677d813e8928cad30d1b29d22600d1d6237879e04fd  ./buildlog.txt
7334077cdcc912aaf57f91c0fbd28ac44a4fb6a025e7832aef671cd67439fb9e  ./unp_2.0~pre9+11.0trisquel1.tar.gz
c692a5ef77debd1dd6759b24a620a6aa27a884691ce26eea6da781ca483d1fdc  ./unp_2.0~pre9+11.0trisquel1_amd64.changes
c3e002d4ab32c312da02b51756c2a05cdece242201314d7a4fedf13715dcd44c  ./unp_2.0~pre9+11.0trisquel1_all.deb
86b6091ea25181e9fe42c480abffe8e426845a06e6c211f39565ddbe5f4515a4  ./unp_2.0~pre9+11.0trisquel1.dsc
+ mkdir published
+ cd published
+ sed -e s,.*/\([^_]*\)_.*,\1,
+ echo ../unp_2.0~pre9+11.0trisquel1_all.deb
+ deb=unp
+ cut -d' -f2
+ grep ^'
+ apt-get --print-uris --yes download unp
+ url=http://archive.trisquel.info/trisquel/pool/main/u/unp/unp_2.0%7epre9%2b11.0trisquel1_all.deb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/u/unp/unp_2.0%7epre9%2b11.0trisquel1_all.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./unp_2.0~pre9+11.0trisquel1_all.deb
c3e002d4ab32c312da02b51756c2a05cdece242201314d7a4fedf13715dcd44c  ./unp_2.0~pre9+11.0trisquel1_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./unp_2.0~pre9+11.0trisquel1_all.deb: OK
+ echo Package unp is reproducible!
Package unp is reproducible!
