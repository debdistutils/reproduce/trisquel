+ date
Mon Apr  3 13:49:26 UTC 2023
+ apt-get source --only-source wireless-regdb
Reading package lists...
NOTICE: 'wireless-regdb' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/kernel-team/wireless-regdb.git
Please use:
git clone https://salsa.debian.org/kernel-team/wireless-regdb.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 60.8 kB of source archives.
Get:1 http://archive.trisquel.info/trisquel aramo/main wireless-regdb 2022.06.06-1+11.0trisquel1 (dsc) [2039 B]
Get:2 http://archive.trisquel.info/trisquel aramo/main wireless-regdb 2022.06.06-1+11.0trisquel1 (tar) [58.8 kB]
dpkg-source: info: extracting wireless-regdb in wireless-regdb-2022.06.06
dpkg-source: info: unpacking wireless-regdb_2022.06.06-1+11.0trisquel1.tar.gz
Fetched 60.8 kB in 0s (382 kB/s)
W: Download is performed unsandboxed as root as file 'wireless-regdb_2022.06.06-1+11.0trisquel1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source wireless-regdb
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-strip-nondeterminism dwz ed libdebhelper-perl
  libfile-stripnondeterminism-perl libsub-override-perl libtool m4 po-debconf
  python3-m2crypto quilt
0 upgraded, 18 newly installed, 0 to remove and 1 not upgraded.
Need to get 3699 kB of archives.
After this operation, 10.7 MB of additional disk space will be used.
Get:1 http://archive.trisquel.info/trisquel aramo/main amd64 ed amd64 1.18-1 [55.7 kB]
Get:2 http://archive.trisquel.info/trisquel aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:3 http://archive.trisquel.info/trisquel aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:4 http://archive.trisquel.info/trisquel aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:5 http://archive.trisquel.info/trisquel aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:6 http://archive.trisquel.info/trisquel aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:7 http://archive.trisquel.info/trisquel aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:8 http://archive.trisquel.info/trisquel aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:9 http://archive.trisquel.info/trisquel aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:10 http://archive.trisquel.info/trisquel aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:11 http://archive.trisquel.info/trisquel aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:12 http://archive.trisquel.info/trisquel aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:13 http://archive.trisquel.info/trisquel aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:14 http://archive.trisquel.info/trisquel aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:15 http://archive.trisquel.info/trisquel aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:16 http://archive.trisquel.info/trisquel aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
Get:17 http://archive.trisquel.info/trisquel aramo/main amd64 python3-m2crypto amd64 0.38.0-1ubuntu5 [191 kB]
Get:18 http://archive.trisquel.info/trisquel aramo/main amd64 quilt all 0.66-2.1 [304 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3699 kB in 0s (10.9 MB/s)
Selecting previously unselected package ed.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123238 files and directories currently installed.)
Preparing to unpack .../00-ed_1.18-1_amd64.deb ...
Unpacking ed (1.18-1) ...
Selecting previously unselected package m4.
Preparing to unpack .../01-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../02-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../03-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../04-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../05-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../06-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../07-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../08-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../09-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../10-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../11-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../12-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../13-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../14-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../15-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Selecting previously unselected package python3-m2crypto.
Preparing to unpack .../16-python3-m2crypto_0.38.0-1ubuntu5_amd64.deb ...
Unpacking python3-m2crypto (0.38.0-1ubuntu5) ...
Selecting previously unselected package quilt.
Preparing to unpack .../17-quilt_0.66-2.1_all.deb ...
Unpacking quilt (0.66-2.1) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up autotools-dev (20220109.1) ...
Setting up ed (1.18-1) ...
Setting up python3-m2crypto (0.38.0-1ubuntu5) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up quilt (0.66-2.1) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name wireless-regdb* -type d
+ cd ./wireless-regdb-2022.06.06
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package wireless-regdb
dpkg-buildpackage: info: source version 2022.06.06-1+11.0trisquel1
dpkg-buildpackage: info: source distribution aramo
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   debian/rules override_dh_auto_clean
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06'
rm -rf debian/build debian/stamps
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06'
   dh_clean
 dpkg-source -b .
dpkg-source: warning: no source format specified in debian/source/format, see dpkg-source(1)
dpkg-source: warning: native package version may not have a revision
dpkg-source: info: using source format '1.0'
dpkg-source: info: building wireless-regdb in wireless-regdb_2022.06.06-1+11.0trisquel1.tar.gz
dpkg-source: warning: upstream signing key but no upstream tarball signature
dpkg-source: info: building wireless-regdb in wireless-regdb_2022.06.06-1+11.0trisquel1.dsc
 debian/rules binary
dh binary
   dh_update_autotools_config
   dh_autoreconf
   debian/rules override_dh_auto_configure
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06'
rm -rf debian/build
test -d .pc
set +e; QUILT_PC=.pc quilt unapplied --quiltrc - >/dev/null && echo 'Patch series not fully applied'; test $? -eq 1
File series fully applied, ends at patch run-scripts-with-python-3.patch
mkdir -p 'debian/build'
cp -a CONTRIBUTING LICENSE Makefile README db.txt db2bin.py db2fw.py dbparse.py debian-example gen-pubcert.sh regulatory.bin.5 regulatory.db.5 sforshee.key.pub.pem sforshee.x509.pem web wireless-regdb.spec .gitignore 'debian/build'
cp debian/regulatory.db.p7s debian/build/
mkdir -p debian/stamps
touch debian/stamps/configure
dh override_dh_auto_configure
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06'
   debian/rules override_dh_auto_build
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06'
/usr/bin/make -C debian/build regulatory.db
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/build'
Generating regulatory.db
./db2fw.py regulatory.db db.txt
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/build'
touch debian/build/regulatory.bin
rm -f debian/build/.custom
diff regulatory.db debian/build/regulatory.db
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06'
   dh_auto_test
   create-stamp debian/debhelper-build-stamp
   dh_prep
   debian/rules override_dh_auto_install
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06'
/usr/bin/make -C debian/build DESTDIR=/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb install
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/build'
gzip < regulatory.bin.5 > regulatory.bin.5.gz
gzip < regulatory.db.5 > regulatory.db.5.gz
cat: .custom: No such file or directory
install -m 755 -d /builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb//lib/crda
install -m 755 -d /builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb//lib/crda/pubkeys
install -m 755 -d /builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb//lib/firmware
if [ -f .custom ]; then \
	install -m 644 -t /builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb//lib/crda/pubkeys/ ; \
fi
install -m 644 -t /builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb//lib/crda/pubkeys/ sforshee.key.pub.pem
install -m 644 -t /builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb//lib/crda/ regulatory.bin
install -m 644 -t /builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb//lib/firmware regulatory.db regulatory.db.p7s
install -m 755 -d /builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb//usr/share/man//man5/
install -m 644 -t /builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb//usr/share/man//man5/ regulatory.bin.5.gz regulatory.db.5.gz
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/build'
for file in regulatory.db regulatory.db.p7s; do \
	install -m644 $file debian/wireless-regdb/lib/firmware/$file-upstream \
	&& mv debian/wireless-regdb/lib/firmware/$file debian/wireless-regdb/lib/firmware/$file-debian \
	|| exit; \
done
rm -r debian/wireless-regdb/lib/crda
mv debian/wireless-regdb/usr/share/man/man5/regulatory.bin.5.gz \
	debian/wireless-regdb/usr/share/man/man5/regulatory.db.5.gz
/usr/bin/make -C debian/build DESTDIR=/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb-udeb install
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/build'
cat: .custom: No such file or directory
install -m 755 -d /builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb-udeb//lib/crda
install -m 755 -d /builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb-udeb//lib/crda/pubkeys
install -m 755 -d /builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb-udeb//lib/firmware
if [ -f .custom ]; then \
	install -m 644 -t /builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb-udeb//lib/crda/pubkeys/ ; \
fi
install -m 644 -t /builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb-udeb//lib/crda/pubkeys/ sforshee.key.pub.pem
install -m 644 -t /builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb-udeb//lib/crda/ regulatory.bin
install -m 644 -t /builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb-udeb//lib/firmware regulatory.db regulatory.db.p7s
install -m 755 -d /builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb-udeb//usr/share/man//man5/
install -m 644 -t /builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/wireless-regdb-udeb//usr/share/man//man5/ regulatory.bin.5.gz regulatory.db.5.gz
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06/debian/build'
rm -r debian/wireless-regdb-udeb/lib/crda debian/wireless-regdb-udeb/usr/share/man
rmdir --ignore-fail-on-non-empty -p debian/wireless-regdb-udeb/usr/share
dh override_dh_auto_install
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/wireless-regdb/wireless-regdb-2022.06.06'
   dh_installdocs
   dh_installchangelogs
   dh_installman
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
pkgstripfiles: processing control file: debian/wireless-regdb/DEBIAN/control, package wireless-regdb, directory debian/wireless-regdb
pkgstripfiles: Truncating usr/share/doc/wireless-regdb/changelog.Debian.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 1 cpus) for package wireless-regdb ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'wireless-regdb' in '../wireless-regdb_2022.06.06-1+11.0trisquel1_all.deb'.
INFO: Disabling pkgsanitychecks for udeb
pkgstripfiles: processing control file: debian/wireless-regdb-udeb/DEBIAN/control, package wireless-regdb-udeb, directory debian/wireless-regdb-udeb
pkgstripfiles: Running PNG optimization (using 1 cpus) for package wireless-regdb-udeb ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'wireless-regdb-udeb' in 'debian/.debhelper/scratch-space/build-wireless-regdb-udeb/wireless-regdb-udeb_2022.06.06-1+11.0trisquel1_all.deb'.
	Renaming wireless-regdb-udeb_2022.06.06-1+11.0trisquel1_all.deb to wireless-regdb-udeb_2022.06.06-1+11.0trisquel1_all.udeb
 dpkg-genbuildinfo -O../wireless-regdb_2022.06.06-1+11.0trisquel1_amd64.buildinfo
 dpkg-genchanges -O../wireless-regdb_2022.06.06-1+11.0trisquel1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ cd ..
+ ls -la
total 124
drwxr-xr-x 3 root root  4096 Apr  3 13:49 .
drwxr-xr-x 3 root root  4096 Apr  3 13:49 ..
-rw-r--r-- 1 root root 17565 Apr  3 13:49 buildlog.txt
drwxr-xr-x 6 root root  4096 Oct 21 17:54 wireless-regdb-2022.06.06
-rw-r--r-- 1 root root  3736 Apr  3 13:49 wireless-regdb-udeb_2022.06.06-1+11.0trisquel1_all.udeb
-rw-r--r-- 1 root root  1156 Apr  3 13:49 wireless-regdb_2022.06.06-1+11.0trisquel1.dsc
-rw-r--r-- 1 root root 58751 Apr  3 13:49 wireless-regdb_2022.06.06-1+11.0trisquel1.tar.gz
-rw-r--r-- 1 root root 10368 Apr  3 13:49 wireless-regdb_2022.06.06-1+11.0trisquel1_all.deb
-rw-r--r-- 1 root root  7669 Apr  3 13:49 wireless-regdb_2022.06.06-1+11.0trisquel1_amd64.buildinfo
-rw-r--r-- 1 root root  2352 Apr  3 13:49 wireless-regdb_2022.06.06-1+11.0trisquel1_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./wireless-regdb-udeb_2022.06.06-1+11.0trisquel1_all.udeb ./wireless-regdb_2022.06.06-1+11.0trisquel1.dsc ./wireless-regdb_2022.06.06-1+11.0trisquel1_amd64.changes ./buildlog.txt ./wireless-regdb_2022.06.06-1+11.0trisquel1.tar.gz ./wireless-regdb_2022.06.06-1+11.0trisquel1_amd64.buildinfo ./wireless-regdb_2022.06.06-1+11.0trisquel1_all.deb
01fc9e60804bfa29c0fbf4ada45ddb023f086caa5e537db8f48b57d3ab4bd21b  ./wireless-regdb-udeb_2022.06.06-1+11.0trisquel1_all.udeb
bde0290572e1efb432bfa4a400460b7d9b49df636427ec3032db814d32f8ad4e  ./wireless-regdb_2022.06.06-1+11.0trisquel1.dsc
af62a62ef06f5f1968746978a06cb3be9ca4881f8038952bd9070413912924c3  ./wireless-regdb_2022.06.06-1+11.0trisquel1_amd64.changes
de9ab64d07bd46df394991cd5a4a440cf43f7439a2ecfd4999bde531d4c1f4a7  ./buildlog.txt
594b1ac942ac80736652dfad455e6e86cdd05ee40c3f8863515ca2c293ef9bb0  ./wireless-regdb_2022.06.06-1+11.0trisquel1.tar.gz
41948bf28325ab3bafa80164df00e0738e9b7e3ffd24181ffa2ffde33c2a24c0  ./wireless-regdb_2022.06.06-1+11.0trisquel1_amd64.buildinfo
de35b816a9e6038462b4cec715ee7f4d81beb78bfa1cb56d130299e7551e99e5  ./wireless-regdb_2022.06.06-1+11.0trisquel1_all.deb
+ mkdir published
+ cd published
+ cd ../
+ ls wireless-regdb_2022.06.06-1+11.0trisquel1_all.deb wireless-regdb-udeb_2022.06.06-1+11.0trisquel1_all.udeb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/w/wireless-regdb/wireless-regdb-udeb_2022.06.06-1+11.0trisquel1_all.udeb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/w/wireless-regdb/wireless-regdb_2022.06.06-1+11.0trisquel1_all.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./wireless-regdb-udeb_2022.06.06-1+11.0trisquel1_all.udeb ./wireless-regdb_2022.06.06-1+11.0trisquel1_all.deb
01fc9e60804bfa29c0fbf4ada45ddb023f086caa5e537db8f48b57d3ab4bd21b  ./wireless-regdb-udeb_2022.06.06-1+11.0trisquel1_all.udeb
de35b816a9e6038462b4cec715ee7f4d81beb78bfa1cb56d130299e7551e99e5  ./wireless-regdb_2022.06.06-1+11.0trisquel1_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./wireless-regdb-udeb_2022.06.06-1+11.0trisquel1_all.udeb: OK
./wireless-regdb_2022.06.06-1+11.0trisquel1_all.deb: OK
+ echo Package wireless-regdb is reproducible!
Package wireless-regdb is reproducible!
