+ date
Mon Apr  3 13:25:07 UTC 2023
+ apt-get source --only-source pcmciautils
Reading package lists...
NOTICE: 'pcmciautils' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/debian/pcmciautils.git
Please use:
git clone https://salsa.debian.org/debian/pcmciautils.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 54.5 kB of source archives.
Get:1 http://archive.trisquel.info/trisquel aramo/main pcmciautils 018-13build1+11.0trisquel1 (dsc) [1900 B]
Get:2 http://archive.trisquel.info/trisquel aramo/main pcmciautils 018-13build1+11.0trisquel1 (tar) [52.6 kB]
dpkg-source: info: extracting pcmciautils in pcmciautils-018
dpkg-source: info: unpacking pcmciautils_018-13build1+11.0trisquel1.tar.xz
Fetched 54.5 kB in 0s (134 kB/s)
W: Download is performed unsandboxed as root as file 'pcmciautils_018-13build1+11.0trisquel1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source pcmciautils
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev bison debhelper debugedit
  dh-autoreconf dh-strip-nondeterminism dwz flex libdebhelper-perl
  libfile-stripnondeterminism-perl libsub-override-perl libtool m4 po-debconf
0 upgraded, 17 newly installed, 0 to remove and 1 not upgraded.
Need to get 4204 kB of archives.
After this operation, 12.4 MB of additional disk space will be used.
Get:1 http://archive.trisquel.info/trisquel aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://archive.trisquel.info/trisquel aramo/main amd64 flex amd64 2.6.4-8build2 [307 kB]
Get:3 http://archive.trisquel.info/trisquel aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:4 http://archive.trisquel.info/trisquel aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:5 http://archive.trisquel.info/trisquel aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:6 http://archive.trisquel.info/trisquel aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:7 http://archive.trisquel.info/trisquel aramo/main amd64 bison amd64 2:3.8.2+dfsg-1build1 [748 kB]
Get:8 http://archive.trisquel.info/trisquel aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:9 http://archive.trisquel.info/trisquel aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:10 http://archive.trisquel.info/trisquel aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:11 http://archive.trisquel.info/trisquel aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:12 http://archive.trisquel.info/trisquel aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:13 http://archive.trisquel.info/trisquel aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:14 http://archive.trisquel.info/trisquel aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:15 http://archive.trisquel.info/trisquel aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:16 http://archive.trisquel.info/trisquel aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:17 http://archive.trisquel.info/trisquel aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 4204 kB in 1s (5840 kB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123238 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package flex.
Preparing to unpack .../01-flex_2.6.4-8build2_amd64.deb ...
Unpacking flex (2.6.4-8build2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../02-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../03-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../04-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../05-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package bison.
Preparing to unpack .../06-bison_2%3a3.8.2+dfsg-1build1_amd64.deb ...
Unpacking bison (2:3.8.2+dfsg-1build1) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../07-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../08-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../09-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../10-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../11-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../12-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../13-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../14-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../15-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../16-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up bison (2:3.8.2+dfsg-1build1) ...
update-alternatives: using /usr/bin/bison.yacc to provide /usr/bin/yacc (yacc) in auto mode
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up flex (2.6.4-8build2) ...
Setting up libtool (2.4.6-15build2) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name pcmciautils* -type d
+ cd ./pcmciautils-018
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package pcmciautils
dpkg-buildpackage: info: source version 018-13build1+11.0trisquel1
dpkg-buildpackage: info: source distribution aramo
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   dh_auto_clean
	make -j1 clean
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018'
find . \( -not -type d \) -and \( -name '*~' -o -name '*.[oas]' \) -type f -print \
 | xargs rm -f 
rm -f pccardctl pcmcia-check-broken-cis pcmcia-socket-startup
rm -f cbdump dump_cis
rm -f src/yacc_config.c src/yacc_config.d src/lex_config.c src/lex_config.d src/yacc_config.h
rm -f udev/60-pcmcia.rules
rm -f build/ccdv
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018'
   dh_clean
 dpkg-source -b .
dpkg-source: warning: native package version may not have a revision
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building pcmciautils in pcmciautils_018-13build1+11.0trisquel1.tar.xz
dpkg-source: info: building pcmciautils in pcmciautils_018-13build1+11.0trisquel1.dsc
 debian/rules binary
dh binary
   dh_update_autotools_config
   dh_autoreconf
   dh_auto_configure
   debian/rules override_dh_auto_build
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018'
/usr/bin/make  STRIPCMD=true
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018'
Building ccdv
build/ccdv.c: In function 'main':
build/ccdv.c:384:24: warning: ignoring return value of 'write' declared with attribute 'warn_unused_result' [-Wunused-result]
  384 |                 (void) write(2, emerg, (size_t) nread);
      |                        ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
build/ccdv.c:121:35: warning: '%s' directive output may be truncated writing up to 199 bytes into a region of size 71 [-Wformat-truncation=]
  121 |         snprintf(s1, sizeof(s1), "%s%s%s... ", gAction, gTarget[0] ? " " : "", gTarget);
      |                                   ^~           ~~~~~~~
In file included from /usr/include/stdio.h:894,
                 from build/ccdv.c:13:
/usr/include/x86_64-linux-gnu/bits/stdio2.h:71:10: note: '__builtin___snprintf_chk' output between 5 and 404 bytes into a destination of size 71
   71 |   return __builtin___snprintf_chk (__s, __n, __USE_FORTIFY_LEVEL - 1,
      |          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   72 |                                    __glibc_objsize (__s), __fmt,
      |                                    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   73 |                                    __va_arg_pack ());
      |                                    ~~~~~~~~~~~~~~~~~
build/ccdv.c:180:35: warning: '%s' directive output may be truncated writing up to 199 bytes into a region of size 71 [-Wformat-truncation=]
  180 |         snprintf(s1, sizeof(s1), "%s%s%s: ", gAction, gTarget[0] ? " " : "", gTarget);
      |                                   ^~         ~~~~~~~
In file included from /usr/include/stdio.h:894,
                 from build/ccdv.c:13:
/usr/include/x86_64-linux-gnu/bits/stdio2.h:71:10: note: '__builtin___snprintf_chk' output between 3 and 402 bytes into a destination of size 71
   71 |   return __builtin___snprintf_chk (__s, __n, __USE_FORTIFY_LEVEL - 1,
      |          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   72 |                                    __glibc_objsize (__s), __fmt,
      |                                    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   73 |                                    __va_arg_pack ());
      |                                    ~~~~~~~~~~~~~~~~~
gcc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -pipe -DPCMCIAUTILS_VERSION=\"018\" -I/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018/src -Wall -Wchar-subscripts -Wpointer-arith -Wsign-compare -Wno-pointer-sign -Wdeclaration-after-statement -Wshadow -I/usr/lib/gcc/x86_64-linux-gnu/11/include -Os -fomit-frame-pointer -D_GNU_SOURCE -Wdate-time -D_FORTIFY_SOURCE=2 -c -o src/pccardctl.o src/pccardctl.c
yacc -d src/yacc_config.y
src/yacc_config.y:47.13-18: warning: POSIX yacc reserves %type to nonterminals [-Wyacc]
   47 | %type <str> STRING
      |             ^~~~~~
src/yacc_config.y:48.13-18: warning: POSIX yacc reserves %type to nonterminals [-Wyacc]
   48 | %type <num> NUMBER
      |             ^~~~~~
mv y.tab.c src/yacc_config.c
mv y.tab.h src/yacc_config.h
gcc -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro -Wl,-warn-common -o pccardctl  src/pccardctl.o -lc 
true pccardctl
gcc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -pipe -DPCMCIAUTILS_VERSION=\"018\" -I/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018/src -Wall -Wchar-subscripts -Wpointer-arith -Wsign-compare -Wno-pointer-sign -Wdeclaration-after-statement -Wshadow -I/usr/lib/gcc/x86_64-linux-gnu/11/include -Os -fomit-frame-pointer -D_GNU_SOURCE -Wdate-time -D_FORTIFY_SOURCE=2 -c -o src/pcmcia-check-broken-cis.o src/pcmcia-check-broken-cis.c
gcc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -pipe -DPCMCIAUTILS_VERSION=\"018\" -I/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018/src -Wall -Wchar-subscripts -Wpointer-arith -Wsign-compare -Wno-pointer-sign -Wdeclaration-after-statement -Wshadow -I/usr/lib/gcc/x86_64-linux-gnu/11/include -Os -fomit-frame-pointer -D_GNU_SOURCE -Wdate-time -D_FORTIFY_SOURCE=2 -c -o src/read-cis.o src/read-cis.c
gcc -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro -Wl,-warn-common -o pcmcia-check-broken-cis  src/pcmcia-check-broken-cis.o src/read-cis.o -lc 
true pcmcia-check-broken-cis
gcc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -pipe -DPCMCIAUTILS_VERSION=\"018\" -I/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018/src -Wall -Wchar-subscripts -Wpointer-arith -Wsign-compare -Wno-pointer-sign -Wdeclaration-after-statement -Wshadow -I/usr/lib/gcc/x86_64-linux-gnu/11/include -Os -fomit-frame-pointer -D_GNU_SOURCE -Wdate-time -D_FORTIFY_SOURCE=2 -c -o src/startup.o src/startup.c
gcc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -pipe -DPCMCIAUTILS_VERSION=\"018\" -I/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018/src -Wall -Wchar-subscripts -Wpointer-arith -Wsign-compare -Wno-pointer-sign -Wdeclaration-after-statement -Wshadow -I/usr/lib/gcc/x86_64-linux-gnu/11/include -Os -fomit-frame-pointer -D_GNU_SOURCE -Wdate-time -D_FORTIFY_SOURCE=2 -c -o src/yacc_config.o src/yacc_config.c
lex  -t src/lex_config.l > src/lex_config.c
gcc -g -O2 -ffile-prefix-map=/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -pipe -DPCMCIAUTILS_VERSION=\"018\" -I/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018/src -Wall -Wchar-subscripts -Wpointer-arith -Wsign-compare -Wno-pointer-sign -Wdeclaration-after-statement -Wshadow -I/usr/lib/gcc/x86_64-linux-gnu/11/include -Os -fomit-frame-pointer -D_GNU_SOURCE -Wdate-time -D_FORTIFY_SOURCE=2 -c -o src/lex_config.o src/lex_config.c
gcc -Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro -Wl,-warn-common -o pcmcia-socket-startup  src/startup.o src/yacc_config.o src/lex_config.o -lc 
true pcmcia-socket-startup
cat udev/rules-start udev/rules-base udev/rules-nonstaticsocket udev/rules-end | sed -e "s#__UDEVHELPERDIR__#/lib/udev#g" > udev/60-pcmcia.rules
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018'
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018'
   dh_auto_test
   create-stamp debian/debhelper-build-stamp
   dh_prep
   debian/rules override_dh_auto_install
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018'
dh_auto_install --destdir=debian/pcmciautils -- \
	pcmciaconfdir=/usr/lib/pcmciautils
	make -j1 install DESTDIR=/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018/debian/pcmciautils AM_UPDATE_INFO_DIR=no "INSTALL=install --strip-program=true" pcmciaconfdir=/usr/lib/pcmciautils
make[2]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018'
install --strip-program=true -d /builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018/debian/pcmciautils/sbin
install: WARNING: ignoring --strip-program option as -s option was not specified
install --strip-program=true -D pccardctl /builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018/debian/pcmciautils/sbin/pccardctl
install: WARNING: ignoring --strip-program option as -s option was not specified
ln -sf pccardctl /builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018/debian/pcmciautils/sbin/lspcmcia
install --strip-program=true -D pcmcia-check-broken-cis /builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018/debian/pcmciautils/lib/udev/pcmcia-check-broken-cis
install: WARNING: ignoring --strip-program option as -s option was not specified
install --strip-program=true -m 644 -D man/man8/pccardctl.8 /builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018/debian/pcmciautils/usr/share/man/man8/pccardctl.8
install: WARNING: ignoring --strip-program option as -s option was not specified
ln -sf pccardctl.8 /builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018/debian/pcmciautils/usr/share/man/man8/lspcmcia.8
install --strip-program=true -m 644 -D udev/60-pcmcia.rules /builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018/debian/pcmciautils/lib/udev/rules.d/60-pcmcia.rules
install: WARNING: ignoring --strip-program option as -s option was not specified
install --strip-program=true -d /builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018/debian/pcmciautils/usr/lib/pcmciautils
install: WARNING: ignoring --strip-program option as -s option was not specified
install --strip-program=true -m 644  -D config/config.opts /builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018/debian/pcmciautils/usr/lib/pcmciautils/config.opts
install: WARNING: ignoring --strip-program option as -s option was not specified
if [ -f config/config.opts.x86_64 ]; then \
	install --strip-program=true -m 644 -D config/config.opts.x86_64 /builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018/debian/pcmciautils/usr/lib/pcmciautils/config.opts; \
fi
install --strip-program=true -D pcmcia-socket-startup /builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018/debian/pcmciautils/lib/udev/pcmcia-socket-startup
install: WARNING: ignoring --strip-program option as -s option was not specified
make[2]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018'
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018'
   debian/rules override_dh_install
make[1]: Entering directory '/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018'
dh_install --sourcedir=debian/pcmciautils
make[1]: Leaving directory '/builds/debdistutils/reproduce-trisquel/buildlogs/pcmciautils/pcmciautils-018'
   dh_installdocs
   dh_installchangelogs
   dh_installman
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_strip
106f396f705a93da33f72092f07655b075fdf698
4f31e4a3da1b4508b41b5c41ae545047117a39c1
97f46907f7e53694f8fe7b3b75624382e149079d
   dh_makeshlibs
   dh_shlibdeps
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
pkgstripfiles: processing control file: debian/pcmciautils/DEBIAN/control, package pcmciautils, directory debian/pcmciautils
pkgstripfiles: Truncating usr/share/doc/pcmciautils/changelog.Debian.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 1 cpus) for package pcmciautils ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'pcmciautils' in '../pcmciautils_018-13build1+11.0trisquel1_amd64.deb'.
INFO: Disabling pkgsanitychecks for udeb
pkgstripfiles: processing control file: debian/pcmciautils-udeb/DEBIAN/control, package pcmciautils-udeb, directory debian/pcmciautils-udeb
pkgstripfiles: Running PNG optimization (using 1 cpus) for package pcmciautils-udeb ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'pcmciautils-udeb' in 'debian/.debhelper/scratch-space/build-pcmciautils-udeb/pcmciautils-udeb_018-13build1+11.0trisquel1_amd64.deb'.
	Renaming pcmciautils-udeb_018-13build1+11.0trisquel1_amd64.deb to pcmciautils-udeb_018-13build1+11.0trisquel1_amd64.udeb
 dpkg-genbuildinfo -O../pcmciautils_018-13build1+11.0trisquel1_amd64.buildinfo
 dpkg-genchanges -O../pcmciautils_018-13build1+11.0trisquel1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ cd ..
+ ls -la
total 148
drwxr-xr-x  3 root root  4096 Apr  3 13:25 .
drwxr-xr-x  3 root root  4096 Apr  3 13:25 ..
-rw-r--r--  1 root root 22146 Apr  3 13:25 buildlog.txt
drwxr-xr-x 12 root root  4096 Apr  3 13:25 pcmciautils-018
-rw-r--r--  1 root root 12412 Apr  3 13:25 pcmciautils-udeb_018-13build1+11.0trisquel1_amd64.udeb
-rw-r--r--  1 root root  1017 Apr  3 13:25 pcmciautils_018-13build1+11.0trisquel1.dsc
-rw-r--r--  1 root root 52588 Apr  3 13:25 pcmciautils_018-13build1+11.0trisquel1.tar.xz
-rw-r--r--  1 root root  7071 Apr  3 13:25 pcmciautils_018-13build1+11.0trisquel1_amd64.buildinfo
-rw-r--r--  1 root root  2347 Apr  3 13:25 pcmciautils_018-13build1+11.0trisquel1_amd64.changes
-rw-r--r--  1 root root 26510 Apr  3 13:25 pcmciautils_018-13build1+11.0trisquel1_amd64.deb
+ find . -maxdepth 1 -type f
+ sha256sum ./pcmciautils_018-13build1+11.0trisquel1.tar.xz ./pcmciautils_018-13build1+11.0trisquel1_amd64.buildinfo ./pcmciautils_018-13build1+11.0trisquel1_amd64.deb ./buildlog.txt ./pcmciautils-udeb_018-13build1+11.0trisquel1_amd64.udeb ./pcmciautils_018-13build1+11.0trisquel1_amd64.changes ./pcmciautils_018-13build1+11.0trisquel1.dsc
790bc5352ffbd75189028f571626a43cb2c56bdf7eb2650c9ceb7a1db52997f6  ./pcmciautils_018-13build1+11.0trisquel1.tar.xz
3a03509da48ddf3a9e82c3145fa6ca01ee6dc072d3a446d408b23d8e0f73cd35  ./pcmciautils_018-13build1+11.0trisquel1_amd64.buildinfo
7f83af21a8727e91e37a732d37ac35a7cbd2667019a1ec45ff56ed1fc22393f0  ./pcmciautils_018-13build1+11.0trisquel1_amd64.deb
e2ec9251619f30d484761fc352f373431552254948e1066944cdd7660b2e4426  ./buildlog.txt
37084e98b33861f8c58cc6af793890be9c7f9d46eb4ef776fd875e704c8b3395  ./pcmciautils-udeb_018-13build1+11.0trisquel1_amd64.udeb
e2055e59bb5cfa70e82b1a98ffffe88f0f3ac2769398660defb82543a3f101e6  ./pcmciautils_018-13build1+11.0trisquel1_amd64.changes
cf3016ef253fa4094816432fb4654315b11959f2db73bc0eb8ad45cf08a8fe7e  ./pcmciautils_018-13build1+11.0trisquel1.dsc
+ mkdir published
+ cd published
+ cd ../
+ ls pcmciautils_018-13build1+11.0trisquel1_amd64.deb pcmciautils-udeb_018-13build1+11.0trisquel1_amd64.udeb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/p/pcmciautils/pcmciautils-udeb_018-13build1+11.0trisquel1_amd64.udeb
+ wget -q http://archive.trisquel.info/trisquel/pool/main/p/pcmciautils/pcmciautils_018-13build1+11.0trisquel1_amd64.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./pcmciautils_018-13build1+11.0trisquel1_amd64.deb ./pcmciautils-udeb_018-13build1+11.0trisquel1_amd64.udeb
a33ecb6e6a3ca33e087d0a0a0c52074ad58f1f79a2248d40aca4c62abc77748e  ./pcmciautils_018-13build1+11.0trisquel1_amd64.deb
7a42ea5a6c3f32f032625e121d7d8d3b4efda7759e7f46807b88b69041424858  ./pcmciautils-udeb_018-13build1+11.0trisquel1_amd64.udeb
+ cd ..
+ sha256sum -c SHA256SUMS
./pcmciautils_018-13build1+11.0trisquel1_amd64.deb: FAILED
./pcmciautils-udeb_018-13build1+11.0trisquel1_amd64.udeb: FAILED
sha256sum: WARNING: 2 computed checksums did NOT match
