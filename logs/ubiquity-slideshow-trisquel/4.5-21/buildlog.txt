+ date
Mon Jun 10 05:38:08 UTC 2024
+ id
uid=0(root) gid=0(root) groups=0(root)
+ apt-get source --only-source ubiquity-slideshow-trisquel=4.5-21
Reading package lists...
Need to get 2636 kB of source archives.
Get:1 http://ftp.acc.umu.se/mirror/trisquel/packages aramo-updates/main ubiquity-slideshow-trisquel 4.5-21 (dsc) [1541 B]
Get:2 http://ftp.acc.umu.se/mirror/trisquel/packages aramo-updates/main ubiquity-slideshow-trisquel 4.5-21 (tar) [2635 kB]
dpkg-source: info: extracting ubiquity-slideshow-trisquel in ubiquity-slideshow-trisquel-4.5
dpkg-source: info: unpacking ubiquity-slideshow-trisquel_4.5-21.tar.gz
Fetched 2636 kB in 0s (6144 kB/s)
W: Download is performed unsandboxed as root as file 'ubiquity-slideshow-trisquel_4.5-21.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source ubiquity-slideshow-trisquel=4.5-21
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev debhelper debugedit dh-autoreconf
  dh-strip-nondeterminism dwz libdebhelper-perl
  libfile-stripnondeterminism-perl libsub-override-perl libtool m4 po-debconf
0 upgraded, 15 newly installed, 0 to remove and 278 not upgraded.
Need to get 3149 kB of archives.
After this operation, 8859 kB of additional disk space will be used.
Get:1 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 m4 amd64 1.4.18-5ubuntu2 [199 kB]
Get:2 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 autoconf all 2.71-2 [338 kB]
Get:3 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 autotools-dev all 20220109.1 [44.9 kB]
Get:4 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 automake all 1:1.16.5-1.3 [558 kB]
Get:5 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 autopoint all 0.21-4ubuntu4 [422 kB]
Get:6 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libdebhelper-perl all 13.6ubuntu1 [67.2 kB]
Get:7 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libtool all 2.4.6-15build2 [164 kB]
Get:8 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 dh-autoreconf all 20 [16.1 kB]
Get:9 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libsub-override-perl all 0.09-2 [9532 B]
Get:10 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 libfile-stripnondeterminism-perl all 1.13.0-1 [18.1 kB]
Get:11 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 dh-strip-nondeterminism all 1.13.0-1 [5344 B]
Get:12 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 debugedit amd64 1:5.0-4build1 [47.2 kB]
Get:13 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 dwz amd64 0.14-1build2 [105 kB]
Get:14 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 po-debconf all 1.0.21+nmu1 [233 kB]
Get:15 http://ftp.acc.umu.se/mirror/trisquel/packages aramo/main amd64 debhelper all 13.6ubuntu1 [923 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3149 kB in 0s (8695 kB/s)
Selecting previously unselected package m4.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 123255 files and directories currently installed.)
Preparing to unpack .../00-m4_1.4.18-5ubuntu2_amd64.deb ...
Unpacking m4 (1.4.18-5ubuntu2) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../01-autoconf_2.71-2_all.deb ...
Unpacking autoconf (2.71-2) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../02-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../03-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../04-autopoint_0.21-4ubuntu4_all.deb ...
Unpacking autopoint (0.21-4ubuntu4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../05-libdebhelper-perl_13.6ubuntu1_all.deb ...
Unpacking libdebhelper-perl (13.6ubuntu1) ...
Selecting previously unselected package libtool.
Preparing to unpack .../06-libtool_2.4.6-15build2_all.deb ...
Unpacking libtool (2.4.6-15build2) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../07-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../08-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../09-libfile-stripnondeterminism-perl_1.13.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.13.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../10-dh-strip-nondeterminism_1.13.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.13.0-1) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../11-debugedit_1%3a5.0-4build1_amd64.deb ...
Unpacking debugedit (1:5.0-4build1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../12-dwz_0.14-1build2_amd64.deb ...
Unpacking dwz (0.14-1build2) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../13-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../14-debhelper_13.6ubuntu1_all.deb ...
Unpacking debhelper (13.6ubuntu1) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up libdebhelper-perl (13.6ubuntu1) ...
Setting up m4 (1.4.18-5ubuntu2) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.21-4ubuntu4) ...
Setting up autoconf (2.71-2) ...
Setting up dwz (0.14-1build2) ...
Setting up debugedit (1:5.0-4build1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
Setting up libfile-stripnondeterminism-perl (1.13.0-1) ...
Setting up libtool (2.4.6-15build2) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.13.0-1) ...
Setting up debhelper (13.6ubuntu1) ...
Processing triggers for man-db (2.10.2-1) ...
+ find . -maxdepth 1 -name ubiquity-slideshow-trisquel* -type d
+ cd ./ubiquity-slideshow-trisquel-4.5
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package ubiquity-slideshow-trisquel
dpkg-buildpackage: info: source version 4.5-21
dpkg-buildpackage: info: source distribution aramo
dpkg-buildpackage: info: source changed by Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh_testdir
dh_testroot
dh_clean
 dpkg-source -b .
dpkg-source: warning: no source format specified in debian/source/format, see dpkg-source(1)
dpkg-source: warning: native package version may not have a revision
dpkg-source: info: using source format '1.0'
dpkg-source: info: building ubiquity-slideshow-trisquel in ubiquity-slideshow-trisquel_4.5-21.tar.gz
dpkg-source: info: building ubiquity-slideshow-trisquel in ubiquity-slideshow-trisquel_4.5-21.dsc
 debian/rules build
make: Nothing to be done for 'build'.
 debian/rules binary
dh_clean
dh_testdir
dh_testroot
dh_installchangelogs
dh_installdocs
cp data/* -a debian/`find debian/ -mindepth 1 -maxdepth 1 -type d -not -path '*/\.*'| cut -d'/' -f2`
dh_fixperms
dh_compress
dh_installdeb
dh_gencontrol
dh_md5sums
dh_builddeb
pkgstripfiles: processing control file: debian/ubiquity-slideshow-trisquel/DEBIAN/control, package ubiquity-slideshow-trisquel, directory debian/ubiquity-slideshow-trisquel
pkgstripfiles: Truncating usr/share/doc/ubiquity-slideshow-trisquel/changelog.Debian.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 40 cpus) for package ubiquity-slideshow-trisquel ...
xargs: warning: options --max-lines and --replace/-I/-i are mutually exclusive, ignoring previous --max-lines value
.o.ooooooooooooooooooooooooooooooooo
pkgstripfiles: PNG optimization (34/0) for package ubiquity-slideshow-trisquel took 6 s
dpkg-deb: building package 'ubiquity-slideshow-trisquel' in '../ubiquity-slideshow-trisquel_4.5-21_all.deb'.
 dpkg-genbuildinfo -O../ubiquity-slideshow-trisquel_4.5-21_amd64.buildinfo
 dpkg-genchanges -O../ubiquity-slideshow-trisquel_4.5-21_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Mon Jun 10 05:38:25 UTC 2024
+ cd ..
+ ls -la
total 4548
drwxr-xr-x 3 root root    4096 Jun 10 05:38 .
drwxr-xr-x 3 root root    4096 Jun 10 05:38 ..
-rw-r--r-- 1 root root    9436 Jun 10 05:38 buildlog.txt
drwxr-xr-x 4 root root    4096 May 14 05:46 ubiquity-slideshow-trisquel-4.5
-rw-r--r-- 1 root root     658 Jun 10 05:38 ubiquity-slideshow-trisquel_4.5-21.dsc
-rw-r--r-- 1 root root 2634718 Jun 10 05:38 ubiquity-slideshow-trisquel_4.5-21.tar.gz
-rw-r--r-- 1 root root 1977880 Jun 10 05:38 ubiquity-slideshow-trisquel_4.5-21_all.deb
-rw-r--r-- 1 root root    6734 Jun 10 05:38 ubiquity-slideshow-trisquel_4.5-21_amd64.buildinfo
-rw-r--r-- 1 root root    1837 Jun 10 05:38 ubiquity-slideshow-trisquel_4.5-21_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./buildlog.txt ./ubiquity-slideshow-trisquel_4.5-21_amd64.changes ./ubiquity-slideshow-trisquel_4.5-21.dsc ./ubiquity-slideshow-trisquel_4.5-21_amd64.buildinfo ./ubiquity-slideshow-trisquel_4.5-21.tar.gz ./ubiquity-slideshow-trisquel_4.5-21_all.deb
10e88b3709fdc0c74dcf6f4448f5536a1d0541eab65103e8a98769601280a313  ./buildlog.txt
15b3daf5c05f34de9edc270eec6e298fc5eeaeb2257d269865470372ac0bb77c  ./ubiquity-slideshow-trisquel_4.5-21_amd64.changes
2d16aa134d4e6b0107e37c738f882da6195fe27da5fbaea6f7f0c3be4f7abbc1  ./ubiquity-slideshow-trisquel_4.5-21.dsc
aa3caab3ff07706b33a7926dd3ec49ac548ebfdadc89694b05ad8d63f5129d33  ./ubiquity-slideshow-trisquel_4.5-21_amd64.buildinfo
1731ccc39922beae9e9d285742dba4db708ce56b1fd6d9326cee4f4f3d9caea9  ./ubiquity-slideshow-trisquel_4.5-21.tar.gz
454a9c557b6e81be3a2d3920fa8aaa39ff5cfa7afe06570ea161d318248b3da4  ./ubiquity-slideshow-trisquel_4.5-21_all.deb
+ mkdir published
+ cd published
+ cd ../
+ ls ubiquity-slideshow-trisquel_4.5-21_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://ftp.acc.umu.se/mirror/trisquel/packages/pool/main/u/ubiquity-slideshow-trisquel/ubiquity-slideshow-trisquel_4.5-21_all.deb
--2024-06-10 05:38:25--  http://ftp.acc.umu.se/mirror/trisquel/packages/pool/main/u/ubiquity-slideshow-trisquel/ubiquity-slideshow-trisquel_4.5-21_all.deb
Resolving ftp.acc.umu.se (ftp.acc.umu.se)... 194.71.11.163, 194.71.11.165, 194.71.11.173, ...
Connecting to ftp.acc.umu.se (ftp.acc.umu.se)|194.71.11.163|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 1977880 (1.9M) [application/x-debian-package]
Saving to: 'ubiquity-slideshow-trisquel_4.5-21_all.deb'

     0K .......... .......... .......... .......... ..........  2% 1.58M 1s
    50K .......... .......... .......... .......... ..........  5% 1.81M 1s
   100K .......... .......... .......... .......... ..........  7% 3.69M 1s
   150K .......... .......... .......... .......... .......... 10% 2.23M 1s
   200K .......... .......... .......... .......... .......... 12% 4.12M 1s
   250K .......... .......... .......... .......... .......... 15% 3.32M 1s
   300K .......... .......... .......... .......... .......... 18% 5.46M 1s
   350K .......... .......... .......... .......... .......... 20% 4.26M 1s
   400K .......... .......... .......... .......... .......... 23% 5.66M 0s
   450K .......... .......... .......... .......... .......... 25% 1.72M 1s
   500K .......... .......... .......... .......... .......... 28% 17.3M 0s
   550K .......... .......... .......... .......... .......... 31% 18.8M 0s
   600K .......... .......... .......... .......... .......... 33% 24.4M 0s
   650K .......... .......... .......... .......... .......... 36% 24.4M 0s
   700K .......... .......... .......... .......... .......... 38% 21.1M 0s
   750K .......... .......... .......... .......... .......... 41% 23.9M 0s
   800K .......... .......... .......... .......... .......... 44% 24.5M 0s
   850K .......... .......... .......... .......... .......... 46% 9.97M 0s
   900K .......... .......... .......... .......... .......... 49% 5.98M 0s
   950K .......... .......... .......... .......... .......... 51% 8.72M 0s
  1000K .......... .......... .......... .......... .......... 54% 9.16M 0s
  1050K .......... .......... .......... .......... .......... 56% 15.9M 0s
  1100K .......... .......... .......... .......... .......... 59% 17.9M 0s
  1150K .......... .......... .......... .......... .......... 62% 19.9M 0s
  1200K .......... .......... .......... .......... .......... 64% 19.8M 0s
  1250K .......... .......... .......... .......... .......... 67% 14.2M 0s
  1300K .......... .......... .......... .......... .......... 69% 17.9M 0s
  1350K .......... .......... .......... .......... .......... 72% 16.5M 0s
  1400K .......... .......... .......... .......... .......... 75% 15.4M 0s
  1450K .......... .......... .......... .......... .......... 77% 34.9M 0s
  1500K .......... .......... .......... .......... .......... 80% 67.3M 0s
  1550K .......... .......... .......... .......... .......... 82% 89.6M 0s
  1600K .......... .......... .......... .......... .......... 85% 13.4M 0s
  1650K .......... .......... .......... .......... .......... 88% 10.7M 0s
  1700K .......... .......... .......... .......... .......... 90% 12.1M 0s
  1750K .......... .......... .......... .......... .......... 93% 16.2M 0s
  1800K .......... .......... .......... .......... .......... 95% 15.8M 0s
  1850K .......... .......... .......... .......... .......... 98% 19.3M 0s
  1900K .......... .......... .......... .                    100% 83.0M=0.3s

2024-06-10 05:38:26 (7.15 MB/s) - 'ubiquity-slideshow-trisquel_4.5-21_all.deb' saved [1977880/1977880]

+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./ubiquity-slideshow-trisquel_4.5-21_all.deb
454a9c557b6e81be3a2d3920fa8aaa39ff5cfa7afe06570ea161d318248b3da4  ./ubiquity-slideshow-trisquel_4.5-21_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./ubiquity-slideshow-trisquel_4.5-21_all.deb: OK
+ echo Package ubiquity-slideshow-trisquel version 4.5-21 is reproducible!
Package ubiquity-slideshow-trisquel version 4.5-21 is reproducible!
